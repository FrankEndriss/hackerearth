

#include <stdio.h>

/** Find avg of list of ints. */
int main(int argc, char** argv) {
int i, k, n, sum, ans;
	scanf("%d", &n);
	sum=0;
	for(i=0; i<n; i++)  {
		scanf("%d", &k);
		sum+=k;
	}
	ans=sum/n;
	while(ans*n<=sum)
		ans++;

	printf("%d\n", ans);
}
