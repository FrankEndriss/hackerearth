
#include <stdio.h>
#include <string.h>

int gcd(int a, int b) 
{ 
    if (a == 0) 
        return b; 
    return gcd(b%a, a); 
} 

int mini(int a, int b) {
	if(a<b)
		return a;
	else 
		return b;
}

void solve() {
int n, q, i, k;
	scanf("%d", &n);
	scanf("%d", &k);
	scanf("%d", &q);

int a[n];
	for(i=0; i<n; i++)
		scanf("%d", &a[i]);

#define INF 10000000
int l, r, tmp, m;
	for(i=0; i<q; i++) {
		scanf("%d", &l);
		scanf("%d", &r);
		l--;
		r--;
		if(l>r) {
			tmp=r;
			r=l;
			l=tmp;
		}
int l1, r1;
long long prod=0;
		m=INF;
// find the shortes subrange of [l, r] with prod%k==0
		for(l1=l; l1<=r; l1++) {
			prod=1;
			for(r1=l1; r1<=r && m>r1-l1+1; r1++) {
				if(gcd(k, a[r1])!=1)
					prod*=a[r1];
				if(prod%k==0) {
					m=mini(m, r1-l1+1);
				}
			}
		}
		if(m==INF)
			printf("-1 ");
		else
			printf("%d ", m);
		
	}
	printf("\n");

	
}

int main(){
	//scanf("%d", &t);
	//for(i=0; i<t; i++)
		solve();
}
