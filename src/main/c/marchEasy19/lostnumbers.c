#include <stdio.h>

int main(){
long long xor, or, and;
	scanf("%lld", &or);
	scanf("%lld", &xor);
	scanf("%lld", &and);

long long a=0, b=0;
long long bit=0;
int bor, bxor, band;
int i;
//printf("or=%lld xor=%lld and=%lld\n", or, xor, and);
	for(i=0; i<60; i++) {
		bit=1LL<<i;	
		bor=(or&bit)!=0;
		bxor=(xor&bit)!=0;
		band=(and&bit)!=0;
//printf("bor=%d bxor=%d band=%d\n", bor, bxor, band);

		if(band) {
			a|=bit;
			b|=bit;
			if(bor==0 || bxor) {
				printf("-1\n");
				return 0;
			}
		} else if(bxor) {
			a|=bit;
			if((bor==0) || band) {
				printf("-1\n");
				return 0;
			}
		} else if(bor) {
			printf("-1\n");
			return 0;
		}
	}
	printf("%lld %lld\n", a, b);
}
