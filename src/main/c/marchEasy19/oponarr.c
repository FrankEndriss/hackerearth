#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int min(int a, int b) {
	if(a<b)
		return a;
	return b;
}

int abs(int i) {
	if(i<0)
		return -i;
	return i;
}

int cmpi(const void* i1, const void* i2) {
	return ( *(int*)i1 - *(int*)i2 );
}

int a[1001];

void solve() {
int n, i, j;
	scanf("%d", &n);

//printf("will scanf %d ints\n", n);
	for(i=0; i<n; i++)
		scanf("%d", &a[i]);

//printf("did scanf %d ints\n", n);

	// qsort(a, n, sizeof(int), cmpi);
//printf("did sort %d ints\n", n);

	// int mid=n/2;
	long long minsum=0;

	for(i=0; i<n; i++)  {
		long long sum=0;
		for(j=0; j<n; j++) 
			sum+=min(a[j], abs(a[j]-a[i]));
		if(i==0 || sum<minsum)
			minsum=sum;
	}

	printf("%lld\n", minsum);
}

int main(){
	//scanf("%d", &t);
	//for(i=0; i<t; i++)
		solve();
}
