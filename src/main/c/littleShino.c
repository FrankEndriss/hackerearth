
#include <stdio.h>
#include <math.h>

long long gcd(long long a, long long b) 
{ 
    if (a == 0) 
        return b; 
    return gcd(b%a, a); 
} 

/** Find common primefactors of two numbers. */
int main(int argc, char** argv) {
long long a, b;
long long g;
int k;
int i, ans;
	scanf("%lld", &a);
	scanf("%lld", &b);

	g=gcd(a, b);
	k=(int)sqrt(g);

	ans=0;
	for(i=1; i<=k; i++) {
		if(g%i==0) {
			if(g/i==i)
				ans++;
			else
				ans+=2;
		}
	}
	printf("%d\n", ans);
	
}
