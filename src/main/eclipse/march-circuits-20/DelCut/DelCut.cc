/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

#define MOD 1000000007

int mul(const int v1, const int v2, int mod = MOD) {
	return (int) ((1LL * v1 * v2) % mod);
}

int toPower(int a, int p, int mod = MOD) {
	int res = 1;
	while (p != 0) {
		if (p & 1)
			res = mul(res, a, mod);
		p >>= 1;
		a = mul(a, a, mod);
	}
	return res;
}

int inv(const int x, const int mod = MOD) {
	return toPower(x, mod - 2);
}

int fraction(const int zaehler, const int nenner) {
	return mul(zaehler, inv(nenner));
}

/* Let p= number of edges which devide the graph in two components of even size.
 * ans== p/bridges
 *
 * How to find bridges?
 * https://cp-algorithms.com/graph/bridge-searching.html
 *
 * How to find the size of the components after removing the bridge?
 * DFS would be probably to slow.
 * Using memoization...seems still to be to slow. But I do not get why. That should be O(max(n,m) log n+m)
 *
 * I we do DFS from a leaf...we can store for every edge/direction the number of nodes in that subtree.
 * For all bridges that should be the number of vertex in that subtree.
 */
void solve() {
	cini(n);
	cini(m);
	vector<map<int, int>> adj(n); /* adj[i][j]= j is a child of i, count of nodes in subtree */
	for (int i = 0; i < m; i++) {
		cini(u);
		cini(v);
		u--;
		v--;
		adj[u][v] = 0;
		adj[v][u] = 0;
	}

	vector<bool> vis(n);
	vi tin(n);
	vi low(n);
	int timer;

	vector<pii> bridges;

	function<void(int, int)> dfs = [&](int v, int p) {
		vis[v] = true;
		tin[v] = low[v] = timer++;
		for (auto to : adj[v]) {
			if (to.first == p)
				continue;
			if (vis[to.first]) {
				low[v] = min(low[v], tin[to.first]);
			} else {
				dfs(to.first, v);
				low[v] = min(low[v], low[to.first]);
				if (low[to.first] > tin[v]) {
					bridges.push_back( { v, to.first });
				}
			}
		}
	};

	timer = 0;
	vis.assign(n, false);
	tin.assign(n, -1);
	low.assign(n, -1);
	for (int i = 0; i < n; ++i) {
		if (!vis[i]) {
			dfs(i, -1);
		}
	}

	/* now count number of vertex in components. */
	function<int(int, int)> cnt = [&](int node, int parent) {
		vis[node] = true;
		int ans = 1;
		for (auto &chl : adj[node]) {
			if (!vis[chl.first]) {
				chl.second = cnt(chl.first, node);
				adj[chl.first][node] = chl.second;
				ans += chl.second;
			}
		}
		return ans;
	};

	if (n % 2) {
		if (bridges.size() == 0)
			cout << "0 0" << endl;
		else
			cout << "0 1" << endl;
	}

	vis.assign(n, false);
	cnt(0, -1);

	log(bridges.size());
	int ans = 0;
	for (pii br : bridges) {
		int cnt1 = adj[br.first][br.second];
		int cnt2 = n - cnt1;
		log(cnt1, cnt2);

		if (cnt1 % 2 == 0 && cnt2 % 2 == 0)
			ans++;
	}

	cout << fraction(ans, bridges.size()) << " " << fraction(bridges.size() - ans, bridges.size()) << endl;

}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

