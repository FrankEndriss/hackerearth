/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* First, we must find  the shift where most elements are on identity.
 * We can maintain pos array, where we maintain offsets
 * to positions where the numbers should be.
 * The biggest freq of offsets is the best shift.
 * Then, we can add/remove elements to these pos array and freq map.
 * To do this efficiently we need another set<pii> where we can
 * reference element with highest freq directly.
 *
 * And we must consider that offset costs. So not the biggest freq is
 * best, but biggest freq-offs, where offs is shift _Left_.
 */
void solve() {
	cini(n);
	vi a(n);
	map<int, int> f;	// f[i]== freq of elements with offset i

	for (int i = 0; i < n; i++) {
		cin >> a[i];
		int offs = (i + 1) - a[i];
		if (a[i] > n)
			offs = 1e9;
		if (offs < 0)
			offs += n;
		f[offs]++;
	}

	set<pair<int, pii>> s;			// set<freq,offs>== set sorted by freqencies referencing offsets
	for (auto ent : f) {
		s.insert( { ent.second - ent.first, { ent.second, ent.first } });
	}

	cini(q);

	for (int i = 0; i < q; i++) {
		cini(idx);
		idx--;
		cini(b);
		/* remove a[idx] from map and set */
		int offs = (idx + 1) - a[idx];
		if (a[idx] > n)
			offs = 1e9;
		if (offs < 0)
			offs += n;

		s.erase( { f[offs] - offs, { f[offs], offs } });
		f[offs]--;
		s.insert( { f[offs] - offs, { f[offs], offs } });

		/* add b to map and set */
		a[idx] = b;
		offs = (idx + 1) - a[idx];
		if (a[idx] > n)
			offs = 1e9;
		if (offs < 0)
			offs += n;
		s.erase( { f[offs] - offs, { f[offs], offs } });
		f[offs]++;
		s.insert( { f[offs] - offs, { f[offs], offs } });

		auto it = s.rbegin();
		int ans = it->second.second + n - it->second.first;
		ans = min(n, ans);	// no shift but changing all elements will allways work, too. ie if all a[i]>n
		cout << ans << endl;
	}
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

