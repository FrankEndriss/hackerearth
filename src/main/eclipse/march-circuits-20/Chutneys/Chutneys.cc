/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int MOD = 1e9 + 7;

/* There is a list of strings, in that list there are updates
 * of single entries.
 * On query, we have to find number of subsequences in L,R
 * where we can permute a palindrome.
 * A palindrome can be build if the freq of all letters is even,
 * exept one. ie if freq of odd freqs is max 1.
 * How to count the freqs?
 *
 * Every string is one bitset<26> (or simply an int).
 * palindrome can be made if xor over all used strings has at most one bit set.
 * There are 2^n ways to use/notuse the strings :/
 *
 * How to find number of subseqs with that property?
 * -> Try brute force first.
 * We would try all 2^n combinations.
 *
 * -> Knapsack?
 * So we would add a string or not add that string, and find if that leads to one of the 26 palindromic sets (exactly one bit set)
 *
 * To get zero set bits, we could find subsets per bit of even size...
 * There are 2^26 possible strings, we could loop throu them once, not 1e5 times.
 *
 * How would we find that sets bit by bit?
 * Loop over bits.
 * For first bit among all Strings...
 *
 * How can we find solution for at least one query of type 2?
 *
 * What about the length of the strings. two strings with same parity build a string
 * with even length, which produces a palindrome only for xorsum==0
 * Else for odd length, a plindrome is possible only for xorsum in 2^k
 *
 * What about if alphabet was only one letter, not 26?
 * -> All combinations would create 0 or 1.
 * Two letters:
 * -> All combinations, but the ones createing 11...
 * How does this help?
 *
 * ******
 * Somehow we need to find a way to _not_ enumerate all combinations, since that number is
 * simply huge. But how can we combine groups of partitially solutions?
 *
 * Still, how to answer at least one query, ie where L==1 and R==n.
 *
 * ********
 * We maintain the list of strings, and add one. This contributes to the solution if the possible xorsums of the
 * list include one of the 27 sums which form a palindrome with the new one.
 * So, we would run through all the combinations (which are 2^26 at most) to check them.
 * ... does not work either ;)
 * *********
 * How to find number of combinations where xorsum in 0, 2^k for a list of bitset<26>?
 *
 * What about sorting the strings? Reordering obviously does not change the result.
 */
void solve() {
	cini(n);
	cini(q);

	vector<bitset<26>> bs(n);

	for (int i = 0; i < n; i++) {
		cins(s);
		for (char c : s)
			bs[i].flip(c - 'a');
	}

	for (int i = 0; i < q; i++) {
		cini(t);
		if (t == 1) {
			cini(idx);
			idx--;
			cins(s);
			bs[idx].reset();
			for (char c : s)
				bs[idx].flip(c - 'a');

		} else if (t == 2) {
			cini(L);
			cini(R);
			L--;
			R--;
			/* brute force */
			/*
			 const int cntSets = R - L + 1;
			 if (cntSets > 20) {
			 cout << "42" << endl;
			 continue;
			 }

			 int ans = 0;
			 for (int mask = 1; mask < (1 << cntSets); mask++) {
			 bitset<26> bset;
			 for (int b = 0; b < cntSets; b++) {
			 if (mask & (1 << b))
			 bset ^= bs[L + b];
			 }
			 int res = bset.count();
			 if (res == 0 || res == 1)
			 ans++;
			 }

			 cout << ans << endl;
			 end brute force ****/

			/* recursive brute force, less xors */
			function<int(int, bitset<26>)> count = [&](int idx, bitset<26> pre) {
				if (idx > R)
					return (int) (pre.count() < 2);

				return (count(idx + 1, pre) + count(idx + 1, pre ^ bs[idx])) % MOD;
			};

			bitset<26> b;
			int ans = count(L, b);
			cout << ans - 1 << endl;

		} else
			assert(false);
	}
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

