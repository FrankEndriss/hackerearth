/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<vb> vvb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;
typedef vector<vll> vvll;

/* D L U R */
const vector<pii> offs = { { 1, 0 }, { 0, -1 }, { -1, 0 }, { 0, 1 } };

void solve() {
	cinll(n);
	vvi b(n, vi(n));
	pii pma = { 0, 0 };
	int ma = -1;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			cin >> b[i][j];
			if (b[i][j] > ma) {
				pma = { i, j };
				ma = b[i][j];
			}
		}
	}

	//const ll MA = LLONG_MAX - 1e15;	// to much
	//const ll MA = LLONG_MAX - 2e15;	// ok
	// orig: const ll MA = LLONG_MAX - 1e15 - 6e14 - 8e13 - 1e12 - 4e11 - 9e10;

	/***********************987654321 */
	const ll MA = 9221690546954775808LL;
	log(MA);

	int dir = -1; /* left to right */
	int col = n - 1;
	vector<pii> ans;

	/* note we can take the sum of all used b[i][j] and shift the path by 1 if it gets better.
	 * so we maintain a lidx and ridx for ans.
	 * Kind of two-pointer.
	 * But still we would like to know the exact limit of MA.
	 **/
	ll res = 0;
	ll mul = 1;
	ll sum = 0;
	bool fini = false;
	for (int i = 0; !fini && i < n; i++) {
		for (int j = 0; !fini && j < n; j++) {
			ll val = MA - res;
			if (val <= b[i][j] * mul) {
				fini = true;
				break;
			}
			sum += b[i][j];
			res += b[i][j] * mul;
			mul++;
			ans.emplace_back(i + 1, col + 1);
			col += dir;
		}
		col -= dir;
		dir *= -1;
	}
	cout << ans.size() << endl;
	for (pii a : ans)
		cout << a.first << " " << a.second << endl;

}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}
