/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<vb> vvb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int MOD = 1e9 + 7;

void solve() {
	cini(n);
	cini(k);

	map<int, int> p2map;
	for (int i1 = 0; i1 < 4; i1++) { /* 5 positions, 4 colors */
		for (int i2 = 0; i2 < 4; i2++) { /* 5 positions, 4 colors */
			for (int i3 = 0; i3 < 4; i3++) { /* 5 positions, 4 colors */
				for (int i4 = 0; i4 < 4; i4++) { /* 5 positions, 4 colors */
					for (int i5 = 0; i5 < 4; i5++) { /* 5 positions, 4 colors */
						int c = (1 << (i1 * 8)) + (1 << (i2 * 8)) + (1 << (i3 * 8)) + (1 << (i4 * 8)) + (1 << (i5 * 8));
						p2map[c]++;
					}
				}
			}
		}
	}

	map<int, map<int, int>> combi; // combi[i][j]==true -> pattern i and j can be combined
	for (auto enti : p2map) {
		for (auto entj : p2map) {
			bool ok = true;
			for (int l = 0; l < 4 && ok; l++) {
				if (((enti.first >> (l * 8)) & 0xFF) + ((entj.first >> (l * 8)) & 0xFF) > k)
					ok = false;
			}
			combi[enti.first][entj.first] = ok;
		}
	}

	map<int, int> ans;	// first column
	for (auto ent : p2map) {
		bool ok = true;
		for (int l = 0; l < 4 && ok; l++) {
			if (((ent.first >> (l * 8)) & 0xFF) > k)
				ok = false;
		}
		if (ok)
			ans[ent.first] = ent.second;
	}

	for (int i = 1; i < n; i++) {	// for all columns starting at 1
		map<int, int> ans2;
		for (auto enta : ans) {
			for (auto entp : p2map) {
				if (combi[enta.first][entp.first]) {
					ans2[entp.first] += (1LL * enta.second * entp.second) % MOD;
					ans2[entp.first] %= MOD;
				}
			}
		}
		swap(ans, ans2);
	}

	int ansl = 0;
	for (auto ent : ans) {
		ansl += ent.second;
		ansl %= MOD;
	}
	cout << ansl << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

