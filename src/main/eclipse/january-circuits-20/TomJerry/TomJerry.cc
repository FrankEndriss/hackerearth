/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* @return true if the beginner (Tom) wins
 * if played on a pile of size x.
 */
bool iswin(int x, int k) {
	return x % (k + 1) != 0;
}

void init(int k) {
	/*
	 vi win(100);
	 for (int i = 0; i < 100; i++) {
	 if (i == 0)
	 win[i] = false;
	 else if (i <= k)
	 win[i] = true;
	 else {
	 for (int j = 1; j <= k; j++) {
	 if (!win[i - j]) {
	 win[i] = true;
	 break;
	 }
	 }
	 }
	 }
	 for (int i = 0; i < 100; i++) {
	 cout << win[i];
	 }
	 cout << endl;
	 */

}

template<typename E>
class SegmentTree {
private:
	vector<E> tree;
	E neutral;
	function<E(E, E)> cumul;

	int treeDataOffset;
	const int ROOT = 1;

	inline int leftChild(const int treeIdx) {
		return treeIdx * 2;
	}
	inline int rightChild(const int treeIdx) {
		return treeIdx * 2 + 1;
	}
	inline int parent(const int treeIdx) {
		return treeIdx / 2;
	}
	inline bool isOdd(const int idx) {
		return (idx & 0x1);
	}

public:
	/** SegmentTree. Note that you can hold your data in your own storage and give
	 * an Array of indices to a SegmentTree.
	 * @param beg, end The initial data.
	 * @param pNeutral the Element neutral to cumul, ie 0 for sum, 1 for product etc
	 * @param pCumul The cumulative function to create the "sum" of two nodes.
	 **/
	template<typename Iterator>
	SegmentTree(Iterator beg, Iterator end, E pNeutral, function<E(E, E)> pCumul) {
		neutral = pNeutral;
		cumul = pCumul;
		treeDataOffset = (int) distance(beg, end);
		tree = vector<E>(treeDataOffset * 2, pNeutral);

		int i = treeDataOffset;
		for (auto it = beg; it != end; it++)
			tree[i++] = *it;

		for (int j = treeDataOffset - 1; j >= 1; j--)
			tree[j] = cumul(tree[leftChild(j)], tree[rightChild(j)]);
	}

	/** Updates all elements in interval(idxL, idxR].
	 * iE add 42 to all elements from 4 to inclusive 7:
	 * update(4, 8, [](int i1) { return i1+42; });
	 */
	void updateRange(int pIdxL, int pIdxR, function<E(E)> apply) {
		pIdxL += treeDataOffset;
		pIdxR += treeDataOffset;
		for (int i = pIdxL; i < pIdxR; i++)
			tree[i] = apply(tree[i]);

		while (pIdxL != ROOT) {
			pIdxL = parent(pIdxL);
			pIdxR = max(pIdxL, parent(pIdxR - 1));
			for (int i = pIdxL; i <= pIdxR; i++)
				tree[i] = cumul(tree[leftChild(i)], tree[rightChild(i)]);
		}
	}

	/** Updates the data at dataIdx to value returned by apply. */
	void updateApply(int dataIdx, function<E(E)> apply) {
		updateSet(dataIdx, apply(get(dataIdx)));
	}

	/** Updates the data at dataIdx by setting it to value. */
	void updateSet(int dataIdx, E value) {
		int treeIdx = treeDataOffset + dataIdx;
		tree[treeIdx] = value;

		while (treeIdx != ROOT) {
			treeIdx = parent(treeIdx);
			tree[treeIdx] = cumul(tree[leftChild(treeIdx)], tree[rightChild(treeIdx)]);
		}
	}

	/** @return value at single position, O(1) */
	E get(int idx) {
		return tree[idx + treeDataOffset];
	}

	/** @return the cumul(idxL, idxR], iE idxL inclusive, idxR exclusive. */
	E get(int pIdxL, int pIdxR) {
		int idxL = pIdxL + treeDataOffset;
		int idxR = pIdxR + treeDataOffset;
		E cum = neutral;
		while (idxL < idxR) {
			if (isOdd(idxL)) { // left is odd
				cum = cumul(cum, tree[idxL]);
				idxL++;
			}
			if (isOdd(idxR)) {
				idxR--;
				cum = cumul(cum, tree[idxR]);
			}
			idxL = parent(idxL);
			idxR = parent(idxR);
		}
		return cum;
	}

};
void solve() {
	cini(q);
	cini(k);
	//init(k);

	/* A single game is lose if pale size mod (k+1) eq 0, else it is win.
	 * Note that this is simply the Nim game, so Nim theory applies.
	 *
	 * A set of games is loose if the Nim sum eq 0.
	 * Nim sum is the xor of all states, in our case the size of the piles (mod k+1).
	 * see https://en.wikipedia.org/wiki/Nim
	 *
	 * So basically, we need to maintain this sum and be able to do
	 * range updates (UR), where a given increment is applied to
	 * a range of piles.
	 *
	 * Applying an increment x to a range of piles changes the xor of that
	 * range by x or by 0 depending on the parity of the range size.
	 *
	 * So after haveing applied y changes, the xor of a range R is
	 * affected by those of the y ranges which intersect with R in
	 * an odd number of positions.
	 * The interaction is that, that the increment of the change has to
	 * be xored once.
	 *
	 * To answer Q1
	 * we need to know how much piles in range qL,qR are 0,
	 * or are not 0.
	 * ???That for we maintain a segtree<vi(6)> including all single updates. ???
	 *
	 * To answer Q2
	 * we need to find the xor of all changes which affect the given
	 * range an odd number of times.
	 * The changes are the ones where intersecion size is odd, as
	 * said before.
	 *
	 * We maintain the changes as kind of events in segtree.
	 * This is, we insert the uL,uR both with the same value (since xor).
	 * Note that index is not uR+1.
	 * We use four segtrees, one for odd indices, one for even. This allows
	 * us to separatly query them.
	 *
	 * Handling of the changes:
	 * We need to distinguish cases of intersection. Let qL,qR be
	 * the query range, and uL,uR be the range of updates.
	 * With given qL and qR, we need to find the sum of relevant
	 * uL,uR from the segtrees.
	 * There are intersections of four "overlapping" types
	 * 1. uL<=qL<=uR<=qR
	 * 2. qL<=uL<=qR<=uR
	 * 3. qL<=uL<=uR<=qR
	 * 4. uL<=qL<=qR<=uR
	 *
	 * And four query types, qL,qR:
	 * 1. odd/odd
	 * 2. odd/even
	 * 3. even/odd
	 * 4. even/even
	 *
	 * odd/odd, ex(1,3)
	 * We need to add odd/uL within range, and even/uR to remove if size is even.
	 * odd/even, ex(1,4)
	 * We need to add odd/uL within range, and even/uR to remove if size is even.	Same???
	 *
	 * even/odd, ex(2,5)
	 * We need to add even/uL withing Range, and odd/uR to remove if size is even.
	 * even/even, ex(2,4)
	 * We need to add even/uL withing Range, and odd/uR to remove if size is even. Same.
	 *
	 * ...
	 * Ok, to get at least some points we implement O(N) updates.
	 * Using these we do simply maintain a single segtree<vi(k+1)>
	 *
	 **********************
	 * Again, idea 2.0:
	 * On (range) update we do evt add, this is for ie x==3, we add { 0,0,1,...,0} on left index, and
	 * { 0,0,-1,...0} on r+1 index.
	 * On query, we query twice. Once for 0,l, once for 0,r+1, and subtract, so we get value for that range.
	 * This answer Q1.
	 *
	 * We maintain k+1 SegmentTree<vi(k+1)>, where segtree[j] is segtree[0] shifted by j, ie j added
	 * On range update l,r,x,
	 * -we select number of piles from every segtree for range l,r
	 * -we add that numbers to the segtrees shifted by x
	 *
	 * **********************
	 * Again again, 2.1:
	 * We maintain k+1 SegmentTree<int>, where segtree[j] maintains the number of piles
	 * with j cheeses.
	 * On update, we do
	 * updateApply(l, []{+1})
	 * updateApply(r+1, []{-1})
	 * So, on range update, we can query the number of piles on every segtree, and
	 * "move" them in between.
	 */

	const int N = 200008;

	vi data(N);
	int first = N / 2;
	int pales = 0;

	function<vi(vi, vi)> myadd = [&](vi v1, vi v2) {
		vi ret(k + 1);
		for (size_t i = 0; i < ret.size(); i++)
			ret[i] = v1[i] + v2[i];
		return ret;
	};

	function<int(int, int)> myadd2 = [&](int v1, int v2) {
		return v1 + v2;
	};

	SegmentTree<int> segaI(data.begin(), data.end(), 0, myadd2);
	vector<SegmentTree<int>> sega(k + 1, segaI);

	function<int(int)> inc = [](int i) {
		return i + 1;
	};
	function<int(int)> dec = [](int i) {
		return i - 1;
	};

	for (int i = 0; i < q; i++) {
		cins(s);
		//cout << s << endl;
		if (s == "AL") {
			cini(x);
			first--;
			pales++;
			x %= (k + 1);
			sega[x].updateSet(first, 1);

		} else if (s == "AR") {
			cini(x);
			x %= (k + 1);
			sega[x].updateSet(first + pales, 1);
			pales++;

		} else if (s == "UP") {
			cini(idx);
			cini(x);
			idx--;
			x %= (k + 1);
			for (int i = 0; i < k + 1; i++) {
				int oldX = sega[i].get(first, first + idx + 1) - sega[i].get(first, first + idx);
				assert(oldX >= 0 && oldX < 2);
				if (oldX) {
					if (i != x) {
						sega[i].updateApply(first + idx, dec);
						sega[i].updateApply(first + idx + 1, inc);
						sega[x].updateApply(first + idx, inc);
						sega[x].updateApply(first + idx + 1, dec);
					}
					break;
				}
			}
		} else if (s == "UR") {
			cini(l);
			cini(r);
			cini(x);
			l--;
			r--;
			x %= (k + 1);

			vi cnt(k + 1);
			for (int i = 0; i < k + 1; i++) {
				cnt[i] = sega[i].get(first, first + r + 1) - sega[i].get(first, first + l);
			}

		} else if (s == "Q1") {
			cini(l);
			cini(r);
			l--;
			r--;

			vi v = sega.get(first + l, first + r + 1);
			int ans = 0;
			/*
			 cout << "v=";
			 for (int i : v)
			 cout << i << " ";
			 cout << endl;
			 */

			for (int i = 1; i < k + 1; i++)
				ans += v[i];

			cout << ans << endl;
		} else if (s == "Q2") {
			cini(l);
			cini(r);
			l--;
			r--;

			int ans = segx.get(first + l, first + r + 1);

			if (ans % (k + 1))
				cout << "Tom" << endl;
			else
				cout << "Jerry" << endl;
		}

		/*
		 cout << "q" << i + 1 << " pales=" << pales << endl;
		 for (int j = first; j < first + pales; j++) {
		 vi val = sega.get(j);
		 cout << " v[" << j + 1 - first << "]=";
		 for (int lv : val)
		 cout << lv << " ";
		 cout << endl;
		 }
		 */
	}
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

