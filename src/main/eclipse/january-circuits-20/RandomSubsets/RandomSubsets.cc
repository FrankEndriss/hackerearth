/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

using namespace __gnu_pbds;
typedef tree<pii, null_type, less<pii>, rb_tree_tag, tree_order_statistics_node_update> indexed_set;
#define MOD 1000000007

int mul(const int v1, const int v2, int mod = MOD) {
	return (int) ((1LL * v1 * v2) % mod);
}

int toPower(int a, int p, int mod = MOD) {
	int res = 1;
	while (p != 0) {
		if (p & 1)
			res = mul(res, a, mod);
		p >>= 1;
		a = mul(a, a, mod);
	}
	return res;
}

int pl(int v1, int v2, int mod = MOD) {
	int res = v1 + v2;

	if (res < 0)
		res += mod;

	else if (res >= mod)
		res -= mod;

	return res;
}

int inv(const int x, const int mod = MOD) {
	return toPower(x, mod - 2);
}

int fraction(const int zaehler, const int nenner) {
	return mul(zaehler, inv(nenner));
}

void smerge(indexed_set &s1, indexed_set &s2) {
	if (s1.size() < s2.size())
		s1.swap(s2);

	for (auto i : s2)
		s1.insert(i);
}

/* number of possible nonempty sets created by x nodes, which is simply (2^x)-1 ) */
int se(int x) {
	if (x == 0)
		return 0;
	return pl(toPower(2, x), -1);
}

void solve() {
	cini(n);
	vvi adj(n);
	for (int i = 0; i < n - 1; i++) {
		cini(u);
		cini(v);
		u--;
		v--;
		adj[u].push_back(v);
		adj[v].push_back(u);
	}
	cinai(val, n);

	/* We need to find for any node u the number of subsets where node u
	 * is lca and all nodes of that set val less than node u.
	 * ans+= num*(val[u]-1)
	 *
	 * All other sets have score 1.
	 * One set has score 0.
	 *
	 * So we need to find for any subtree the number of nodes in every direct child
	 * of the subtree whichs * val[v] < val[subtreeroot].
	 * Sum of all products of these numbers is number of paths.
	 *
	 * We can use an "gnu indexed set" holdings pairs of <val[u],id>
	 * Then order_of_key({val[u],-1}) to get number of smaller vals.
	 *
	 * How to efficiently get get product of all pairs for node with huge num of childs?
	 * -> It is sum of all childs multipled by
	 */

	int gans = 0;
	function<void(int, int, indexed_set&)> dfs = [&](int node, int parent, indexed_set &ans) {
		ans.insert( { val[node], node });
		vi chlcnt;
		int sum = 0;
		for (int chl : adj[node]) {
			if (chl == parent)
				continue;
			indexed_set chl_set;
			dfs(chl, node, chl_set);
			chlcnt.push_back(chl_set.order_of_key( { val[node], -1 }));
			sum += chlcnt.back();
			smerge(ans, chl_set);
		}

		/* Now calc how much subsets exist in this subtree where node is lca
		 * and all nodes of the set have val less than val[node].
		 * Basically we need to connect every possible nonempty set createable by the nodes
		 * of one child whith all possible nonemtpy sets createable by the ones of the other
		 * childs.
		 *
		 * let se(x) be the number of possible nonempty sets build of x nodes, then
		 * it is:
		 * sum(se(chlcnt[i=0..n]) * se(sum-chlcnt[i]))
		 **/
		ll prod = 0;
		for (int i : chlcnt) {
			sum -= i;
			prod = pl(prod, mul(se(i), se(sum)));
		}

		prod = mul(prod, node); /* really, we need to use node id as multiplicator */
		gans = pl(gans, prod);
		//cout << "prod=" << prod << " gans=" << gans << endl;
	};

	indexed_set rset;
	dfs(0, -1, rset);

	gans = pl(gans, se(n));

	int fans = toPower(fraction(1, 2), n);
	fans = mul(fans, gans);

	//cout << "gans=" << gans << " sen=" << sen << endl;
	cout << fans << endl;

}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

