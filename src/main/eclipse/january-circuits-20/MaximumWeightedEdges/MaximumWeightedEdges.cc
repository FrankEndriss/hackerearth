/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

#define gc() getchar_unlocked()

/* see https://www.geeksforgeeks.org/fast-io-for-competitive-programming/ */
inline void fs(int &number) {
	int c = gc();
	while (c == ' ' || c == 10)
		c = gc();

	number = 0;
	while (c >= '0' && c <= '9') {
		number = number * 10 + c - '0';
		c = gc();
	}
}

const char digits[] =
		"00010203040506070809101112131415161718192021222324252627282930313233343536373839404142434445464748495051525354555657585960616263646566676869707172737475767778798081828384858687888990919293949596979899";

#define pc(x) putchar_unlocked(x);
inline void writeInt(int n) {
	char buf[32];
	int idx = 0;

	if (n == 0) {
		pc('0');
		return;
	}

	while (n >= 100) {
		ll val = n % 100;
		buf[idx++] = digits[val * 2 + 1];
		buf[idx++] = digits[val * 2];
		n /= 100;
	}

	while (n) {
		buf[idx++] = n % 10 + '0';
		n /= 10;
	}

	while (idx--) {
		pc(buf[idx]);
	}
	pc(' ');
}

void solve() {
	int n, m;
	fs(n);
	fs(m);

	vvi adj(n);
	for (int i = 0; i < m; i++) {
		int u, v;
		fs(u);
		fs(v);
		u--;
		v--;
		adj[u].push_back(v);
		adj[v].push_back(u);
	}
	vi ans(n);
	for (int i = 0; i < n; i++)
		fs(ans[i]);

	/* check random swaps, swap whenever the result increases */
	const int extra = 200000 - n - m;
	const int CNT = 200000000 + extra * 3000;
	for (int i = 0; i < CNT; i++) {
		int v1 = rand() % n;
		int v2;
		do {
			v2 = rand() % n;
		} while (v1 == v2);

		ll sumO = 0;
		ll sumN = 0;
		for (int chl : adj[v1]) {
			i++;
			sumO += max(ans[v1], ans[chl]);
			sumN += max(ans[v2], ans[chl]);
		}
		for (int chl : adj[v2]) {
			i++;
			sumO += max(ans[v2], ans[chl]);
			sumN += max(ans[v1], ans[chl]);
		}
		if (sumN > sumO)
			swap(ans[v1], ans[v2]);
	}

	for (int i = 0; i < n; i++)
		writeInt(ans[i]);
	pc('\n');
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

