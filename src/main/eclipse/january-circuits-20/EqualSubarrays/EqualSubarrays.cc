/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
	cini(n);
	cini(k);
	cinai(a, n);

	int l = 0;
	map<int, int> s;
	ll ssum = 0;	// sum of elements in s
	int scnt = 0;
	int ans = 0;
	for (int r = 0; r < n; r++) {
		s[a[r]]++;
		ssum += a[r];
		scnt++;
		ll ma = s.rbegin()->first;
		//cout << "scnt=" << scnt << " ma=" << ma << endl;
		while (l < r && ssum + k < scnt * ma) {
			auto it = s.find(a[l]);
			it->second--;
			ssum -= it->first;
			scnt--;
			if (it->second == 0)
				s.erase(it);
			l++;
			ma = s.rbegin()->first;
		}
		ans = max(ans, scnt);
	}
	cout << ans << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

