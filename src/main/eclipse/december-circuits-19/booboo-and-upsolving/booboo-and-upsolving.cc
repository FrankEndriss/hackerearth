/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = 3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
	cini(n);
	cini(m);
	vll a(n);
	ll sum = 0;
	ll ma = 0;
	ll mi = 1e9;
	;
	fori(n)
	{
		cin >> a[i];
		sum += a[i];
		ma = max(ma, a[i]);
		mi = min(mi, a[i]);
	}

	ll lo = mi * (n / m);
	ll up = ma * (n / m + 1);
#ifdef DEBUG
	cout << "lo=" << lo << " up=" << up << endl;
#endif
	while (lo + 1 < up) {
		ll mid = (lo + up) / 2;

		int lsum = 0;
		int cnt = 0;
		for (int i = 0; i < n; i++) {
			if (lsum + a[i] > mid) {
				lsum = a[i];
				cnt++;
			} else
				lsum += a[i];
		}
		if (lsum > 0)
			cnt++;

#ifdef DEBUG
		cout << "cnt=" << cnt << " lo=" << lo << " up=" << up << " mid=" << mid << endl;
#endif

		if (cnt <= m)
			up = mid;
		else
			lo = mid;
	}
	cout << up << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

