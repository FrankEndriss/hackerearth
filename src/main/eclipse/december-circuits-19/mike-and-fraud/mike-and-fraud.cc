/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = 3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void pfac(int d, vi &ans) {
	for (int j = 2; j * j <= d; j++) {
		while (d % j == 0) {
			ans.push_back(j);
			d /= j;
		}
	}
	if (d > 1)
		ans.push_back(d);
}

void solve() {
	cini(n);
	cini(k);
	cinai(a, n);

	vvi ad(n);	// ad[i]==divisiors of a[i]
	/* could be faster using precalculated primes */
	for (int i = 0; i < n; i++)
		pfac(a[i], ad[i]);

	vi kd;
	pfac(k, kd);
	map<int, int> kf;	// kf[i]==j;  =>  i is contained j times
	for (int aux : kd)
		kf[aux]++;

	/* Now find maximum subarrays of ad where not all
	 * primefactors in kd are contained.
	 * Use two pointer.
	 * ans is number of all subarrays - number of subarrays with to less factors.
	 */
	int l = 0;
	int r = 0;
	map<int, int> f;		// all factors contained in (l,r]
	vector<pii> inter;	// minimum intervals containing all factors of kf
	while (l < n) {
		/* increment r until all factors of kf are in f */
		bool notfound = true;
		while (r < n && notfound) {
			for (int aux : ad[r])
				f[aux]++;
			r++;

			notfound = false;
			for (auto ent : kf) {
				if (f[ent.first] < ent.second) {
					notfound = true;
					break;
				}
			}
		}

		/* increment l until not all factors of kf are n f */
		bool fini = false;
		while (l <= r && !fini) {
			for (int aux : ad[l])
				f[aux]--;
			l++;

			for (auto ent : kf) {
				if (f[ent.first] < ent.second) {
					fini = true;
					break;
				}
			}
		}
		//cout << "l=" << l - 1 << " r=" << r << endl;
		inter.push_back( { l - 1, r });
	}
	if (inter.size() == 0) {
		cout << 0 << endl;
		return;
	}

	/* now find the 'spaces' between the intervals */
	ll ans = 0;
	l = 0;
	for (size_t i = 0; i < inter.size(); i++) {
		r = inter[i].first;
		if (l < r) {
			int aux = r - l;
			ans += aux * (aux + 1) / 2;
		}
		l = inter[i].second;
	}
	r = n;
	if (l < r) {
		int aux = r - l;
		ans += aux * (aux + 1) / 2;
	}

	ans -= n * (n + 1) / 2;
	ans = -ans;
	cout << ans << endl;

}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

