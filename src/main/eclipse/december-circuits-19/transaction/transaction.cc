/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = 3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

template<typename T>
class fenwick {
public:
	vector<T> fenw;
	int n;

	fenwick(int _n) :
			n(_n) {
		fenw.resize(n);
	}

	void modify(int x, T v) {
		while (x < n) {
			fenw[x] += v;
			x |= (x + 1);
		}
	}

	/* get sum of range (0,x), including x */
	T get(int x) {
		T v { };
		while (x >= 0) {
			v += fenw[x];
			x = (x & (x + 1)) - 1;
		}
		return v;
	}
};

template<typename T>
class fenwick2d {
public:
	vector<fenwick<T>> fenw2d;
	int n, m;

	fenwick2d(int x, int y) :
			n(x), m(y) {
		fenw2d.resize(n, fenwick<int>(m));
	}

	void modify(int x, int y, T v) {
		while (x < n) {
			fenw2d[x].modify(y, v);
			x |= (x + 1);
		}
	}

	T get(int x, int y) {   // range 0..x/y, including x/y
		x = min(x, n - 1);
		y = min(y, m - 1);
		T v { };
		while (x >= 0) {
			v += fenw2d[x].get(y);
			x = (x & (x + 1)) - 1;
		}
		return v;
	}
};

const int N = 100007;
/* we need to use a 2D fenwick to query the values.
 * Maybe it would not have to be fenwick in first place, but
 * simple prefix sums somehow in 2D.
 */
void solve() {
	cini(t);
	cini(q);

	fenwick2d<int> fw(N, t);
	vi c(t);
	for (int i = 0; i < t; i++) {
		cin >> c[i];
		fw.modify(c[i], i, 1);
	}

	for (int i = 0; i < q; i++) {
		cini(m);
		cini(n);
		/* Now do binary search on fw, finding the max k of (0,k] where
		 * n-1 entries are returned.
		 * Then N==(k+1)
		 */
		int lo = 0;
		int hi = N;
		while (lo + 1 < hi) {
			int mid = (lo + hi) / 2;
			int cnt = fw.get(N, mid + 1) - fw.get(m, mid + 1);
			if (cnt > n - 1)
				hi = mid;
			else
				lo = mid;
		}

		if (lo + 1 == N)
			cout << -1 << endl;
		else
			cout << c[lo + 1] << endl;
	}

}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

