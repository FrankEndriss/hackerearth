/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = 3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int cntb(int i) {
	return __builtin_popcount(i);
}

void solve() {
	cini(n);
	cini(m);
	cini(q);
	vi x(q);
	vi y(q);
	for (int i = 0; i < q; i++)
		cin >> x[i] >> y[i];

	/* possible paths per coordinate */
	int pn = 1 << (cntb(n) - 1);
	int pm = 1 << (cntb(m) - 1);

	/* How many paths are unuseable by an ostacle?
	 * Or better, how many obstacles are in one path?
	 * How many paths lead to a given obstacle? -> same calculation as n,m
	 * How many paths include two given obstacles?
	 *
	 * Paths from one obstacle to another exist if both x and y are supermasks of
	 * the other. Then again the same calculation applies.
	 **/

}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

