/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = 3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
	cind(x1);
	cind(y1);	// y1<0, ie on land
	cind(x2);
	cind(y2);
	cind(v1);	// speed at neg
	cind(v2);	// speed at pos

	double mi = min(x1, x2);
	double ma = max(x1, x2);
	while (mi + 0.0000001 < ma) {
		double m2 = (mi + ma) / 2;
		double m1 = (mi + m2) / 2;
		double m3 = (ma + m2) / 2;

		double t1 = hypot(x1 - m1, y1) / v1 + hypot(x2 - m1, y2) / v2;
		double t2 = hypot(x1 - m2, y1) / v1 + hypot(x2 - m2, y2) / v2;
		double t3 = hypot(x1 - m3, y1) / v1 + hypot(x2 - m3, y2) / v2;

		if (t1 <= t2 && t2 <= t3) {	// t1 smallest
			ma = m2;
		} else if (t3 <= t2 && t2 <= t1) {	// t3 smalles
			mi = m2;
		} else { // t2 smallest
			mi = m1;
			ma = m2;
		}
	}

	double ans = hypot(x1 - mi, y1) / v1 + hypot(x2 - mi, y2) / v2;
	cout << setprecision(5) << fixed << ans << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	cini(t);
	while (t--)
		solve();
}

