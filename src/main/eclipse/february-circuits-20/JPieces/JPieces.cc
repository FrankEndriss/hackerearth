/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<int, ll> pll;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* Friends distance is limited to 1000 meter
 * and tree nodes are max 1000.
 * So, we create a tree dp.
 * For every subtree, we find  a list of pair<distance,worth>
 * So we create a dp[node][i]== <distance,worth>, sorted by distance
 * We should be able to build this for the whole tree bottom up.
 */

const int MAXDIST = 1001;
const ll INF = 1e18;

void solve() {
	cini(n);	// num cities
	cini(m);	// num jews
	cini(k);	// num friends

	cinai(km, k);	// friends max distance
	int maxdist = 0;
	for (int i = 0; i < k; i++) {
		assert(km[i] < MAXDIST);
		maxdist = max(maxdist, km[i]);
	}

	vector<vector<pii>> adj(n);
	for (int i = 0; i < n - 1; i++) {
		cini(a);
		cini(b);
		cini(l);
		a--;
		b--;
		if (l * 2 <= maxdist) {
			adj[a].emplace_back(b, l);
			adj[b].emplace_back(a, l);
		}
	}

	vll c(n);	// c[i]==worth of city i
	for (int i = 0; i < m; i++) {
		cini(cit);
		cinll(val);
		c[cit - 1] += val;
	}

	/* places the combination of vectors of cost/value of sub in ans */
	function<void(vector<vector<pll>>&, vector<pll>&, ll)> maxValue = [](vector<vector<pll>> &sub, vector<pll> &ans, ll zeroval) {
		vll ans2(MAXDIST);
		ans2[0] = zeroval;

		for (size_t i = 0; i < sub.size(); i++) {
			vll ans3 = ans2;
			for (pll p : sub[i]) {
				for (int idx = p.first; idx < MAXDIST; idx++) {
					//if (ans2[idx - p.first] > 0 && p.second > 0)
					ans3[idx] = max(ans3[idx], ans2[idx - p.first] + p.second);
				}
			}
			ans2.swap(ans3);
		}

		ans.clear();
		int prev = -1;
		for (int i = 0; i < MAXDIST; i++) {
			if (ans2[i] > prev) {
				ans.emplace_back(i, ans2[i]);
				prev = ans2[i];
			}
		}
	};

	function<void(int, int, vector<pll>&)> dfs = [&](int node, int parent, vector<pll> &ans) {
		vector<vector<pll>> chlcv;

		for (pii chl : adj[node]) {
			if (chl.first == parent)
				continue;

			chlcv.resize(chlcv.size() + 1);
			dfs(chl.first, node, chlcv.back());

			for (size_t i = 0; i < chlcv.back().size(); i++)
				chlcv.back()[i].first += 2 * chl.second;

			while (chlcv.back().size() > 0 && chlcv.back().back().first >= MAXDIST)
				chlcv.back().pop_back();
		}

		maxValue(chlcv, ans, c[node]);
	};

	vector<pll> ans;
	dfs(0, -1, ans);

	int idx = 0;
	vll ans2(MAXDIST, c[0]);

	for (pll p : ans) {
		for (int i = p.first; i < MAXDIST; i++) {
			ans2[i] = max(ans2[i], p.second);
		}
	}

#ifdef DEBUG
	cout << "ans vector, size()=" << ans.size() << endl;
	for (pll p : ans)
		cout << "dist=" << p.first << " worth=" << p.second << endl;

	for (int i = 0; i < 22; i++) {
		cout << "ans2[" << i << "]=" << ans2[i] << endl;
	}
#endif
	for (int i = 0; i < k; i++) {
		cout << ans2[km[i]] << " ";
	}
	cout << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}
