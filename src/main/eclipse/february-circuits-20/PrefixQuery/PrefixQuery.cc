/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
	cini(n);
	cini(q);

	cins(s);
	assert(s.size() == n);
	vvi idx2(n);	// idx2[i]=list of lengths of prefixes ending in s[i]
	vvi idx3(n + 1); // reverse index to idx2, idx3[i]=list of indexes where prefixes of len i are ending.

	//idx2[0].push_back(1);
	for (int i = 0; i < n; i++) {
		if (s[i] == s[0]) {
			idx2[i].push_back(1);
			idx3[1].push_back(i);
		}

		if (i > 0) {
			for (int j : idx2[i - 1]) {
				if (s[j] == s[i]) {
					idx2[i].push_back(j + 1);
					idx3[j + 1].push_back(i);
				}
			}
		}
	}

	function<int(vi&, vi&)> biggestCommon = [&](vi &v1, vi &v2) {
		vb b(s.size());
		for (int i : v1)
			b[i] = true;

		for (int i = 0; i < v2.size(); i++)
			if (b[v2[v2.size() - i - 1]])
				return v2[v2.size() - i - 1];

		return 0;
	};

	for (int i = 0; i < q; i++) {
		cini(t);
		if (t == 1) {
			n++;
			cins(s0);
			s += s0;

			idx2.resize(idx2.size() + 1);
			idx3.resize(idx3.size() + 1);

			if (s[n - 1] == s[0]) { // new prefix of size 1
				idx2[n - 1].push_back(1);
				idx3[1].push_back(n - 1);
			}

			if (n > 1) {
				for (int j : idx2[n - 2]) {
					if (s[n - 1] == s[j]) {
						idx2[n - 1].push_back(j + 1);
						idx3[j + 1].push_back(n - 1);
					}
				}
			}

		} else if (t == 2) {
			cini(x);
			x--;
			cini(y);
			y--;

			int ans = 0;
			if (x == y) {
				ans = x;
			} else {
				/* find biggest common value in idx2[x] and idx2[y] */
				if (idx2[x].size() < idx2[y].size())
					ans = biggestCommon(idx2[x], idx2[y]);
				else
					ans = biggestCommon(idx2[y], idx2[x]);
			}

			cout << ans << endl;
		} else if (t == 3) {
			cini(p);
			cini(l);
			l--;
			cini(r);
			r--;
			int ans = 0;

			auto it1 = lower_bound(idx3[p].begin(), idx3[p].end(), l + p - 1);
			auto it2 = upper_bound(idx3[p].begin(), idx3[p].end(), r);
			ans = distance(it1, it2);

			/*
			 // wrong. why?
			 for (int j : idx3[p]) {
			 if (j + 1 - p >= l && j <= r) {
			 ans++;
			 }
			 }
			 */

			// ok, but should produce same ans as the block before ???
#ifdef DEBUG
			int ans2 = 0;
			for (int j = l + p - 1; j <= r; j++) {
				for (int k : idx2[j]) {
					if (k == p) {
						ans2++;
						break;
					}
				}
			}
#endif

#ifdef DEBUG
			log(p, l + p - 1, r);
			cout << "idx3[p]=";
			for (int j : idx3[p])
				cout << j << " ";
			cout << endl;
			log(ans, ans2);
			assert(ans == ans2);
#endif
			cout << ans << endl;

		} else
			assert(false);
	}

}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

