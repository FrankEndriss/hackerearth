/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;
typedef vector<vll> vvll;
typedef vector<vvll> vvvll;

int NPOS = 17;
/* dp[i][pos][sum]==posibilities for i=first digit and pos number of positions and sum==sum of all digits
 */
vvvll dp(10, vvll(NPOS, vll(101)));

const ll INF = 1e10;

void init() {
	for (int i = 0; i <= 9; i++)
		dp[i][1][i] = 1;

	for (int pos = 2; pos < NPOS; pos++) {
		for (int sum = 0; sum <= 100; sum++) {
			for (int i = 0; i <= 9; i++) {
				if (sum - i >= 0) {
					for (int j = 0; j <= 9; j++) {
						dp[i][pos][sum] += dp[j][pos - 1][sum - i];
					}
				}
			}
		}
	}
	/*
	 #ifdef DEBUG
	 for (int first = 1; first <= 9; first++) {
	 cout << " dp[" << first << "][12][100]=" << dp[first][12][100] << endl;
	 }
	 #endif
	 */
}

/* solves the query for n=n and digitsum==sum and pos positions. */
void solve2(const ll n, const int sum, const int pos, vi &ans, const bool with0) {
	/*
	 cout << "n=" << n << " sum=" << sum << " pos=" << pos << " ";
	 for (auto a : ans)
	 cout << a;
	 cout << endl;
	 */
	if (sum == 0 && pos == 0)
		return;

	ll cnt = 0;
	for (int i = with0 ? 0 : 1; i <= 9; i++) {
		if (cnt + dp[i][pos][sum] >= n) {
			ans.push_back(i);
			solve2(n - cnt, sum - i, pos - 1, ans, true);
			return;
		} else {
			//		cout << "adding i=" << i << " pos-1=" << pos - 1 << " sum-i=" << sum - i << " val=" << dp[i][pos - 1][sum - i] << endl;
			cnt += dp[i][pos][sum];
		}
	}

	assert(false);
}

/* number of possibilities with exactly pos positions to get sum sum */
ll bypos(int sum, int pos) {
	ll ans = 0;
	for (int i = 1; i <= 9; i++)	// must not start with 0
		ans += dp[i][pos][sum];
	//cout << "bypos sum=" << sum << " pos=" << pos << " ans=" << ans << endl;
	return ans;
}

void solve() {
	cinll(n);
	vi ans;

	for (int pos = 12; pos < NPOS; pos++) {
		ll cnt = bypos(100, pos);
		if (cnt < n)
			n -= cnt;
		else {
			solve2(n, 100, pos, ans, false);
			break;
		}
	}

	for (int i : ans)
		cout << i;
	cout << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	init();
	cini(t);
	while (t--)
		solve();
}

