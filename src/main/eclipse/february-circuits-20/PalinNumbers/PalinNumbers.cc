/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

ll ipow(int i) {
	ll ans = 1;
	while (i > 0) {
		ans *= 10;
		i--;
	}
	return ans;
}

const int NMAX = 36;
vll dp(NMAX);	// dp[i]=number of palnums with exactly i digits

void init() {
	dp[1] = 9;
	dp[2] = 9;
	for (int i = 3; i < NMAX; i++) {
		dp[i] = ipow((i - 1) / 2) * 9;
#ifdef DEBUG
		cout << "i=" << i << " dp[i]=" << dp[i] << endl;
#endif
	}
}

/* makes a digits digit palnum from base by appending the number to itself in reverse order. */
ll app(ll base, int digits) {
	log(base, digits);
	queue<int> d;
	ll aux = base;
	ll ans = base;
	while (aux) {
		d.push(aux % 10);
		aux /= 10;
	}

	if (d.size() > digits)
		d.pop();

	assert(d.size() == digits);

	ll inc = 0;
	while (d.size()) {
		inc *= 10;
		inc += d.front();
		d.pop();
		ans *= 10;
	}
	return ans + inc;
}

/* return the kth pnum */
ll kthpnum(ll k) {
	if (k == 0)
		return 0LL;

	ll cnt = 0;
	for (int i = 1; i < NMAX; i++) {
		if (cnt + dp[i] >= k) {
			/* find (k-cnt)th pnum with i digits */
			int ndigits = (i + 1) / 2;
			ll base = ipow(ndigits - 1);
			base += (k - cnt - 1);
			/* append the i/2 first digits of base in reverse
			 * order to base. */
			return app(base, i / 2);
		} else
			cnt += dp[i];
	}
	assert(false);
}

/* find how much pnums are less than x by binary search. */
ll lessx(const int x) {
	ll l = 0;
	ll r = 1e10;

	while (l + 1 < r) {
		ll mid = (l + r) / 2;
		ll val = kthpnum(mid);
		if (val < x) {
			l = mid;
		} else
			r = mid;
	}
#ifdef DEBUG
	cout << "less(" << x << ")=" << l << endl;
#endif
	return l;
}

/* kth palindrome-number eq or greater than x */
void solve() {
	cinll(x);
	cinll(k);

	ll ans = kthpnum(lessx(x) + k);
	cout << ans << endl;
}

#ifdef DEBUG
void test() {
	for (int i = 1; i < 1200; i++) {
		cout << "i=" << i << " kthpnum(i)=" << kthpnum(i) << endl;
	}

}
#endif

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	init();
#ifdef DEBUG
	test();
#endif
	cini(t);
	while (t--)
		solve();
}

