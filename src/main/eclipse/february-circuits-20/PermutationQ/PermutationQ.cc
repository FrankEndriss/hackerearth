/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<vvvi> vvvvi;
typedef vector<vvvvi> vvvvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* todo
 * create sectors by halving (or /3) and
 * move all nodes of one sector together in one group
 * then optimize groups separatly, and randomly mix
 * groups.
 */
const int N = 2;
void solve2(vvi &a) {
	const int n = a.size();
	vvvvvi q(N, vvvvi(N, vvvi(N, vvi(N)))); // q[i][j][k][l]=list of indexes into a of elements belonging to that "quadrant"

	vvi idx(4, vi(n));
	for (int i = 0; i < n; i++)
		for (int j = 0; j < 4; j++)
			idx[j][i] = i;

	for (int i = 0; i < 4; i++) {
		sort(idx[i].begin(), idx[i].end(), [&](int i1, int i2) {
			a[i1][i] < a[i2][i];
		});
	}

	const int n2 = n / 2;
	for (int i = 0; i < n; i++) {
		int i0, i1, i2, i3;
		if (idx[0][i] <= n2)
			i0 = 0;
		else
			i0 = 1;

		if (idx[1][i] <= n2)
			i1 = 0;
		else
			i1 = 1;

		if (idx[2][i] <= n2)
			i2 = 0;
		else
			i2 = 1;

		if (idx[3][i] <= n2)
			i3 = 0;
		else
			i3 = 1;

		q[i0][i1][i2][i3].push_back(i);
	}

	function<void(vi&)> doSort = [](vi &idx) {
		// TODO take first, find nearest, remove that one, repeat...
	};

	for (int i1 = 0; i1 < 2; i1++) {
		for (int i2 = 0; i2 < 2; i2++) {
			for (int i3 = 0; i3 < 2; i3++) {
				for (int i4 = 0; i4 < 2; i4++) {
					doSort(q[i1][i2][i3][i4]);
				}
			}
		}
	}

	/* we want to find the median index for each dimension.
	 * Then put every element in that "quadtrant".
	 * Then sort every single quadrant.
	 * And sort the quadrants somehow usefull. */

	for (int i = 0; i < n; i++)
		cout << a[i][4] + 1 << endl;
}
void solve1(vvi &a) {
	const int n = a.size();

	sort(a.begin(), a.end(), [](vi &a1, vi &a2) {
		return a1[0] < a2[0];
	});
	srand(42);
	const int N = 15000000;
	for (int i = 0; i < N; i++) {
		int r0 = rand() % n;
		int r1;
		while ((r1 = rand() % n) == r0)
			;

		int d0 = 0;	// dist before swap
		int d1 = 0; // dist after swap
		for (int j = 0; j < 4; j++) {
			if (r0 - 1 >= 0) {
				d0 += abs(a[r0 - 1][j] - a[r0][j]);
				d1 += abs(a[r0 - 1][j] - a[r1][j]);
			}

			if (r0 + 1 < n) {
				d0 += abs(a[r0][j] - a[r0 + 1][j]);
				d1 += abs(a[r1][j] - a[r0 + 1][j]);
			}

			if (r1 - 1 >= 0) {
				d1 += abs(a[r1 - 1][j] - a[r1][j]);
				d0 += abs(a[r1 - 1][j] - a[r0][j]);
			}

			if (r1 + 1 < n) {
				d1 += abs(a[r1][j] - a[r1 + 1][j]);
				d0 += abs(a[r0][j] - a[r1 + 1][j]);
			}
		}
		if (d1 < d0)
			swap(a[r0], a[r1]);
	}
	for (int i = 0; i < n; i++)
		cout << a[i][4] + 1 << endl;
}

/* We need to find some kind of traveling salesman, where the distance
 * of two nodes is defined by the manhattan distance in 4D space.
 *
 *Observations
 * A complete ramdomized solution could be good.
 */
void solve() {
	cini(q);
	vvi a(q, vi(5));
	for (int i = 0; i < q; i++) {
		for (int j = 0; j < 4; j++)
			cin >> a[i][j];
		a[i][4] = i;
	}

	solve1(a);
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

