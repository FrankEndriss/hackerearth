/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* LCA based on euler path with segment tree. */
struct LCA {
	vector<int> height, euler, first, segtree, parent;
	vector<bool> visited;
	int n;

	LCA(vector<vector<int>> &adj, int root = 0) {
		n = (int) adj.size();
		//parent.resize(n);
		height.resize(n);
		first.resize(n);
		euler.reserve(n * 2);
		visited.assign(n, false);
		dfs(-1, adj, root);
		int m = (int) euler.size();
		segtree.resize(m * 4);
		build(1, 0, m - 1);
	}

	void dfs(int par, vector<vector<int>> &adj, int node, int h = 0) {
		//parent[node] = par;
		visited[node] = true;
		height[node] = h;
		first[node] = (int) euler.size();
		euler.push_back(node);
		for (auto to : adj[node]) {
			if (!visited[to]) {
				dfs(node, adj, to, h + 1);
				euler.push_back(node);
			}
		}
	}

	void build(int node, int b, int e) {
		if (b == e) {
			segtree[node] = euler[b];
		} else {
			int mid = (b + e) / 2;
			build(node << 1, b, mid);
			build(node << 1 | 1, mid + 1, e);
			int l = segtree[node << 1], r = segtree[node << 1 | 1];
			segtree[node] = (height[l] < height[r]) ? l : r;
		}
	}

	int query(int node, int b, int e, int L, int R) {
		if (b > R || e < L)
			return -1;
		if (b >= L && e <= R)
			return segtree[node];
		int mid = (b + e) >> 1;

		int left = query(node << 1, b, mid, L, R);
		int right = query(node << 1 | 1, mid + 1, e, L, R);
		if (left == -1)
			return right;
		if (right == -1)
			return left;
		return height[left] < height[right] ? left : right;
	}

	/* @return the lca of u and v */
	int lca(int u, int v) {
		int left = first[u], right = first[v];
		if (left > right)
			swap(left, right);
		return query(1, 0, (int) euler.size() - 1, left, right);
	}
};

void solve() {
	cini(n);
	cini(q);

	vvi adj(n);
	for (int i = 0; i < n - 1; i++) {
		cini(u);
		cini(v);
		u--;
		v--;
		adj[u].push_back(v);
		adj[v].push_back(u);
	}

	LCA lca(adj);

	for (int i = 0; i < q; i++) {
		cini(k);
		vi v(k);
		for (int j = 0; j < k; j++) {
			cin >> v[j];
			v[j]--;
		}

		sort(v.begin(), v.end(), [&](int v1, int v2) {
			return lca.height[v2] < lca.height[v1];
		});

		/* brute force all pairs of cities, ans==max(dist+1)/2
		 * one of the nodes on deepest level must be one of the  two farthest away from each other.
		 **/
		int ans = 0;
		for (int j = 0; j < k && lca.height[v[j]] == lca.height[v[0]]; j++) {
			for (int jj = j + 1; jj < k; jj++) {
				int lroot = lca.lca(v[j], v[jj]);
				ans = max(ans, (lca.height[v[j]] - lca.height[lroot] + lca.height[v[jj]] - lca.height[lroot] + 1) / 2);
			}
		}
		cout << ans << endl;

		/* sort by deepest level */
		/*
		 int ans = 0;
		 for (int j = 0; j < k && deepest == lca.height[j]; j++) {
		 for (int jj = j + 1; jj < k; jj++) {
		 int lroot = lca.lca(v[j], v[jj]);
		 ans = max(ans, (lca.height[v[j]] - lca.height[lroot] + lca.height[v[jj]] - lca.height[lroot] + 1) / 2);
		 }
		 }
		 cout << ans << endl;
		 */

		/* first find root of the given subsets of nodes */
		/*
		 int lroot = -1;
		 for (int j = 0; j < k; j++) {
		 if (lroot == -1)
		 lroot = v[j];
		 else
		 lroot = lca.lca(lroot, v[j]);
		 }
		 */

		/* We would need to know for every node the child of the root node through wich
		 * that node can be reached.
		 * Then we need to find the two child nodes with the deepest node in it.
		 * Ans is sum of heights of those nodes div by 2.
		 *
		 * So, what we do is sort the nodes by height, biggest first. Then, for every
		 * remaining node, step throug the parent nodes until lroot. While this, remove
		 * all seen nodes from the list.
		 * Every time we reach lroot a new group (and child node) starts.
		 * We need to find only two deepest childs/nodes.
		 * What if there is only one? -> second size==0.
		 * Still: ans=(h1+h2)/2
		 * ...blah.. does not work at all :/
		 */

		/*
		 set<pii> nodes; // set of nodes sorted by height
		 for (int j = 0; j < k; j++)
		 nodes.insert( { -lca.height[v[j]], v[j] });

		 auto it = nodes.begin();
		 int h1 = -it->first;
		 int n1 = it->second;
		 nodes.erase(it);

		 while (n1 != lroot) {
		 n1 = lca.parent[n1];
		 nodes.erase( { -lca.height[n1], n1 });
		 }

		 int hroot = lca.height[lroot];
		 int h2 = hroot;
		 if (nodes.size()) {
		 h2 = -nodes.begin()->first;
		 }

		 int ans = ((h1 - hroot) + (h2 - hroot)) / 2;
		 cout << ans << endl;
		 */
	}

}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

