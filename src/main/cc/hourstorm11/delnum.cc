/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
int n, k;
    cin>>n>>k;
    vector<int> a(n);
    fori(n)
        cin>>a[i];

    int ans;
    if(n%2==1) {
        int mid=n/2+1;
        int m=-1;
        for(int i=mid-(k+1)/2; i<mid+(k+1)/2; i++) 
            m=max(m, a[i]);
        ans=m;
    } else { // n is even
        int mid=n/2-1;
        int m=-1;
        for(int i=mid-(k+1)/2; i<mid+(k+1)/2; i++) 
            m=max(m, a[i]);
        ans=m;
    }

    cout<<ans<<endl;

}

