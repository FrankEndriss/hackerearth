/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

typedef struct path  {
    int row, col;
    ll cost;
    int mrow;
    int mcol;
} Path;

int main() {
int n, t;
    cin>>n>>t;
    vector<vector<int>> a(n, vector<int>(n));
    fori(n) {
        for(int j=0; j<n; j++) 
            cin>>a[i][j];
    }

    //vector<vector<path>> m(n, vector<path>(n));
    //m[0][0]=0;
    //vector<pair<int, int>> next;
    //next.push_back({0, 0});
    
    int maxScore=1;

    function<void(Path)> dfs=[&](Path path) {
        if(path.cost>=t)
            return;
//cout<<"max, mrow="<<path.mrow<<" mcol="<<path.mcol<<endl;;
        maxScore=max(maxScore, (path.mrow+1) * (path.mcol+1));

        for(int drow=-1; drow<=1; drow++) {
            for(int dcol=-1; dcol<=1; dcol++) {
                if(dcol==0 && drow==0)
                    continue;
                if(path.row+drow<n && path.row+drow>=0 
                        && path.col+dcol<n && path.col+dcol>=0) {
                    int nrow=path.row+drow;
                    int ncol=path.col+dcol;
                    dfs({ nrow, ncol, path.cost+a[nrow][ncol], 
                            max(nrow, path.mrow), max(ncol, path.mcol) });
                }
            }
        }
    };

    dfs({ 0, 0, 0, 0, 0 });

    cout<<maxScore<<endl;
}

