
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

int dig3(int i) {
    int d=0;
    while(i) {
        d+=i%10;
        i/=10;
    }
    return d*d*d;
}

/* The value should not increse forever, at some 
 * point it loops.
 * Just find the loopsize.
 */
const int N=3e6;
void solve() {
    cini(n);
    cini(k);

    //cerr<<"in n="<<n<<" k="<<k<<endl;
    vi a;
    a.push_back(n);
    set<pii> memo;
    memo.insert({n,0});
    for(int i=1; i<=k; i++) {
        n=dig3(n);
        //cerr<<"i="<<i<<" n="<<n<<endl;
        a.push_back(n);

        auto it=memo.lower_bound({n,-1});
        if(it!=memo.end() && it->first==n) {
            int loop=i-it->second;
            int ans=a[it->second+(k-i)%loop];
            cout<<ans<<endl;
            return;
        }
        memo.insert({n,i});
    }
    cout<<n<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
