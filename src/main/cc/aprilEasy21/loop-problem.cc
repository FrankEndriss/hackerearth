
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Find number of elements in range 0,a inclusive
 * with ith bit set.
 */
int cn(int a, int i) {
    int bsz=1LL<<(i+1);
    int cnt=(a+1)/bsz;
    int ans=cnt*bsz/2;
    ans+=max(0LL, (a+1)%bsz-bsz/2);
    return ans;
}

int cn(int a, int b, int i) {
    return cn(b,i)-cn(a-1,i);
}

/* Each a..b is added sum(c..d) times.
 * Each bit in c..d is xored sum(a..b) times.
 *
 * Solve bit by bit. Let cntab[i]=number of times bit 
 * is set in ab, cntcd[j] same for cd.
 * Then the bit i contributes cntab[i]*(n-cntcd[i])+(n-cntab[i])*cntcd[i] times.
 */
const int MOD=1e9+7;
void solve() {
    cini(a);
    cini(b);
    cini(c);
    cini(d);

    //cerr<<"a="<<a<<" b="<<b<<" c="<<c<<" d="<<d<<endl;
    vi cntab(31); /* cntab[i]=number of numbers in (a..b) having bit i set */
    vi cntcd(31); /* cntcd[i]=number of numbers in (c..d) having bit i set */

    const int nab=b-a+1;
    const int ncd=d-c+1;
    int ans=0;
    for(int i=0; i<31; i++) {

        cntab[i]=cn(a,b,i);
        cntcd[i]=cn(c,d,i);
        //cerr<<"i="<<i<<" cntab[i]="<<cntab[i]<<" cntcd[i]="<<cntcd[i]<<endl;

        int t=(cntab[i]*(ncd-cntcd[i])%MOD) + ((cntcd[i]*(nab-cntab[i]))%MOD);
        ans=ans+ ((1LL<<i)*t)%MOD;
        ans%=MOD;
    }
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
