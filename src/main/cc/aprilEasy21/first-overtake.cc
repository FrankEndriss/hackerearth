
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* kind of convex hull thing 
 * consider cars sorted by position,
 * then each pair has pos diff and velo diff,
 * sort by division of both.
 *
 * We need to efficiently find which of the right
 * functions crosses the current function ant the 
 * leftmost point.
 * ***
 * get some points for brute force
 * ...
 * x[i]<x[i+1]: That means, the rightmost car is the fastest,
 * so it will overtake the slowest first. very funny.
 * ...
 * No, not the last is the fastest, but the first.
 * ...
 * Ok, simply ignore m, we do not need it anyway.
 * Then ans=min dist between two consecutive cars.
 * */
const int INF=1e18;
void solve() {
    cini(n);
    //cini(m);

    cinai(x,n); /* positions */

    int ans=INF;
    for(int i=0; i+1<n; i++)
        ans=min(ans, x[i+1]-x[i]);

    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
