/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

void solve() {
    int s0, s1, a01, b10;
    cin>>s0>>s1>>a01>>b10;

// SUBSEQUENCES !!!
    if(a01>s0 || a01>s1 || b10>s0 || b10>s1)
        cout<<"No";
    else {
        cout<<"Yes";
    }
    cout<<endl;
}

int main() {
int t;
    cin>>t; 
    while(t--)
        solve();

}

