/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
int n;
    cin>>n;
    vector<int> g;
    vector<vector<int>> f(10001);

    fori(n) {
        int a;
        cin>>a;
        f[a].push_back(i+1);
    }

    const int MOD=1000000007;

/* Count all pairs f[i][*],f[i+1][*] multiplied by the number of substrings including these pairs.
 * The number of substrings is
 * f[i][*] * n-f[i+1][*]
 */
    ll ans=0;
    for(int i=0; i<n-1; i++) {
        for(int idx1 : f[i]) {
            for(int idx2 : f[i+1]) {
cout<<"idx1="<<idx1<<" idx2="<<idx2<<" n="<<n<<endl;
                int m1=min(idx1, idx2);
                int m2=n-max(idx1, idx2)+1;
                ll c=1LL*m1*m2;
                if(m1>1 && m2>1)
                    c--;
                c%=MOD;
                ans+=c;
                ans%=MOD;
cout<<ans<<endl;
            }
        }
    }
    for(int i=0; i<n; i++) {
        int idx1=-1;
        for(int idx2 : f[i]) {
            if(idx1>=0) {
cout<<"idx1="<<idx1<<" idx2="<<idx2<<endl;
                int m1=min(idx1, idx2);
                int m2=n-max(idx1, idx2)+1;
                ll c=1LL*m1*m2;
                if(m1>1 && m2>1)
                    c--;
                c%=MOD;
                ans+=c;
                ans%=MOD;
cout<<ans<<endl;
            }
            idx1=idx2;
        }
    }

    cout<<ans<<endl;

}

