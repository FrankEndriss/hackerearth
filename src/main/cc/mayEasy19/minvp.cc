/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

class path {
public:
    ll w;           // sum w in path
    int special;   // special used?
    int v;          // vertex at end of path
    ll lastW;       // last w in path
};

class mycmp {
public:
  bool operator() (const path& lhs, const path &rhs) const
  {
    return rhs.w<lhs.w;
  }
};

int main() {
int n, m, k;
    cin>>n>>m;
    vector<vector<pair<int, ll>>> tree(n+1);
    fori(m) {
        int u, v, w;
        cin>>u>>v>>w;
        tree[u].push_back(make_pair(v, w));
    }

    vector<bool> s(n+1, false);
    cin>>k;
    fori(k) {
        int v;
        cin>>v;
        s[v]=true;
    }
    
    int x, y;
    cin>>x>>y;

    priority_queue<path, vector<path>, mycmp> paths;
    path p={ 0, s[x], x, -1 };
    paths.push(p);

    vector<vector<ll>> minw(2, vector<ll>(n+1, -1));
    minw[p.special][p.v]=0;

    int ans=-1;
    while(paths.size()>0) {
        path next=paths.top();
        paths.pop();
//cout<<"next.v="<<next.v<<" next.w="<<next.w<<" paths.size()="<<paths.size()<<endl;
        if(next.v==y && next.special) {
            ans=next.w;
            break;
        }

        for(pair<int, ll> &c : tree[next.v]) {
            if(next.special && s[c.first])
                continue;

            if(next.lastW!=-1) {
                if(min(next.lastW, c.second)*2<max(next.lastW, c.second)) {
//cout<<"cont w/ next.lastW="<<next.lastW<<" c.second="<<c.second<<endl;
                    continue;
                }
            }

            p={ next.w+c.second, next.special || s[c.first], c.first, c.second };
            if(minw[p.special][p.v]<0 || minw[p.special][p.v]>p.w) {
                minw[p.special][p.v]=p.w;
                paths.push(p);
            }
//cout<<"push v="<<p.v<<endl;
        }
    }
    cout<<ans<<endl;

}

