
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

void solve() {
    int n;
    cin>>n;

    int ans;
    if(n<2)
        ans=0;
    else
        ans=(n-2)/4+1;
    cout<<ans<<endl;
}
int main() {
    int t;
    cin>>t;
    while(t--)
        solve();

}

