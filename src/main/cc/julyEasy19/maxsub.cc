/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;

ll gauss(ll m) {
    return (m*m+m)/2;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    ll n, m, k;
    cin>>n>>m>>k;

    ll g=gauss(m)-gauss(m-(k+1));    // k moves
    ll ans=(n/g)*(k+1);
    ll r=n%g;
    while(r>=0) {
        r-=m;
        ans++;
        m--;
    }
    cout<<ans<<endl;
}

