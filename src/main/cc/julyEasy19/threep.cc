/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;

const int primeAproxFactor=30;
const int MAXN=1000000;
vector<int> pr(MAXN);

void init() {
    vector<bool> cmp(MAXN*primeAproxFactor, false);

    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!cmp[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                cmp[j] = true;
                j += i;
            }
        }
        i++;
    }
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    init();
    int n;
    cin>>n;
    int maxp=-1;
    while(pr[maxp+1]<=n)
        maxp++;

    ll ans=0;
    function<void(int, int, int)> addSol=[&](int i, int j, int k) {
                ll s=pr[i]+pr[j]+pr[k];
                ll p=1LL*pr[i]*pr[j]*pr[k];
                if(p%s==0) {
                    if(i==j && j==k)
                        ans++;
                    else if(i==j || i==k || j==k)
                        ans+=3;
                    else
                        ans+=6;
//                    cout<<pr[i]<<" "<<pr[j]<<" "<<pr[k]<<endl;
                }
    };

    for(int i=0; pr[i]<=n; i++) {
        for(int j=i; pr[i]*pr[j]<=3*n && pr[j]<=n; j++) {
            /* find c for a+b+c==a*b */
            int l=0; 
            int r=maxp+1;
            int s=pr[i]+pr[j];
            int p=pr[i]*pr[j];
            while(l<r-1) {
                ll mid=(l+r)/2;
//cout<<"pr[i]="<<pr[i]<<" pr[j]="<<pr[j]<<" pr[mid]="<<pr[mid]<<endl;
                if(s+pr[mid] > p)
                    r=mid;
                else 
                    l=mid;
            }
            if(s+pr[l]==p) {
                addSol(i, j, l);
                continue;
            }

            /* find c for a+b+c==a*c */
            
           

/*
            ll c=pr[i]*pr[j]-(pr[i]+pr[j]);
            auto it=lower_bound(pr.begin(), pr.end(), pr[i]*pr[j]-(pr[i]+pr[j]));
            if(*it==pr[i]+pr[j]) {
                ll s=(pr[i]+pr[j])*2;
                ll p=(pr[i]*pr[j])*(pr[i]+pr[j]);
                if(p%s==0) {
                    if(i==j)
                        ans+=3;
                    else
                        ans+=6;
                }
            }
            for(int k=j; pr[k]<=n; k++) {
                ll s=pr[i]+pr[j]+pr[k];
                ll p=1LL*pr[i]*pr[j]*pr[k];
                if(p%s==0) {
                    if(i==j && j==k)
                        ans++;
                    else if(i==j || i==k || j==k)
                        ans+=3;
                    else
                        ans+=6;
                    cout<<pr[i]<<" "<<pr[j]<<" "<<pr[k]<<endl;
                }
            }
*/
        }
    }
    cout<<ans<<endl;
}

