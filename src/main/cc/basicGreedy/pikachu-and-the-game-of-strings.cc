/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

int dist(char s, char t) {
    int d=t-s;
    if(d<0)
        d+=26;

    return d/13 + d%13;
}

int main() {
int n;
    cin>>n;
string s, t;
    cin>>s>>t;

    int ans=0;
    for(uint i=0; i<s.length(); i++) {
        ans+=dist(s[i], t[i]);
    }
    cout<<ans<<endl;

}

