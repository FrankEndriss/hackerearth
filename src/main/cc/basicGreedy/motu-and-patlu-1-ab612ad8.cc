/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

void solve() {
    int n;
    cin>>n;
    vector<int> a(n);
    for(int i=0; i<n; i++) 
        cin>>a[i];

    int l=0;
    int r=n-1;
    while(l<r-1) {
        if(a[l]>a[r]*2) {
            a[l]-=a[r]*2;
            a[r]=0;
        } else {
            if(a[l]==1) {
                a[l]--;
                l++;
                if(l<r) 
                    a[l]--;
            } else {
                a[r]-=a[l]/2;
                a[l]-=2*(a[l]/2);
            }
        }
        if(a[l]==0 && l<r-1)
            l++;
        if(a[r]==0 && r>l+1)
            r--;

// TODO still something wrong...
    }

    cout<<l<<" "<<(n-r)<<endl;
    
    cout<<(l>(n-r)?"Motu":(l==(n-r)?"Tie":"Patlu"))<<endl;
    
}

int main() {
    int t;
    cin>>t;
    for(int i=0; i<t; i++)
        solve();

}

