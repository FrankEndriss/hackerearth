
#include <math.h> 
#include <iostream>
using namespace std;

int main() {
int n, t, i;
	cin>>t;
// Ans is YES for all n which are not one less than an odd prime.
// So we need to find if n+1 is prime (and odd, ie not 2)
	for(i=0; i<t; i++) {
		cin>>n;
		if(n==1) {
			cout << "YES" << endl;
			continue;
		}
		n++;
		int sq=sqrt(n)+1;
		int j;
		int ans=1;
		for(j=2; j<sq; j++) {
			if(n%j==0) {
				ans=0;
				break;
			}
		}
		if(ans)
			cout << "NO" << endl;
		else
			cout << "YES" << endl;
	}
}
