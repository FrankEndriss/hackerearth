
#include <math.h> 
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

long long toLong(int *num) {
long long n=0;
int i=0;
long long tens=1;
	while(num[i++]>0) {
		n+=num[i-1]*tens;
		tens*=10;
	}
	return n;
}

int followed[][9]={ 
	{ 2, 3, 4, 5, 6, 7, 8, 9, 0 },	// 0
	{ 2, 0, 0, 0, 0, 0, 0, 0, 0 },	// 1	must not be used
	{ 2, 4, 6, 8, 0, 0, 0, 0, 0 },	// 2
	{ 3, 6, 9, 0, 0, 0, 0, 0, 0 }, 	// 3
	{ 2, 4, 6, 8, 0, 0, 0, 0, 0 },	// 4
	{ 5, 0, 0, 0, 0, 0, 0, 0, 0 },	// 5
	{ 2, 3, 4, 6, 8, 0, 0, 0, 0 },	// 6
	{ 7, 0, 0, 0, 0, 0, 0, 0, 0 },	// 7
	{ 2, 4, 6, 8, 0, 0, 0, 0, 0 },	// 8
	{ 3, 6, 9, 0, 0, 0, 0, 0, 0 }	// 9
};

#define META 99
// gnum[META] is the highest ever flipped index of that num


/** flips digit num[idx] to next possible value.
 * Set all previous (0..idx-1) to first possible value. */
void flip(int *num, int idx) {
	num[META]=max(num[META], idx);
	int *possibleDigit=followed[num[idx+1]];
	int i=0;
	while(possibleDigit[i]<=num[idx] && possibleDigit[i]>0)
		i++;
	if(possibleDigit[i]==0) {
//cout<<"log flip to next num, idx="<<idx<<endl;
		flip(num, idx+1);
	} else {
		num[idx]=possibleDigit[i];
//cout<<"log flip current num to:"<<num[idx]<<endl;
		for(i=idx-1; i>=0; i--) {
			num[i]=followed[num[i+1]][0];
//cout<<"log set num["<<i+1<<"] to fist possible value: "<<num[i]<<endl;
		}
	}
}

#define WO2 0
#define WI2 1
#define D3_0 0
#define D3_1 1
#define D3_2 2
#define D3_X 3

#define MOD 1000000007

int dp[20][10][4][2];

void dumpDP() {
	for(int i=0; i<20; i++)  {
		cout<<"d"<<i<<endl;
		for(int j=0; j<10; j++) {
			for(int k=0; k<4; k++)
				for(int h=0; h<2; h++)
					cout<<dp[i][j][k][h] <<" ";
			cout<<endl;
		}
	}
}

void dumpNum(int *num) {
	cout<<"log num=";
	for(int i=num[META]; i>=0; i--) {
		cout<<num[i];
	}
	cout<<endl;
}

void initDP() {
	dp[0][2][D3_1][WI2]=1;
	dp[0][2][D3_1][WO2]=1;
	dp[0][2][D3_X][WI2]=1;
	dp[0][2][D3_X][WO2]=1;

	dp[0][3][D3_0][WO2]=1;
	dp[0][3][D3_X][WO2]=1;

	dp[0][4][D3_2][WI2]=1;
	dp[0][4][D3_2][WO2]=1;
	dp[0][4][D3_X][WI2]=1;
	dp[0][4][D3_X][WO2]=1;

	dp[0][5][D3_1][WO2]=1;
	dp[0][5][D3_X][WO2]=1;

	dp[0][6][D3_0][WI2]=1;
	dp[0][6][D3_0][WO2]=1;
	dp[0][6][D3_X][WI2]=1;
	dp[0][6][D3_X][WO2]=1;

	dp[0][7][D3_2][WO2]=1;
	dp[0][7][D3_X][WO2]=1;

	dp[0][8][D3_1][WI2]=1;
	dp[0][8][D3_1][WO2]=1;
	dp[0][8][D3_X][WI2]=1;
	dp[0][8][D3_X][WO2]=1;

	dp[0][9][D3_0][WO2]=1;
	dp[0][9][D3_X][WO2]=1;

	for(int i=0; i<19; i++) {
//cout<<"log init loop, i="<<i<<endl;
// TODO need to use MOD while adding sums
		for(int k=2; k<10; k++)
			for(int i3=0; i3<4; i3++)
				for(int i2=0; i2<2; i2++)
					dp[i+1][0][i3][i2]+=dp[i][k][i3][i2];

		dp[i+1][2][D3_0][WI2] = dp[i][2][D3_2][WI2] + dp[i][4][D3_2][WI2] + dp[i][6][D3_2][WI2] + dp[i][8][D3_2][WI2];
		dp[i+1][2][D3_0][WO2] = dp[i+1][2][D3_0][WI2];
		dp[i+1][2][D3_1][WI2] = dp[i][2][D3_0][WI2] + dp[i][4][D3_0][WI2] + dp[i][6][D3_0][WI2] + dp[i][8][D3_0][WI2];
		dp[i+1][2][D3_1][WO2] = dp[i+1][2][D3_1][WI2];
		dp[i+1][2][D3_2][WI2] = dp[i][2][D3_1][WI2] + dp[i][4][D3_1][WI2] + dp[i][6][D3_1][WI2] + dp[i][8][D3_1][WI2];
		dp[i+1][2][D3_2][WO2] = dp[i+1][2][D3_2][WI2];
		dp[i+1][2][D3_X][WI2] = dp[i][2][D3_X][WI2] + dp[i][4][D3_X][WI2] + dp[i][6][D3_X][WI2] + dp[i][8][D3_X][WI2];
		dp[i+1][2][D3_X][WO2] = dp[i+1][2][D3_X][WI2];

		dp[i+1][3][D3_0][WI2] = dp[i][3][D3_0][WI2] + dp[i][6][D3_0][WI2] + dp[i][9][D3_0][WI2];
		dp[i+1][3][D3_0][WO2] = dp[i][3][D3_0][WO2] + dp[i][6][D3_0][WO2] + dp[i][9][D3_0][WO2];
		dp[i+1][3][D3_1][WI2] = dp[i][3][D3_1][WI2] + dp[i][6][D3_1][WI2] + dp[i][9][D3_1][WI2];
		dp[i+1][3][D3_1][WO2] = dp[i][3][D3_1][WO2] + dp[i][6][D3_1][WO2] + dp[i][9][D3_1][WO2];
		dp[i+1][3][D3_2][WI2] = dp[i][3][D3_2][WI2] + dp[i][6][D3_2][WI2] + dp[i][9][D3_2][WI2];
		dp[i+1][3][D3_2][WO2] = dp[i][3][D3_2][WO2] + dp[i][6][D3_2][WO2] + dp[i][9][D3_2][WO2];
		dp[i+1][3][D3_X][WI2] = dp[i+1][3][D3_0][WI2];
		dp[i+1][3][D3_X][WO2] = dp[i+1][3][D3_0][WO2];

		dp[i+1][4][D3_0][WI2] = dp[i][2][D3_2][WI2] + dp[i][4][D3_2][WI2] + dp[i][6][D3_2][WI2] + dp[i][8][D3_2][WI2];
		dp[i+1][4][D3_0][WO2] = dp[i][2][D3_2][WO2] + dp[i][4][D3_2][WO2] + dp[i][6][D3_2][WO2] + dp[i][8][D3_2][WO2];
		dp[i+1][4][D3_1][WI2] = dp[i][2][D3_0][WI2] + dp[i][4][D3_0][WI2] + dp[i][6][D3_0][WI2] + dp[i][8][D3_0][WI2];
		dp[i+1][4][D3_1][WO2] = dp[i][2][D3_0][WO2] + dp[i][4][D3_0][WO2] + dp[i][6][D3_0][WO2] + dp[i][8][D3_0][WO2];
		dp[i+1][4][D3_2][WI2] = dp[i][2][D3_1][WI2] + dp[i][4][D3_1][WI2] + dp[i][6][D3_1][WI2] + dp[i][8][D3_1][WI2];
		dp[i+1][4][D3_2][WO2] = dp[i][2][D3_1][WO2] + dp[i][4][D3_1][WO2] + dp[i][6][D3_1][WO2] + dp[i][8][D3_1][WO2];
		dp[i+1][4][D3_X][WI2] = dp[i][2][D3_X][WI2] + dp[i][4][D3_X][WI2] + dp[i][6][D3_X][WI2] + dp[i][8][D3_X][WI2];
		dp[i+1][4][D3_X][WO2] = dp[i][2][D3_X][WO2] + dp[i][4][D3_X][WO2] + dp[i][6][D3_X][WO2] + dp[i][8][D3_X][WO2];

		dp[i+1][5][D3_0][WI2] = dp[i][5][D3_2][WI2];
		dp[i+1][5][D3_0][WO2] = dp[i][5][D3_2][WO2];
		dp[i+1][5][D3_1][WI2] = dp[i][5][D3_0][WI2];
		dp[i+1][5][D3_1][WO2] = dp[i][5][D3_0][WO2];
		dp[i+1][5][D3_2][WI2] = dp[i][5][D3_1][WI2];
		dp[i+1][5][D3_2][WO2] = dp[i][5][D3_1][WO2];
		dp[i+1][5][D3_X][WI2] = dp[i][5][D3_X][WI2];
		dp[i+1][5][D3_X][WO2] = dp[i][5][D3_X][WO2];

		dp[i+1][6][D3_0][WI2] = dp[i][3][D3_0][WI2] + dp[i][6][D3_0][WI2] + dp[i][9][D3_0][WI2] + dp[i][2][D3_0][WI2] + dp[i][4][D3_0][WI2];
		dp[i+1][6][D3_0][WO2] = dp[i][3][D3_0][WO2] + dp[i][6][D3_0][WO2] + dp[i][9][D3_0][WO2] + dp[i][2][D3_0][WO2] + dp[i][4][D3_0][WO2];
		dp[i+1][6][D3_1][WI2] = dp[i][3][D3_1][WI2] + dp[i][6][D3_1][WI2] + dp[i][9][D3_1][WI2] + dp[i][2][D3_1][WI2] + dp[i][4][D3_1][WI2];
		dp[i+1][6][D3_1][WO2] = dp[i][3][D3_1][WO2] + dp[i][6][D3_1][WO2] + dp[i][9][D3_1][WO2] + dp[i][2][D3_1][WO2] + dp[i][4][D3_1][WO2];
		dp[i+1][6][D3_2][WI2] = dp[i][3][D3_2][WI2] + dp[i][6][D3_2][WI2] + dp[i][9][D3_2][WI2] + dp[i][2][D3_2][WI2] + dp[i][4][D3_2][WI2];
		dp[i+1][6][D3_2][WO2] = dp[i][3][D3_2][WO2] + dp[i][6][D3_2][WO2] + dp[i][9][D3_2][WO2] + dp[i][2][D3_2][WO2] + dp[i][4][D3_2][WO2];
		dp[i+1][6][D3_X][WI2] = dp[i][6][D3_X][WI2];
		dp[i+1][6][D3_X][WO2] = dp[i][6][D3_X][WO2];

		dp[i+1][7][D3_0][WI2] = dp[i][7][D3_2][WI2];
		dp[i+1][7][D3_0][WO2] = dp[i][7][D3_2][WO2];
		dp[i+1][7][D3_1][WI2] = dp[i][7][D3_0][WI2];
		dp[i+1][7][D3_1][WO2] = dp[i][7][D3_0][WO2];
		dp[i+1][7][D3_2][WI2] = dp[i][7][D3_1][WI2];
		dp[i+1][7][D3_2][WO2] = dp[i][7][D3_1][WO2];
		dp[i+1][7][D3_X][WI2] = dp[i][7][D3_X][WI2];
		dp[i+1][7][D3_X][WO2] = dp[i][7][D3_X][WO2];

		dp[i+1][8][D3_0][WI2] = dp[i][4][D3_2][WI2];
		dp[i+1][8][D3_0][WO2] = dp[i][4][D3_2][WO2];
		dp[i+1][8][D3_1][WI2] = dp[i][4][D3_0][WI2];
		dp[i+1][8][D3_1][WO2] = dp[i][4][D3_0][WO2];
		dp[i+1][8][D3_2][WI2] = dp[i][4][D3_1][WI2];
		dp[i+1][8][D3_2][WO2] = dp[i][4][D3_1][WO2];
		dp[i+1][8][D3_X][WI2] = dp[i][4][D3_X][WI2];
		dp[i+1][8][D3_X][WO2] = dp[i][4][D3_X][WO2];

		dp[i+1][9][D3_0][WI2] = dp[i][3][D3_0][WI2] + dp[i][6][D3_0][WI2] + dp[i][9][D3_0][WI2];
		dp[i+1][9][D3_0][WO2] = dp[i][3][D3_0][WO2] + dp[i][6][D3_0][WO2] + dp[i][9][D3_0][WO2];
		dp[i+1][9][D3_1][WI2] = dp[i][3][D3_1][WI2] + dp[i][6][D3_1][WI2] + dp[i][9][D3_1][WI2];
		dp[i+1][9][D3_1][WO2] = dp[i][3][D3_1][WO2] + dp[i][6][D3_1][WO2] + dp[i][9][D3_1][WO2];
		dp[i+1][9][D3_2][WI2] = dp[i][3][D3_2][WI2] + dp[i][6][D3_2][WI2] + dp[i][9][D3_2][WI2];
		dp[i+1][9][D3_2][WO2] = dp[i][3][D3_2][WO2] + dp[i][6][D3_2][WO2] + dp[i][9][D3_2][WO2];
		dp[i+1][9][D3_X][WI2] = dp[i][3][D3_X][WI2] + dp[i][6][D3_X][WI2] + dp[i][9][D3_X][WI2];
		dp[i+1][9][D3_X][WO2] = dp[i][3][D3_X][WO2] + dp[i][6][D3_X][WO2] + dp[i][9][D3_X][WO2];

	}
//dumpDP();
}

void toNum(int *num, long long value) {
	int i=0;
	while(value>0) {
		num[i++]=value%10;
		value/=10;
	}
	num[META]=i-1;
}

int toIdx(int number, int with3) {
	if(!with3)
		return D3_X;

	int mod=number%3;
	if(mod==0)
		return D3_0;
	else if(mod==1)
		return D3_1;
	else 
		return D3_2;
}

long long c(int *num, int pDsum, int pWith3, int pWith2) {
cout<<"log c(), num=";
dumpNum(num);
long long ans=0;
int dsum=pDsum;
int with3=pWith3;
int with2=pWith2;
	for(int i=num[META]; i>=0; i--) {
		for(int j=2; j<=num[i]; j++) {
			ans+=dp[i][j][toIdx(dsum, with3)][with2];
cout<<"log ans="<<ans<<" i="<<i<<" j="<<j<<" didx="<<toIdx(dsum, with3)<<endl;
			ans%=MOD;
		}
		dsum+=num[i];
		if(num[i]==2)
			with2=1;
		else if(num[i]==3)
			with3=1;
	}

	if(num[META]>0) {
// example: 363
// on first recursion, here ans=3xx
// we need to subtract
// 37x, 38x, 39x: dsum=3;  7x, 8x, 9x
// 364, 365, 366, 367, 368, 369: dsum=9;  4, 5, 6, 7, 8, 9

		int lnum[100];
		for(int i=0; i<100; i++)
			lnum[i]=num[i];

		dsum=pDsum+lnum[lnum[META]];
		with2=pWith2 || lnum[lnum[META]]==2;
		with3=pWith3 || lnum[lnum[META]]==3;
		lnum[lnum[META]]=0;
		lnum[META]--;
		for(int i=num[lnum[META]]+1; i<=9; i++) {
			lnum[lnum[META]]=i;
			ans-=c(lnum, dsum, with2, with3);
		}

// we need to add
// 09x
		for(int i=0; i<100; i++)
			lnum[i]=num[i];

		dsum=0;
		with2=0;
		with3=0;
		lnum[lnum[META]]=0;
		lnum[META]--;
		lnum[lnum[META]]=9;
		ans+=c(lnum, dsum, with2, with3);
	}

cout<<"log c(), ret="<<ans<<endl;
	return ans;
}

int gnumL[100];
int gnumR[100];

int main() {
long long L, R;
	initDP();
//cout<<"log after init"<<endl;
	cin>>L>>R;
	toNum(gnumL, L-1);
	flip(gnumL, 0);
	//dumpNum(gnumL);
	toNum(gnumR, R);
	flip(gnumR, 0);
	//dumpNum(gnumR);

	long long countL=c(gnumL, 0, 0, 0);
	long long countR=c(gnumR, 0, 0, 0);
	long long ans=countR-countL;
cout<<"log countL="<<countL<<" countR="<<countR<<" ans="<<ans<<endl;

	cout<<ans<<endl;
}

