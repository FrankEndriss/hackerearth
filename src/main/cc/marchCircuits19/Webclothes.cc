

#include <math.h> 
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

int t[100];
int a[100];
int main() {
int n, m, g, i;

	cin>>n>>m>>g;
	for(i=0; i<n; i++)
		cin>>t[i];
	for(i=0; i<m; i++)
		cin>>a[i];

	int maxDry=-1;
	for(i=1; i<n; i++) {
		int dry=t[i]-t[i-1];
		if(dry>maxDry)
			maxDry=dry;
	}
	int ans=0;
	for(i=0; i<m; i++) {
		if(a[i]<=maxDry)
			ans++;
	}
	cout<<ans<<endl;
}
