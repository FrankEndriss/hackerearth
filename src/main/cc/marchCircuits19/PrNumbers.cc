
#include <math.h> 
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

#define FOLLOWERS 8
//int follower[]={ 2, 3, 4, 5, 6, 7, 8, 9 }; // followers

int followed[][9]={ 
	{ 2, 3, 4, 5, 6, 7, 8, 9, 0 },	// 0
	{ 2, 0, 0, 0, 0, 0, 0, 0, 0 },	// 1	must not be used
	{ 2, 4, 6, 8, 0, 0, 0, 0, 0 },	// 2
	{ 3, 6, 9, 0, 0, 0, 0, 0, 0 }, 	// 3
	{ 2, 4, 6, 8, 0, 0, 0, 0, 0 },	// 4
	{ 5, 0, 0, 0, 0, 0, 0, 0, 0 },	// 5
	{ 2, 3, 4, 6, 8, 0, 0, 0, 0 },	// 6
	{ 7, 0, 0, 0, 0, 0, 0, 0, 0 },	// 7
	{ 2, 4, 6, 8, 0, 0, 0, 0, 0 },	// 8
	{ 3, 6, 9, 0, 0, 0, 0, 0, 0 }	// 9
};


long long toLong(int *num) {
long long n=0;
int i=0;
long long tens=1;
	while(num[i++]>0) {
		n+=num[i-1]*tens;
		tens*=10;
	}
	return n;
}

int gnum[100];
#define META 99
// gnum[META] is the highest ever flipped index of that num


/** flips digit num[idx] to next possible value.
 * Set all previous (0..idx-1) to first possible value. */
void flip(int *num, int idx) {
	num[META]=max(num[META], idx);
	int *possibleDigit=followed[num[idx+1]];
	int i=0;
	while(possibleDigit[i]<=num[idx] && possibleDigit[i]>0)
		i++;
	if(possibleDigit[i]==0) {
//cout<<"log flip to next num, idx="<<idx<<endl;
		flip(num, idx+1);
	} else {
		num[idx]=possibleDigit[i];
//cout<<"log flip current num to:"<<num[idx]<<endl;
		for(i=idx-1; i>=0; i--) {
			num[i]=followed[num[i+1]][0];
//cout<<"log set num["<<i+1<<"] to fist possible value: "<<num[i]<<endl;
		}
	}
}

//int singleDigitPrimes[]={ 2, 3, 5, 7 };

int checkIfRule2(int *num) {
//int containsSDP[]={ 0, 0, 0, 0 };
	if(num[0]==5 || num[0]==7)	// all 5s or all 7s, is divisable by 5 or 7
		return !0;

int i=0;
int has2=0;
int has3=0;
	while(num[i]>0) {
		if(num[i]==2) {
			has2=1;
			if(has3)
				break;
		} else if(num[i]==3) {
			has3=1;
			if(has2)
				break;
		}
		i++;
	}

	int ret=!has2 || (has2 && num[0]%2==0);
	if(ret && has3) {
		// check if divisable by 3
		i=0;
		int dSum=0;
		while(num[i]>0)
			dSum+=num[i++];
		ret=ret && dSum%3==0;
	}

	return ret;
}

void nextNum(int *num) {
	do {
		flip(num, 0);
	}while(!checkIfRule2(num));
}

int Rnum[100];

/** @return true if num1 is less or equal num2. */
int le(int *num1, int *num2) {
	if(num1[META]>num2[META]) {
		return 0;
	} else if(num1[META]<num2[META]) {
		return 1;
	} else {
		for(int i=num1[META]; i>=0; i--) {
			if(num1[i]>num2[i])
				return 0;
			else if(num2[i]>num1[i])
				return 1;
		}
	}
	return 1; // equal
}

// 1. No pair of adjacent digits are coprime i.e.  adjacent digits in a PR number will not be coprime to each other.
int main() {
long long L, R;
	cin>>L>>R;

	int i=0;
	long long l=L-1;
	while(l>0) {
		gnum[i++]=l%10;
		l/=10;
	}
	gnum[META]=i-1;

	i=0;
	while(R>0) {
		Rnum[i++]=R%10;
		R/=10;
	}
	Rnum[META]=i-1;

	long long ans=-1;
	while(le(gnum, Rnum)) {
		nextNum(gnum);
		ans++;
		cout<<"log, "<<ans+1<<". "<<toLong(gnum)<<endl;
	}
	cout<<ans<<endl;
}

