
#include <math.h> 
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

int MOD = 1000000007;
int A[100001];
vector<long long> P;
int dp[100001];
int n;
long long l, r;

int solve(int start) {
//cout<<"log solving, start="<<start<<" n="<<n<<" P[start]="<<P[start]<<endl;

	//long long sum=0;
	long long count=0;
	
	// find start, end
	int lowIdx=lower_bound(P.begin()+start, P.end(), l+P[start]) -P.begin()-1; 
	int upIdx= upper_bound(P.begin()+start, P.end(), r+P[start]) -P.begin()-1;
//cout<<"log lowIdx="<<lowIdx<<" upIdx="<<upIdx<<endl;
	for(; lowIdx<upIdx; lowIdx++) {
		if(lowIdx==n-1)
			count=(count+1)%MOD;
		else
			count=(count+dp[lowIdx+1])%MOD;
	}

	// might speed this up by using prefix sums of A and binary search
	/**
	for(int i=start; i<n && sum<=r; i++) {
		sum+=A[i];
		if(sum>=l && sum<=r) { // possible prefixes
			if(i==n-1)
				count=(count+1)%MOD;
			else
				count=(count+dp[i+1])%MOD;
		}
	}
	*/

//cout<<"log solved, start="<<start<<" count="<<count<<endl;
	return dp[start]=count%MOD;
}

/** Find number of possible partitions in postfix starting at start. Do caching. Recurse. */
int solveStacked() {
	for(int i=n-1; i>=0; i--) 
		solve(i);
	return dp[0];
}

int main() {
	cin>>n>>l>>r;
	
	P.push_back(0);
	for(int i=0; i<n; i++) {
		cin>>A[i];
		dp[i]=-1;
		P.push_back(P[i]+A[i]);
	}
	int ans=solveStacked();
	cout<<ans<<endl;
}
