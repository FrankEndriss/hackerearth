

#include <math.h> 
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

int n;
int L[2][100003][2];	// Points on L[0] and L[1]
int Lidx[2][100003];	// Indexes sorted by x,y, points to L[]

pair<int,int> Lpairs[100003];	// Pairs of of indexes of points
int pairsByF[100003];	// pairs sorted by first
int pairsByS[100003];	// pairs sorted by second

set<int> Lx[100003];		// crossings
int LxIdx[100003];	// crossings counts idx

int s;

bool byLx(int i1, int i2) {
	return Lx[i1]<Lx[i2];
}

bool byMax(pair<int,int> p1, pair<int,int> p2) {
	return max(p1.first, p1.second)<max(p2.first, p2.second);
}

bool byFirst(pair<int,int> p1, pair<int,int> p2) {
	return p1.first<p2.first;
}

bool bySecond(pair<int,int> p1, pair<int,int> p2) {
	return p1.second<p2.second;
}

bool byXY(int i1, int i2) {
//cout<<"log byXY, s="<<s<<" i1="<<i1<<" i2="<<i2<<endl;
	int dx=L[s][i1][0]-L[s][i2][0];
	if(dx==0)
		return L[s][i1][1]<L[s][i2][1];
	else
		return dx<0;
}

int main() {
	cin>>n;
	for(int i=0; i<n; i++) {
		cin>>L[0][i][0]>>L[0][i][1]>>L[1][i][0]>>L[1][i][1];
		Lidx[0][i]=i;
		Lidx[1][i]=i;
		pairsByF[i]=i;
		pairsByS[i]=i;
		LxIdx[i]=i;
	}

	// 1. + 2.
	for(s=0; s<2; s++)
		sort(&Lidx[s][0], &Lidx[s][n], byXY);

	for(int i=0; i<n;i++)
		Lpairs[i]=make_pair(Lidx[0][i], Lidx[1][i]);

	sort(&Lpairs[0], &Lpairs[n], byFirst);
	
	//sort(&pairsByF[0], &pairsByF[n], byFirst);
	//sort(&pairsByS[0], &pairsByS[n], bySecond);

	// find crossings
	int crossCount=0;
	for(int i=0; i<n; i++) {
		for(int j=i+1; j<n; j++)  {
			// i.first < j.first
			if(Lpairs[i].second>=Lpairs[j].second) {
				Lx[i].insert(j);
				Lx[j].insert(i);
				crossCount+=2;
			}
		}
	}

	// fuck this shit... idk

}
