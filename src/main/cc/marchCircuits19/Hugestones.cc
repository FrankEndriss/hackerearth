

#include <math.h> 
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

int s[50000][3];
int n;

typedef struct triple { double v[3]; } triple;
#define X 0
#define Y 1
#define R 2

/** Calc and return costs for placing the circle at x,y with radius r. */
double costs(const double x, const double y, const double r) {
double sum=0;
int i;
	//cout<<"**log costs("<<x<<", "<<y<<", "<<r<<")"<<endl;
	for(i=0; i<n; i++) {
		const double dx=x-s[i][0];
		const double dy=y-s[i][1];
		const double d=sqrt(dx*dx+dy*dy);
		//cout<<"log dx="<<dx<<" dy="<<dy<<" d="<<d<<" w="<<s[i][2]<<endl;
		const double c=abs(d-r)*s[i][2];
		//cout<<"log costs["<<i<<"]="<<c<<endl;
		sum+=c;
	}
	//cout<<"log costs: x="<<x<<" y="<<y<<" r="<<r<<" => sum="<<sum<<endl;
	return sum;
}

double findMedian(int d) {
int i;
double sum=0, count=0;
	for(i=0; i<n; i++) {
		sum+=s[i][d]*s[i][2];
		count+=s[i][2];
	}
	return sum/count;
}

double findRadius(const double x, const double y) {
double dists[n];
int i;
	double sum=0, count=0;
	for(i=0; i<n; i++) {
		double dX=x-s[i][0];
		double dY=y-s[i][1];
		sum+= sqrt(dX*dX+dY*dY)*s[i][2];
		count+=s[i][2];
	}
	return sum/count;
}

/** Tries to optimize the value sol.v[idx]. */
triple optimize2(triple sol, int idx, double bound, int steps) {
triple tmp=sol;
	double iterP=sol.v[idx];
	double iterN=sol.v[idx];
	double step=bound/steps;
	double optCosts=costs(sol.v[X], sol.v[Y], sol.v[R]);
	double optVal=sol.v[idx];
	for(int i=0; i<steps; i++) {
		iterP+=step;
		tmp.v[idx]=iterP;
		double tmpCosts=costs(tmp.v[X], tmp.v[Y], tmp.v[R]);
		if(tmpCosts<optCosts) {
			optCosts=tmpCosts;
			optVal=tmp.v[idx];
		}
		iterN-=step;
		tmp.v[idx]=iterN;
		tmpCosts=costs(tmp.v[X], tmp.v[Y], tmp.v[R]);
		if(tmpCosts<optCosts) {
			optCosts=tmpCosts;
			optVal=tmp.v[idx];
		}
	}
	sol.v[idx]=optVal;
	return sol;
}

triple optimze(triple sol, int steps, double factor) {
triple ret;
	double lowerR=sol.v[R]/factor;
	double upperR=sol.v[R]*factor;
	double stepR=(upperR-lowerR)/steps;
	ret.v[R]=sol.v[R];

	double lowerX=sol.v[X]-(sol.v[R]*factor/2);
	double upperX=sol.v[X]+(sol.v[R]*factor/2);
	double stepX=(upperX-lowerX)/steps;
	ret.v[X]=sol.v[X];

	double lowerY=sol.v[Y]-(sol.v[R]*factor/2);
	double upperY=sol.v[Y]+(sol.v[R]*factor/2);
	double stepY=(upperY-lowerY)/steps;
	ret.v[Y]=sol.v[Y];

	//cout<<"log try r: "<<lowerR<<" "<<upperR<<" "<<stepR<<endl;
	//cout<<"log try x: "<<lowerX<<" "<<upperX<<" "<<stepX<<endl;
	//cout<<"log try y: "<<lowerY<<" "<<upperY<<" "<<stepY<<endl;
	double optCosts=costs(sol.v[X], sol.v[Y], sol.v[R]);
	for(double x=lowerX; x<upperX; x+=stepX) {
		for(double y=lowerY; y<upperY; y+=stepY) {
			for(double r=lowerR; r<upperR; r+=stepR) {
				double c=costs(x, y, r);
				if(c<optCosts) {
					optCosts=c;
					ret.v[R]=r;
					ret.v[X]=x;
					ret.v[Y]=y;
				}
			}
		}
	}

	return ret;
}

int main() {
int i;
	cin>>n;
	for(i=0; i<n; i++)  {
		cin>>s[i][0]>>s[i][1]>>s[i][2];
		//cout<<"did read: "<<s[i][0]<<" "<<s[i][1]<<" "<<s[i][2]<<endl;
	}

double x, y, r;
	x=findMedian(0);
	y=findMedian(1);
	// the cost to move stone[i] is:
	// distCenter=sqrt((x-s[i][0])^2*(y-s[i][1])^2)
	// distCircle=abs(distCenter-r)
	// cost=distCircle*s[i][2]
	// The cost depends on x, y, r.
	//
	// step1: Find the gravity center independend
	// for x and for y.
	// step2: Find a radius where the weighted half
	// of the stones are inside, and the other half outside.
	// Any such radius is not that bad.
	r=findRadius(x, y);
	double c=costs(x, y, r);

	// step3: Try other values for x, y, r, then use the cheapest one.
#define STEPS 50
	triple ans={ x, y, r };
	//cout<< "log "<< ans.x << " " << ans.y << " " << ans.r << endl;
	
	double bound=ans.v[R]*4.0;
	if(bound<1)
		bound=1;
	int mis=0;
	for(int j=0; j<22; j++) {
	for(i=0; i<3; i++) {
		triple val=optimize2(ans, i, bound, STEPS);
		if(ans.v[i]==val.v[i] && ++mis==3) {
			mis=0;
			bound*=0.6;
			//cout<<"log new bound="<<bound<<endl;
		}
		ans=val;
		//cout<< "log "<< ans.v[X] << " " << ans.v[Y] << " " << ans.v[R] << endl;
	}
	}
	if(ans.v[X]<-1000000)
		ans.v[X]=-1000000;
	if(ans.v[X]>1000000)
		ans.v[X]=1000000;
	if(ans.v[Y]<-1000000)
		ans.v[Y]=-1000000;
	if(ans.v[Y]>1000000)
		ans.v[Y]=1000000;
	if(ans.v[R]<-4000000)
		ans.v[R]=-4000000;
	if(ans.v[R]>4000000)
		ans.v[R]=4000000;

	cout<< ans.v[X] << " " << ans.v[Y] << " " << ans.v[R] << endl;
}
