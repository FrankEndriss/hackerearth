
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* LCA based on euler path with segment tree and tin/tout. */
struct LCA {
    vector<int> height, euler, first, segtree;
    vector<bool> visited;
    int n;
    int time;
    vector<int> tin;
    vector<int> tout;

    vector<pii> p;

    LCA(vector<vector<pii>> &adj, int root = 0) {
        n = (int)adj.size();
        height.resize(n);
        first.resize(n);
        euler.reserve(n * 2);
        visited.assign(n, false);
        tin.resize(n);
        tout.resize(n);
        p.resize(n);
        time=1;
        dfs(adj, root);
        int m = (int)euler.size();
        segtree.resize(m * 4);
        build(1, 0, m - 1);
    }

    void dfs(vector<vector<pii>> &adj, int node, int h = 0) {
        tin[node]=time++;
        visited[node] = true;
        height[node] = h;
        first[node] = (int)euler.size();
        euler.push_back(node);
        for (auto to : adj[node]) {
            if (!visited[to.first]) {
                p[to.first]= {node, to.second};
                dfs(adj, to.first, h + 1);
                euler.push_back(node);
            }
        }
        tout[node]=time++;
    }

    void build(int node, int b, int e) {
        if (b == e) {
            segtree[node] = euler[b];
        } else {
            int mid = (b + e) / 2;
            build(node << 1, b, mid);
            build(node << 1 | 1, mid + 1, e);
            int l = segtree[node << 1], r = segtree[node << 1 | 1];
            segtree[node] = (height[l] < height[r]) ? l : r;
        }
    }

    int query(int node, int b, int e, int L, int R) {
        if (b > R || e < L)
            return -1;
        if (b >= L && e <= R)
            return segtree[node];
        int mid = (b + e) >> 1;

        int left = query(node << 1, b, mid, L, R);
        int right = query(node << 1 | 1, mid + 1, e, L, R);
        if (left == -1)
            return right;
        if (right == -1)
            return left;
        return height[left] < height[right] ? left : right;
    }

    /* @return true if parent is ancestor of node in O(1) */
    bool isAncestor(int node, int parent) {
        return tin[node]>tin[parent] && tin[node]<tout[parent];
    }

    /* @return the lca of u and v */
    int lca(int u, int v) {
        int left = first[u], right = first[v];
        if (left > right)
            swap(left, right);
        return query(1, 0, (int)euler.size() - 1, left, right);
    }
};

void solve() {
    cini(n);
    cini(q);

    vector<vector<pii>> adj(n);
    for(int i=0; i<n-1; i++) {
        cini(u);
        cini(v);
        cini(w);
        u--;
        v--;
        adj[u].push_back({v,w});
        adj[v].push_back({u,w});
    }

    LCA lca(adj);

    for(int i=0; i<q; i++) {
        cini(u);
        cini(v);
        cini(x);
        u--;
        v--;
        int pp=lca.lca(u,v);

        int ans=0;
        for(int node : {
                    u, v
                }) {
            while(node!=pp) {
                ans=max(ans, lca.p[node].second ^ x);
                node=lca.p[node].first;
            }
        }
        cout<<ans<<endl;
    }
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

