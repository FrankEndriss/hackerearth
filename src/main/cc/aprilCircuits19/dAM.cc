/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define forn(i, n) for(ll i=0; (i)<(n); (i)++)
#define fortn(t, i, n) for(t i=0; (i)<(n); (i)++)

int answer(vector<uint> &b, vector<uint> &c) {
    fortn(uint, i, b.size())
    cout<<b[i]<<" ";
    cout<<endl;

    fortn(uint, i, c.size())
    cout<<c[i]<<" ";
    cout<<endl;
    return 0;
}

/* Brute-Force permutations checking for maximum points.
 */
/* Calc points of b and c. (offset pos)
 */
int points(uint pos, vector<uint> &a, vector<uint> &b, vector<uint> &c) {
    int ans=0;
    for(uint i=0; i<b.size(); i++) {
        if(a[b[i]+pos-1]<a[c[i]+pos-1])
            ans++;
    }
    return ans;
}

/** k is number of digits, higest number is 2*k.
 * This func creates the next permutation, ret
 * false if not possible because the highest one reached.
 */
bool perm(vector<uint> &b) {
    /* Move the rightmost moveable digit from left to right one place.
     * After that move, move all right of the moved on to the leftmost
     * possible place.
     **/
    uint up=b.size()*2;
    for(int i=b.size()-1; i>=0; i--) {
        if(b[i]<up) {
            b[i]++;
            for(uint j=i+1; j<b.size(); j++)
                b[j]=b[j-1]+1;
            return true;
        }
        up--;
    }
    return false;
}

void mirror(vector<uint> &b, vector<uint> &c) {
    c.clear();
    uint lo=0;
    for(uint i=1; i<=b.size()*2; i++) {
        if(b[lo]!=i)
            c.push_back(i);
        else
            lo++;
    }
}

/** counts the current sequentials */
int currSeqCount(vector<uint> &v) {
    uint ret=1;
    int pos=v.size()-1;
    while(pos>0) {
        if(v[pos]==v[pos-1]+1)
            ret++;
    }
    return ret;
}

void findOpt(uint k, uint i, uint len, vector<uint> &a, vector<uint> &b, vector<uint> &c) {
//cout<<"log findOpt, i="<<i<<" len="<<len<<endl;
    vector<uint> tb, tc;
    vector<uint> mb;

/*
// currSeqCount(...), create bad prefixes
    int seqCountB=currSeqCount(b);
    vector<uint> badPrefixB;
    for(int j=0; j<k-seqCountB; j++) {
        badPrefixB.push_back(
    }
    int seqCountC=currSeqCount(c);
    vector<uint> badPrefixC;
*/


    forn(j, len)
        tb.push_back(j+1);
//cout<<"log tb.size()="<<tb.size()<<endl;

    int ma=-1;
    do {
        mirror(tb, tc);
        int p=points(i*2, a, tb, tc);
        if(p>ma) {
            ma=p;
            mb=tb;  // copy
            if(ma==(int)len) // opt, more not possible
                break;
        }
    } while(perm(tb));
    mirror(mb, tc);
    fortn(uint, j, mb.size()) {
        b.push_back(mb[j]+i*2);
        c.push_back(tc[j]+i*2);
    }
}

void swap(uint idx, vector<uint> &b, vector<uint> &c) {
    uint tmp=b[idx];
    b[idx]=c[idx];
    c[idx]=tmp;
}

/** Checks for max sequential. */
bool seqCheck(int k, vector<uint> &b, vector<uint> &c)  {
    bool ret=false;
    int bseq=1;
    int cseq=1;
    for(uint i=1; i<b.size(); i++) {
        if(b[i]==b[i-1]+1)
            bseq++;
        else
            bseq=1;

        if(c[i]==c[i-1]+1)
            cseq++;
        else
            cseq=1;

        if(bseq==k || cseq==k) {
            ret=true;
            b[i]=i*2;
            c[i]=i*2+1;
            if(b[i]==b[i-1]+1 || c[i]==c[i-1]+1)
                swap(i, b, c);
            bseq=1;
            cseq=1;
        }
        uint j=i+1;
        while(j<b.size() && (b[j]<b[j-1] || c[j]<c[j-1])) {
            b[j]=j*2;
            c[j]=j*2+1;
            //swap(j, b, c);
            ret=true;
            j++;
        }
    }
    return ret;
}

int main() {
    uint n, k;
    cin>>n>>k;
    vector<uint> a(n*2);
    forn(i, n*2)
    cin>>a[i];

    vector<uint> b, c;

    if(k==2) {

        /* count odd/even, flip accordingly */
        uint evenC=0;
        for(uint i=0; i<n; i++) {
            b.push_back(i*2+1);
            c.push_back(i*2+2);

            if(a[i*2]<a[i*2+1])
                evenC++;
        }
        if(evenC<n/2)
            b.swap(c);

    } else {

        uint step=min(min(k-1, (uint)8), n);   // limit blocksize max 8
        for(uint i=0; i<n; i+=step) {
            const uint block=min(step, n-i);
            findOpt(k, i, block, a, b, c);
        }

        while(seqCheck(step+1, b, c)); // NOTE step instead of k
    }

    return answer(b, c);
}

