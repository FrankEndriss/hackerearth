/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)
#define fauto(c, vec) for(auto c : (vec))

int main() {
    int n, m, a, b;
    cin>>n>>m>>a>>b;

    vector<pair<int, int>> mapp[n+1][m+2];   // maps positions to reachable positions.

    vector<string> lines(n+1);
    getline(cin, lines[0]); // need to read some newline for some reason
    for(int i=0; i<n+1; i++)
        getline(cin, lines[i]);

    for(int y=1; y<n+1; y++) {
        for(int x=1; x<m*2; x+=2) {
            if(lines[y][x-1]==' ')
                mapp[y][x/2+1].push_back({y, x/2});      // left
            if(lines[y][x+1]==' ')
                mapp[y][x/2+1].push_back({y, x/2+2});    // right
            if(lines[y-1][x]==' ')
                mapp[y][x/2+1].push_back({y-1, x/2+1});  // up
            if(lines[y][x]==' ')
                mapp[y][x/2+1].push_back({y+1, x/2+1});  // down
        }
    }
    /* now create map of vendor occupied places, value is day that place
    * was occupied first.
    **/
    vector<vector<int>> vd(n+1, vector<int>(m+1));
    int k;
    int day=1;
    cin>>k;
    vector<pair<int, int>> next;
    for(int i=0; i<k; i++) {
        int la, lb;
        cin>>la>>lb;
        vd[la][lb]=1;
        next.push_back({la, lb});
    }

    while(next.size()) {
        day++;
        vector<pair<int, int>> nextnext;
        for(auto p : next) {
            for(auto padj : mapp[p.first][p.second]) {
                if(vd[padj.first][padj.second]==0) {
                    vd[padj.first][padj.second]=day;
                    nextnext.push_back(padj);
                }
            }
        }
        next.swap(nextnext);
    }

    // now same with my position. But we cannot step on positions occupied by vendors
    // on the _first_ day.
    vector<vector<bool>> merun(n+1, vector<bool>(m+1, false));
    next.push_back({a, b});
    int ans=0;
    while(next.size()) {
        vector<pair<int, int>> nextnext;
        for(auto p : next) {
            ans=max(ans, vd[p.first][p.second]-1);
            for(auto padj : mapp[p.first][p.second]) {
                if(vd[padj.first][padj.second]>1) {
                    if(!merun[padj.first][padj.second]) {
                        nextnext.push_back(padj);
                    }
                    merun[padj.first][padj.second]=true;
                }
            }
        }
        next.swap(nextnext);
    }
    cout<<ans<<endl;
}

