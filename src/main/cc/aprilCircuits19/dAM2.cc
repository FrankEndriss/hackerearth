/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define forn(i, n) for(ll i=0; (i)<(n); (i)++)
#define fortn(t, i, n) for(t i=0; (i)<(n); (i)++)
#define fauto(c, vec) for(auto c : (vec))

int answer(vector<uint> &b, vector<uint> &c) {
    fortn(uint, i, b.size())
    cout<<b[i]<<" ";
    cout<<endl;

    fortn(uint, i, c.size())
    cout<<c[i]<<" ";
    cout<<endl;
    return 0;
}

/* Brute-Force permutations checking for maximum points.
 */
/* Calc points of b and c. (offset pos)
 */
int points(uint pos, vector<uint> &a, vector<uint> &b, vector<uint> &c) {
    int ans=0;
    for(uint i=0; i<b.size(); i++) {
        if(a[b[i]+pos-1]<a[c[i]+pos-1])
            ans++;
    }
    return ans;
}

/** k is number of digits, higest number is 2*k.
 * This func creates the next permutation, ret
 * false if not possible because the highest one reached.
 */
bool perm(vector<uint> &b) {
    /* Move the rightmost moveable digit from left to right one place.
     * After that move, move all right of the moved on to the leftmost
     * possible place.
     **/
    uint up=b.size()*2;
    for(int i=b.size()-1; i>=0; i--) {
        if(b[i]<up) {
            b[i]++;
            for(uint j=i+1; j<b.size(); j++)
                b[j]=b[j-1]+1;
            return true;
        }
        up--;
    }
    return false;
}

void mirror(vector<uint> &b, vector<uint> &c) {
    c.clear();
    uint lo=0;
    for(uint i=1; i<=b.size()*2; i++) {
        if(b[lo]!=i)
            c.push_back(i);
        else
            lo++;
    }
}

/** counts the current sequentials */
int currSeqCount(vector<uint> &v) {
    uint ret=1;
    int pos=v.size()-1;
    while(pos>0) {
        if(v[pos]==v[pos-1]+1)
            ret++;
    }
    return ret;
}

typedef struct seq {
    int start;
    int len;
    int val;
} seq;

void index_sequentials(vector<seq> &seqs, vector<int> &a, uint n);
for(uint i=0; i<n*2; i+=2) {
    int j=i;
    while(j<n-2 && a[j]==a[j+1])
        j++;
    int c=j-i+1;
    if(c>1) {
        seqs.push_back({i, c, a[i] });
    }
    i=j;
    if(i%2)
        i++;
}
}

int main() {
    uint n, k;
    cin>>n>>k;
    vector<uint> a(n*2);
    forn(i, n*2)
    cin>>a[i];

    vector<uint> b, c;

    if(k==2) {

        /* count odd/even, flip accordingly */
        uint evenC=0;
        for(uint i=0; i<n; i++) {
            b.push_back(i*2+1);
            c.push_back(i*2+2);

            if(a[i*2]<a[i*2+1])
                evenC++;
        }
        if(evenC<n/2)
            b.swap(c);

    } else {

        /* TODO find sequences of same number, index them
         * for all such seqs do:
         * check if a left prefix can be made to points with the
         * numbers before the seq.
         * With the remaining to the right, check the numbers to the right.
         * Output remaining numbers in the middle for no points.
         */

        /* Step 1, Index sequentials starting at even indexes (idx, len). */

        vector<seq> seqs;
        index_sequentials(seqs, a, n);

        /* Step 2, Find (and store) blocks before and after the sequentials.
        * We can use at most K letters before and after the sequential.
        * We need to decide if they are lower or bigger than the sequential value.
        * So, we need a block of lower or bigger values.
        * pi-3, pi-2, pi-1, ai, ai+1, ai+2, ...
        * Now index pX with the diff of bigger/lower values as a[i]. Stop at maxlen K.
        * Use the Index at the biggest diff.
        */
        for(auto s : seqs) {
            int ma=min(min(k, s.len), s.start+1); // maxlen of block left of seq
            vector<int> mad;
            int bg=0;
            int lt=0;
            for(int i=0; i<ma; i++) {
                if(a[s.start-1-i]>a[s.start])
                    bg++;
                else if(a[s.start-1-i]<a[s.start])
                    lt++;
                mad.push_back(bg-lt);
                // TODO build block....
            }
        }
    }

    return answer(b, c);
}

