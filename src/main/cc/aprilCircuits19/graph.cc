/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

vector<pair<char, char>> tree[101];

// since number of node is fairly small, use string (like vector<char>) for pathes.
vector<pair<string, vector<string>> looplessPaths; // list of indipendent paths with its loops

/* Brute force. Simply run all possible paths, if k-th step finishes at target we are fine.
 * Do this until count >= k. Wont work well for huge k or long paths.
 */
void dfs(int node, string &path, int target) {
    
    for(int c : tree[node]) {
        auto cInPath=path.find(c.first);
        if(cInPath==string::npos) {  // notfound
            path.append(c.first);
            if(c.first==target)
                looplessPaths.push_back(path); // copy
            else
                dfs(c.first, npath, target);
            path=path.substr(0, path.size()-1);
        } else {    // loop found
            string loop=path.substr(cInPath, path.end());
            loops.push_back(loop);
        }
    }
}

int main() {
int n, m, p, nc, u, v;
    cin>>n>>m>>p>>nc>>u>>v;

    vector<int> c;  
    for(int i=0; i<nc; i++)
        cin>>c[i];
    int k;
    cin>>k;
    
    for(int i=0; i<m; i++) {
        int v1, v2, w;
        cin>>v1>>v2>>w;
        tree[v1].push_back({(char)v2, (char)w});
        tree[v2].push_back({(char)v1, (char)w});
    }

    // now find number of pathes which include { c[0], c[1]...c[n]} at least k times.
    // Note that there are loops.
    //
    // 1. dfs the loopless, indipendent paths.
    // 2. Find the loops for each indipendent path.
    // 3. Combine both each indipendent path with its loops.

    string path;
    dfs(u, path, v);

    // now we can insert loops into loopless paths at certain possitions.
    // Count number of ways this leads to the key.
    // How???
    
}

