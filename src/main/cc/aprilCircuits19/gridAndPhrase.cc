
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)
#define fauto(c, vec) for(auto c : (vec))

int main() {
    int n, m;
    cin>>n>>m;
    vector<string> s(n);
    fauto(&str, s)
    cin>>str;

    const string saba="saba";
    const struct { int x, y; } off[4][3]= {
        { {1, 1}, {2, 2}, {3, 3}, },
        { {1, 0}, {2, 0}, {3, 0}, },
        { {0, 1}, {0, 2}, {0, 3}, },
        { {1, -1}, {2, -2}, {3, -3} }
    };

    int ans=0;
    forn(y, n) {
        forn(x, m) {
            if(s[y][x]!=saba[0])
                continue;

            forn(r, 4) { // four rules
                bool lans=true;
                forn(c, 3) { // three chars per rule
                    const int offX=x+off[r][c].x;
                    const int offY=y+off[r][c].y;

                    if(offX<0 || offX>=m || offY<0 || offY>=n) {
                        lans=false;
                        break;
                    }

                    if(s[offY][offX]!=saba[c+1]) {
                        lans=false;
                        break;
                    }
                }
                if(lans)
                    ans++;
            }
        }
    }

    cout<<ans<<endl;

}

