/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

vector<int> treeF;

int main() {
    int n;
    cin>>n;
    treeF.resize(n);

    int maxChilds=0;
    fori(n-1) {
        int u, v;
        cin>>u>>v;
        treeF[u-1]++;
        maxChilds=max(maxChilds, treeF[u-1]);
        treeF[v-1]++;
        maxChilds=max(maxChilds, treeF[v-1]);
    }
    int mcount=0;
    fori(n) {
        if(treeF[i]==maxChilds)
            mcount++;
    }
    cout<<mcount<<endl;
    fori(n) {
        if(treeF[i]==maxChilds)
            cout<<i+1<<" ";
    }
    cout<<endl;
    return 0;
}

