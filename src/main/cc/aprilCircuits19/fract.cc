/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)
#define fauto(c, vec) for(auto c : (vec))

/** Better construct like a TREE, and count nodes while dfs
 * node->num is the index to the child nodes.
 **/

typedef struct node {
    ll num;
    ll den;
    struct node *next;
} node;

int main() {
    ll n, k;
    cin>>n>>k;

    if(k==1) {
        cout<<"0/1"<<endl;  
        return 0;
    }

    node *first=new node();
    first->num=0;
    first->den=1;
    first->next=new node();
    first->next->num=1;
    first->next->den=2;
    first->next->next=NULL;

    node *prev=first;
    node *curr=prev->next;
    int ncount=1;
    while(curr!=NULL) {
        while(prev->den+curr->den <= n) {
            node *nnode=new node();
            nnode->num=prev->num+curr->num;
            nnode->den=prev->den+curr->den;
            prev->next=nnode;
            nnode->next=curr;
            curr=nnode;
        }
        node *tmp=prev;
        prev=curr;
        curr=curr->next;
cout<<"delete "<<tmp->num<<"/"<<tmp->den<<endl;
        delete tmp;
        ncount++;
        if(ncount==k) {
            cout<<prev->num<<"/"<<prev->den<<endl;
            return 0;
        }
    }

    cout<<"1/2"<<endl;
    return 0;
}

