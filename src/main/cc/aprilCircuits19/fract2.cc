/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)
#define fauto(c, vec) for(auto c : (vec))

ll kcount;
ll n, k;

/* if number of nodes between l and r can be calculated, then that
 * number is returned.
 * Else 0.
 */
int count(const pair<int, int> &l, const pair<int, int> &r)  {
    if(l.second<4 && r.second>n/2) {
        return (n-n/2)/l.second;
    }

    return 0;
}

void dfs(const pair<int, int> &l, const pair<int, int> &r)  {
    if(kcount==0) {
        cout<<l.first<<"/"<<l.second<<endl;
        exit(0);
    }

    const pair<int, int> nn={ l.first+r.first, l.second+r.second };
    if(nn.second<=n) {
        int c=count(l, nn);
        if(c>0 && c<=kcount)
            kcount-=c;
        else {
            dfs(l, nn); 
            kcount--;
        }
        dfs(nn, r);
    }
}

int main() {
    cin>>n>>k;

    kcount=k-1;
    dfs({ 0, 1}, { 1, 2 });

    cout<<"1/2"<<endl;
    return 0;
}

