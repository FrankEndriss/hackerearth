/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)
#define fauto(c, vec) for(auto c : (vec))

/** Finds the longest block of left smallers, ie [3 5 8 4 6 9] -> 3 */
int findMaxBlock(int idx, vector<int> &a, uint maxK, bool &flip) {
    int retLeft=-1;
    for(uint k=2; k<maxK && idx+k*2<a.size(); k++) {
//cout<<"check left k="<<k<<endl;
        bool ok=true;
        for(uint i=0; i<k; i++) {
            if(a[idx+i]<a[idx+i+k])
                ;
            else {
//cout<<"check left break, a[idx+i]="<<a[idx+i]<<" a[idx+i+k]="<<a[idx+i+k]<<endl;
                ok=false;
                break;
            }
        }
        if(ok)
            retLeft=k;
    }

    int retRight=-1;
    for(uint k=2; k<maxK && idx+k*2<a.size(); k++) {
        bool ok=true;
        for(uint i=0; i<k; i++) {
            if(a[idx+i]>a[idx+i+k])
                ;
            else {
                ok=false;
                break;
            }
        }
        if(ok)
            retRight=k;
    }

//cout<<"retLeft="<<retLeft<<" retRight="<<retRight<<endl;
    int ret=max(retLeft, retRight);
    if(retLeft<retRight)
        flip=true;

    return ret;
}

uint countSames(vector<int> &v, int i) {
    uint idx=i;
    while(idx<v.size() && v[i]==v[idx++])
        ;
    return idx-i;
}

bool ruleKok(vector<int> v, int k, int value) {
    int count=0;
    for(int i=v.size()-1; i>=0; i--)
        if(v[i]==--value)
            count++;
        else
            break;
    return count<k-1;
}

int main() {
    uint n, k;
    cin>>n>>k;
    vector<int> a(n*2);
    vector<int> id;
    fori(n*2) {
        cin>>a[i];
        id.push_back(i);
    }

    vector<int> b, c;

#define Sol_trivial3
#ifdef Sol_blockbased
 /** Find blocks of same numbers.
 *  For all other numbers use simple swapping strategy.
 *  For Blocks:
 *  Define a Block by it starting pos, len and number.
 *  1. Find left of block in max distance k-1 subarrays with
 *  all less than block-number or all bg than block-number.
 *  Ex: k>5
 *  A: 1 2 3 3 4 6 6 6 6 6
 *  B: 1 2 3 4 5
 *  C: 6 7 8 9 10
 *  Ex: k=4
 *  A: 1 2 3 3 4 6 6 6 6 6
 *  B: 1 3 3 4 5
 *  C: 2 4 8 9 10
 */
    
#elif defined Sol_trivial
    /** all green, score 30.83 */
    fori(n) {
        b.push_back(i*2+1);
        c.push_back(i*2+2);
    }
#elif defined Sol_trivial2
    /** all green, score 28.01 */
    fori(n) {
        b.push_back(i*2+2);
        c.push_back(i*2+1);
    }
#elif defined Sol_trivial3
    /** all green, score 49.50 */
    if(k==2) {
        /* count odd/even, flip accordingly */
        uint evenC=0;
        fori(n) {
            b.push_back(i*2+1);
            c.push_back(i*2+2);

            if(a[i*2]<a[i*2+1])
                evenC++;
        }
        if(evenC>n/2)
            b.swap(c);
    } else {
        fori(n) {
            if(a[i*2]==a[i*2+1]) {
                uint cs=countSames(a, i*2);
                uint block=min(min(cs, k-2), (uint)(n-i));
                bool flip=true;
                if(i*2+cs<n)
                    if(a[i*2+cs]>a[i*2])
                        flip=false;
                for(uint j=0; j<block; j++)
                    (flip?c:b).push_back(i*2+1+j);
                for(uint j=0; j<block; j++)
                    (flip?b:c).push_back(i*2+1+j+block);
                i+=(block-1);
            } else if(a[i*2]>=a[i*2+1]) {
                if(ruleKok(c, k, i*2+1)) {
                    c.push_back(i*2+1);
                    b.push_back(i*2+2);
                } else {
                    b.push_back(i*2+1);
                    c.push_back(i*2+2);
                }
            } else {
                if(ruleKok(b, k, i*2+1)) {
                    b.push_back(i*2+1);
                    c.push_back(i*2+2);
                } else {
                    c.push_back(i*2+1);
                    b.push_back(i*2+2);
                }
            }
        }
    }
#else
    if(k==2) {
        /* count odd/even, flip accordingly */
        uint evenC=0;
        fori(n) {
            b.push_back(i*2+1);
            c.push_back(i*2+2);

            if(a[i*2]<a[i*2+1])
                evenC++;
        }
        if(evenC<n/2)
            b.swap(c);
    } else {
        bool flip=true;
        uint lastK=0;
        bool lastFlip=!flip;
        fori(n) {
            int pb=findMaxBlock(i*2, a, k, flip);
//cout<<"pb="<<pb<<endl;

            if(lastFlip!=flip && pb+lastK>=k) {
//cout<<"lastK="<<lastK<<" pb="<<pb<<endl;
                pb=1;
                flip=!flip;
            }

            if(pb>0) {
                for(int j=0; j<pb; j++) {
                    if(flip) {
                        c.push_back(i*2+j+1);
                        b.push_back(i*2+j+1+pb);
                    } else {
                        b.push_back(i*2+j+1);
                        c.push_back(i*2+j+1+pb);
                    }
                }
                i+=pb-1;
                lastFlip=flip;
                lastK=pb;
            } else {
                b.push_back(i*2+1);
                c.push_back(i*2+2);
                lastK=1;
                lastFlip=false;
            }
        }
    }
#endif

    fori(n)
    cout<<b[i]<<" ";
    cout<<endl;

    fori(n)
    cout<<c[i]<<" ";
    cout<<endl;
}

