/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

const int N=101;
ll fak[101];

ll gaus(int x) {
    return (1LL * x * x + x)/2;
}

int main() {
    ll n;
    cin>>n;
    ll ans=0;
    for(int i=2; i<n; i++)
        ans+=gaus(n-i)*(i/2);
    cout<<ans<<endl;
}

