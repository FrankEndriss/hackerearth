/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef const unsigned int cuint;
typedef const int cint;
typedef long long ll;
typedef const long long cll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

/** SQRT decomposition, with persistence. see
 * https://blog.anudeep2011.com/mos-algorithm/
*/
int main() {
    ll n, k, q;
    cin>>n>>k>>q;

    const int BLOCKSIZE=256;
    const int BLOCKCOUNT=n/BLOCKSIZE+1;

    /* Input values */
    vector<int> ballons(n);

    /* frequency of color n in input. */
    vector<int> f(64);

    /* Copy of f at BLOCKSIZE*i.
     * dp[L]== f of subarray ballons[0..L*BLOCKSIZE]
     */
    vector<vector<int>> dp1(BLOCKCOUNT);

    /* Number of ballons with K==k at block i
    */
    vector<vector<int>> dp3(BLOCKCOUNT, vector<int>(BLOCKCOUNT, -1));

    /* map ballons to smaller numbers, work with these smaller numbers.
    *  Colors are 1 based.
    */
    unordered_map<int, int> colorIdx;
    int cIdx=1;

    int bcount=0;
    int bidx=1;
    dp1[0]=f;    // dp1[0] is array of 0s
    fori(n) {
        int x;
        cin>>x;

        int xi; // indexed color
        if((xi=colorIdx[x])==0)
            xi=(colorIdx[x]=cIdx++);

        ballons[i]=xi;
//cout<<"did read color="<<xi<<endl;
        if(f.size()<=xi)
            f.resize(f.size()*2);
        f[xi]++;

        if(++bcount==BLOCKSIZE) {
            dp1[bidx]=f;     // copy of array
            bidx++;
            bcount=0;
        }
    }
    const int colorCount=colorIdx.size();
    colorIdx.clear();
    //cout<<"log parsed input, max color="<<cIdx-1<<endl;
    f.resize(colorCount+1);
    for(auto &v : dp1)
        v.resize(colorCount+1);

    ll ans=0;
    fori(q) {
        int il, ir;
        cin>>il>>ir;
//cout<<"query il="<<il<<" ir="<<ir<<" ans="<<ans<<endl;
        int l=min((il+ans)%n, (ir+ans)%n);
        int r=max((il+ans)%n, (ir+ans)%n);
//cout<<"query l="<<l<<" r="<<r<<" ans="<<ans<<endl;

        const int lBlock=l/BLOCKSIZE;
        const int rBlock=r/BLOCKSIZE;
        if(dp3[lBlock][rBlock]<0) { /* count and memoize k for blocks. */
            if(rBlock>lBlock) {
                int kCount=0;
                for(int i=1; i<=colorCount; i++) {
                    int lCount=dp1[rBlock][i]-dp1[lBlock][i];
                    if(lCount==k)
                        kCount++;
                }
                dp3[lBlock][rBlock]=kCount;
            } else
                dp3[lBlock][rBlock]=0;
        }
        ans=dp3[lBlock][rBlock];    // number of ballons which K==k at start of lBlock

        vector<int> lfreqs(colorCount+1);
//cout<<"count left lBlock="<<lBlock<<" rBlock="<<rBlock<<" lfreqs.size()="<<lfreqs.size()<<endl;

        for(int j=lBlock*BLOCKSIZE; j<l; j++) {
            //cout<<"count left check j="<<j<<" ballons[j]="<<ballons[j]<<" freq="<<lfreqs[ballons[j]]<<endl;
            --lfreqs[ballons[j]];
            int nfreq=lfreqs[ballons[j]]+dp1[rBlock][ballons[j]]-dp1[lBlock][ballons[j]];
            if(nfreq==k)
                ans++;
            else if(nfreq==k-1)
                ans--;
        }

//cout<<"count right lBlock="<<lBlock<<" rBlock="<<rBlock<<" lfreqs.size()="<<lfreqs.size()<<endl;
        for(int j=rBlock*BLOCKSIZE; j<=r; j++) {
            ++lfreqs[ballons[j]];
            //cout<<"count right check j="<<j<<" ballons[j]="<<ballons[j]<<" freq="<<lfreqs[ballons[j]]<<endl;
            int nfreq=lfreqs[ballons[j]]+dp1[rBlock][ballons[j]]-dp1[lBlock][ballons[j]];
            if(nfreq==k)
                ans++;
            else if(nfreq==k+1)
                ans--;
        }

        cout<<ans<<endl;
    }
    exit(0);
}
