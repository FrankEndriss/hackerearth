/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

//#define DEBUG

const int MOD=1e9+7;

int pl(int i1, int i2) {
    return (i1+i2)%MOD;
}

int mul(ll i1, ll i2) {
    return (int)((i1*i2)%MOD);
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);

    cini(x);
    const int N=max(x+1, 4);

    vvi dpWI(N, vi(N));    // dp[i][j]= for n=i there are dp[i][j] sets of size j with i in i 
    vvi dpWO(N, vi(N));    // dp[i][j]= for n=i there are dp[i][j] sets of size j without i in it

    dpWI[1][1]=1;   // {1}
    dpWO[1][1]=0;   // none

    dpWI[2][1]=1;   // {2}
    dpWO[2][1]=1;   // {1}

    dpWI[3][1]=1;   // {3}
    dpWO[3][1]=2;   // {1}{2}

    dpWI[3][2]=2;   // {3,1}{1,3}
    dpWO[3][2]=0;   // none

/*
    dpWI[4].push_back(1);   // {4}
    dpWO[4].push_back(3);   // {1}, {2}, {3}
    dpWI[4].push_back(4);   // {1,4}{4,1}{2,4}{4,2}
    dpWO[4].push_back(2);   // {1,3}{3,1}
*/

    for(int n=4; n<N; n++) {
        dpWI[n].resize(N/2+1);
        dpWO[n].resize(N/2+1);

        dpWI[n][1]=1;
        dpWO[n][1]=n-1;

        for(int j=2; j<=(n+1)/2; j++) {
            dpWI[n][j]=mul(dpWO[n-1][j-1], j);
#ifdef DEBUG
cout<<"n="<<n<<" j="<<j<<" dpWO[n][j]="<<dpWI[n][j]<<endl;
#endif
            dpWO[n][j]=pl(dpWI[n-1][j], dpWO[n-1][j]);
        }
    }

    int ans=0;
    for(int j=1; j<=(x+1)/2; j++) {
        ans=pl(ans, dpWI[x][j]);
        ans=pl(ans, dpWO[x][j]);
    }
    cout<<ans<<endl;
}

