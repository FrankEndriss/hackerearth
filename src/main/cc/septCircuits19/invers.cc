/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int INF=1e9+7;

//#define DEBUG

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cinai(a, n);

    vvi tree(n);
    for(int i=1; i<n; i++) {
        cini(p);
        tree[p-1].push_back(i);
    }

/** We need to traverse the tree "bottom up", returning a list
 * of child nodes values sorted by their value.
 * In every node we do a merge sort of these child lists.
 * For any entry we need to find the cnt of bigger numbers in all
 * child lists of this node.
 * From these cnts we can calc which child must go first etc.
 */

#define VAL 0
#define CHL 1
#define IDX 2

    function<vi(int)> dfs=[&](int node) {
#ifdef DEBUG
cout<<"dfs(), node="<<node<<" #childs="<<tree[node].size()<<endl;
#endif
        vvi chlLists(tree[node].size());
        vll chlWeight(tree[node].size());
        vi chlAvail(tree[node].size());
        priority_queue<tuple<int, int, int>> firsts; // { -value, child, idx }
        int sz=0;

        for(size_t i=0; i<tree[node].size(); i++) {
            chlLists[i]=dfs(tree[node][i]);
            firsts.push(make_tuple(-chlLists[i][0], i, 0 ));
            sz+=chlLists[i].size();
            chlAvail[i]=chlLists[i].size();
        }
#ifdef DEBUG
cout<<"dfs(), node="<<node<<" after child dfs, #childs="<<tree[node].size()<<" sz="<<sz<<endl;
#endif
        firsts.push(make_tuple( -a[node], -1, -1 ));     // special child index
        vi ret;

        while(firsts.size()) {
            vi chlUsed;

            auto p=firsts.top();
            do {
                p=firsts.top();
#ifdef DEBUG
cout<<"first, val="<<get<VAL>(p)<<" chl="<<get<CHL>(p)<<" idx="<<get<IDX>(p)<<endl;
#endif
                firsts.pop();
                ret.push_back(-get<VAL>(p));
                if(get<CHL>(p)>=0) {
                    sz--;
                    int chl=get<CHL>(p);
                    chlUsed.push_back(chl);
                    chlAvail[chl]--;
                    int nidx=get<IDX>(p)+1;
                    if(chlLists[chl].size()>nidx)
                        firsts.push(make_tuple( -chlLists[chl][nidx], chl, nidx ));
                }
            }while(firsts.size() && get<VAL>(p)==get<VAL>(firsts.top()));
#ifdef DEBUG
cout<<"after inner while, firsts.size()="<<firsts.size()<<endl;
#endif
            for(int chl : chlUsed)
                chlWeight[chl]-=sz;
#ifdef DEBUG
cout<<"after adding chlWeight"<<endl;
#endif
        }
assert(sz==0);
#ifdef DEBUG
cout<<"after outer while, will sort"<<endl;
#endif

        /* sort children by weight */
        vi ctmp(tree[node].size());
        for(int i=0; i<tree[node].size(); i++) {
            ctmp[i]=i;
            chlWeight[i]/=chlLists[i].size();
        }

        sort(ctmp.begin(), ctmp.end(), [&](int c1, int c2) {
            return chlWeight[c1]<chlWeight[c2];
        });
        for(int i=0; i<tree[node].size(); i++)
            ctmp[i]=tree[node][ctmp[i]];
        for(int i=0; i<tree[node].size(); i++)
            tree[node][i]=ctmp[i];

#ifdef DEBUG
cout<<"did sort, ret.size()="<<ret.size()<<endl;
#endif
        return ret;
    };

    dfs(0);
    for(int i=0; i<tree.size(); i++) {
        if(tree[i].size()==0)
            cout<<0;
        else {
            for(int c : tree[i])
                cout<<(c+1)<<" ";
        }
        cout<<endl;
    }
}

