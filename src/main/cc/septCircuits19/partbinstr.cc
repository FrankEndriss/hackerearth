/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

//#define DEBUG

const int N=5001;
const int M1=128;
const int M2=64;
string s;
//vvvi mem2(N, vvi(M1, vi(M2, -1)));
vvvi mem2;

typedef pair<size_t, pair<int, int>> ltuple;

map<ltuple, int> mem;

ltuple lmake_tuple(const size_t v1, const int v2, const int v3) {
    return { v1, { v2, v3}};
}

int solveCalls[10];
int memo(const size_t idx, const int len, const int ones) {
    if(len<M1 && ones<M2) {
#ifdef DEBUG
        solveCalls[4]++;
#endif
        return mem2[idx][len][ones];
    } else {
#ifdef DEBUG
        solveCalls[5]++;
#endif
        auto it=mem.find(lmake_tuple(idx,len,ones));
        if(it!=mem.end())
            return it->second;
        else
            return -1;
    }
}

void putMemo(const size_t idx, const int len, const int ones, const int value) {
    if(len<M1 && ones<M2)
        mem2[idx][len][ones]=value;
    else
        mem[lmake_tuple(idx,len,ones)]=value;
}

const int MOD=1e9+7;

vector<size_t> dp;   // indexes of all 1s in s

int pl(int i1, int i2) {
    return (i1+i2)%MOD;
}

/** @return number of ways to partition string starting at idx
 * with previous len len and previous number of 1s ones.
 */
int solve(const size_t idx, const int minLen, const int minOnes) {
    assert(minLen>0);
    assert(minOnes>0);

#ifdef DEBUG
    solveCalls[0]++;
#endif
    if(minLen+dp[idx]>s.size())
        return -1;

    if(idx+minOnes>=dp.size())
        return -1;

#ifdef DEBUG
    solveCalls[1]++;
#endif
    const int lret=memo(idx, minLen, minOnes);
    if(lret>=0)
        return lret;

#ifdef DEBUG
    solveCalls[2]++;
#endif

#ifdef DEBUG
    //cout<<"solve, idx="<<idx<<" minLen="<<minLen<<" minOnes="<<minOnes<<endl;
#endif

    int ans=1;

/* lidx == index of '1' following the prefix. */
    for(size_t lidx=idx+minOnes; lidx<dp.size(); lidx++) {
#ifdef DEBUG
    solveCalls[3]++;
#endif
        size_t llen=dp[lidx]-dp[idx];
        if(llen>=minLen) {
#ifdef DEBUG
    solveCalls[6]++;
#endif
            int num=solve(lidx, llen, lidx-idx);
            if(num>0)
                ans=pl(ans, num);
            else if(num<0)
                break;
        }
    }

#ifdef DEBUG
    // cout<<"ans="<<ans<<endl;
#endif
    putMemo(idx,minLen,minOnes,ans);
    return ans;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(n);
    cin>>s;

    mem2=vvvi(n+1, vvi(M1, vi(M2, -1)));

    for(int i=0; i<n; i++) {
        if(s[i]=='1')
            dp.push_back(i);
    }
    dp.push_back(n);

    int ans=solve(0, 1, 1);

#ifdef DEBUG
    cout<<"mem.size()="<<mem.size()<<endl;
    cout<<" solveCalls[0]="<<solveCalls[0]<<endl;
    cout<<" solveCalls[1]="<<solveCalls[1]<<endl;
    cout<<" solveCalls[2]="<<solveCalls[2]<<endl;
    cout<<" solveCalls[3]="<<solveCalls[3]<<endl;
    cout<<" solveCalls[4]="<<solveCalls[4]<<endl;
    cout<<" solveCalls[5]="<<solveCalls[5]<<endl;
    cout<<" solveCalls[6]="<<solveCalls[6]<<endl;
#endif
    cout<<ans<<endl;

}

