/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

string s2;
string s3;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(l1); cini(l2); cini(l3);
    cins(s1);
    cin>>s2;
    cin>>s3;

    vvi cidx('z'+1);
    char cMin='z';
    int idx=0;
    for(char c : s2) {
        cidx[c].push_back(idx++);
        cMin=min(cMin, c);
    }

    /* case empty s2 is optimal */
    if(cMin>s3[0]) {
        cout<<s1+s3<<endl;
        return 0;
    }

/* The optimal subseq consists of all cMin letters, 
 * followed by all cMin+1 letters after the last cMin letter smaller than s3[0],
 * followed by all cMin+2...
 *
 * Two cases for cMin+i == s3[0]
 * If s3[1]<=s3[0] we should use no s2[x]==s3[0], else we should use all.
 */
    string ans;
    idx=0;
    char letter=cMin;
    for(; letter<s3[0]; letter++) {
        auto it=lower_bound(cidx[letter].begin(), cidx[letter].end(), idx);
        for(;it!=cidx[letter].end(); it++) {
            idx=*it;
            ans+=letter;
        }
    }

/* find postfixes of x begining with s3[0]
 * We need to construct the longest subseq starting at idx
 * which is lex smaller than s3.
 *
 * There are no smaller letters than s3[0] in s2 after idx idx.
 * We cannot add a bigger letter than s3[0] to x since if we do,
 * the prefix without that letter would be lex smaler.
 *
 * So there are two cases only:
 * 1. s3[k]<s3[0]
 * 2. s3[k]>s3[0]
 * where k is the lowest index with s3[k]!=s3[0]
 */
    assert(letter==s3[0]);

    bool followIsBigger=false;
    for(size_t i=1; i<s3.size(); i++) {
        if(s3[i]>letter) {
            followIsBigger=true;
            break;
        } else if(s3[i]<letter)
            break;
    }

    if(followIsBigger) {
        auto it=lower_bound(cidx[letter].begin(), cidx[letter].end(), idx);
        for(;it!=cidx[letter].end(); it++)
            ans+=letter;
    }

    cout<<s1+ans+s3<<endl;
}

