
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cini(m);
    cinll(k);

    vvi tree(n);
    vll sums(n);
    vi ans(n, -1);
    for(int i=0; i<m; i++) {
        cini(a);
        cini(b);
        a--;
        b--;
        tree[a].push_back(b);
        tree[b].push_back(a);
    }

    cini(q);
    for(int i=0; i<q; i++) {
        cini(p);
        p--;
        cinll(x);   
        for(int f: tree[p]) {
            if(sums[f]<k) {
                sums[f]+=x;
                if(sums[f]>=k)
                    ans[f]=i+1;
            }
        }
    }

    for(int a : ans)
        cout<<a<<" ";
    cout<<endl;
}

