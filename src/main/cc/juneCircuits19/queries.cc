/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

#define DEBUG

/* Arbitrary long integer
 */
struct BigInt {
    const int base = 1000*1000*1000;
    vector<int> a;

    void set(string s) {
        for (int i=(int)s.length(); i>0; i-=9) {
            if (i < 9)
                a.push_back (atoi (s.substr (0, i).c_str()));
            else
                a.push_back (atoi (s.substr (i-9, 9).c_str()));
        }
        while(a.size() > 1 && a.back() == 0)
            a.pop_back();
    }

    void add(BigInt& b) {
        int carry = 0;
        for (size_t i=0; i<max(a.size(), b.a.size()) || carry; ++i) {
            if(i == a.size())
                a.push_back (0);
            a[i] += carry + (i < b.a.size() ? b.a[i] : 0);
            carry = a[i] >= base;
            if(carry)
                a[i] -= base;
        }
    }

    void subtract(BigInt& b) {
        int carry = 0;
        for (size_t i=0; i<b.a.size() || carry; ++i) {
            a[i] -= carry + (i < b.a.size() ? b.a[i] : 0);
            carry = a[i] < 0;
            if (carry)  a[i] += base;
        }
        while (a.size() > 1 && a.back() == 0)
            a.pop_back();
    }

    void mul(BigInt& b) {
        vector<int> c(a.size()+b.a.size());
        for (size_t i=0; i<a.size(); ++i)
            for (int j=0, carry=0; j<(int)b.a.size() || carry; ++j) {
                long long cur = c[i+j] + a[i] * 1ll * (j < (int)b.a.size() ? b.a[j] : 0) + carry;
                c[i+j] = int (cur % base);
                carry = int (cur / base);
            }
        while (c.size() > 1 && c.back() == 0)
            c.pop_back();

        swap(a, c);
    }

    void dump() {
        printf ("%d", a.empty() ? 0 : a.back());
        for (int i=(int)a.size()-2; i>=0; --i)
            printf ("%09d", a[i]);
    }

    bool operator<(BigInt& b) {
        if(a.size()<b.a.size())
            return true;
        else if(b.a.size()<a.size())
            return false;
        
        for(int i=a.size()-1; i>=0; i--)
            if(a[i]<b.a[i])
                return true;
            else if(a[i]>b.a[i])
                return false;

        return false;
    }

    void diff(BigInt& b, BigInt& res) {
        if(*this<b) {
            res.a=b.a;
            res.subtract(*this);
        } else {
            res.a=a;
            res.subtract(b);
        }
    }

};

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, m;
    cin>>n>>m;

    vector<BigInt> a;
    for(int i=0; i<n; i++) {
        string s;
        cin>>s;
        BigInt bi;
        bi.set(s);
        a.push_back(bi);
    }


    vector<BigInt> left(n); // products created from left side
    int lIdx=0;
    vector<BigInt> right(n);// products created form right side
    int rIdx=n-1;

    /* true if the diff at idx is less than the one at idx-1, and less than or equals than the one at idx+1 */
    function<bool(int)> isMin=[&](int idx) {
#ifdef DEBUG
cout<<"isMin, idx="<<idx<<endl;
#endif
        while(lIdx<idx+2 && lIdx<n)  {
            if(lIdx==0)
                left[0].a=a[0].a;
            else {
                left[lIdx].a=left[lIdx-1].a;
                left[lIdx].mul(a[lIdx]);
            }
            lIdx++;
        }
#ifdef DEBUG
cout<<"isMin, after lIdx"<<endl;
#endif
        while(rIdx>idx && rIdx>0)  {
            if(rIdx==n-1)
                right[n-1].a=a[n-1].a;
            else {
                right[rIdx].a=right[rIdx+1].a;
                right[rIdx].mul(a[rIdx]);
            }
            rIdx--;
        }
#ifdef DEBUG
cout<<"isMin, after rIdx"<<endl;
#endif

        if(idx==0)  {
            BigInt diff0;
            left[0].diff(right[0], diff0);
            BigInt diff1;
            left[1].diff(right[1], diff1);
            return diff0<diff1;
        } else if(idx==n-1) {
            BigInt diff0;
            //left[n-1].diff(right[0], diff0);
            BigInt diff1;
            left[n-2].diff(right[n-1], diff1);
            return diff0<diff1;
        } else {
#ifdef DEBUG
cout<<"isMin, in else, idx="<<idx<<endl;
#endif
            BigInt biZero;
            BigInt diffL;
            left[idx-1].diff(right[idx], diffL);
            BigInt diffR;
            left[idx+1].diff(idx>=n-2?biZero:right[idx+2], diffR);
            BigInt diff0;
            left[idx].diff(right[idx+1], diff0);
            return diff0<diffL && diff0<diffR;
        }
        
    };

    int lastX=0;
    for(int i=0; i<m; i++) {
        int t;
        cin>>t;
#ifdef DEBUG
cout<<"t="<<t<<endl;
#endif
        if(t==1) {
            int pos;
            string val;
            cin>>pos>>val;
            pos--;
            BigInt bi;
            bi.set(val);
            a[pos].mul(bi);
            lIdx=min(lIdx, pos-1);
            rIdx=max(rIdx, n-pos);
        } else {
            for(int j=0; j<n; j++)
                if(isMin(j))
                    cout<<j+1<<endl;
        }
    }
}


