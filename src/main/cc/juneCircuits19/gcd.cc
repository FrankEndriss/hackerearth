/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define DEBUG

const int N=100001;
const int MOD=1000000007;

int mul(const int v1, const int v2) {
    return (int)((1LL * v1 * v2) % MOD);
}

int pow4(const int n) {
    return mul(mul(mul(n, n), n), n);
}

/* @return the number of permutations according to the rules; 4==1, 5==5, 6== */
int perms(int n) {
    ll ret=n;
    for(int i=1; i<=3; i++) {
        ret*=(n-i);
        // NOT ret%=MOD;
    }
    ret/=24;
    return (int)(ret%MOD);
}

/* divisors of n, returned in ret. 
 * Note that not all divisors are returned
 **/
void divs(int n, vector<int> &ret) {
    for(int i=2; i*i<=n; i++) {
        if(n%i==0) {
            ret.push_back(i);
            int j=n/i;
            if(j!=i)
                ret.push_back(j);
        }
    }
}

void solve(int n) {
    if(n<4) {
        cout<<"0"<<endl;
        return;
    }
    if(n==4) {
        cout<<"1"<<endl;
        return;
    }

    vector<int> dp(n/4+1);
    
    for(int i=n/4; i>=1; i--)
        dp[i]=perms(n/i);

    vector<int> d;
    for(int i=n/4; i>1; i--) {
        d.clear();
        divs(i, d);
        for(int div : d)  {
            dp[div]-=dp[i];
            while(dp[div]<0)
                dp[div]+=MOD;
        }
        dp[1]-=dp[i];
        while(dp[1]<0)
            dp[1]+=MOD;
    }

    int ans=0;
    for(int i=1; i<=n/4; i++) {
        if(dp[i]<0)
            cout<<"error, i="<<i<<" n="<<n<<endl;
        assert(dp[i]>=0);
        ans+=mul(dp[i], pow4(i));
        ans%=MOD;
    }
    cout<<ans<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);

/*
for(int i=25000; i>0; i/=2) {
    ll p=perms(i);
    //long double ld=p;
    cout<<p<<endl;
}
*/
/*
vector<int> d;
divs(100, d);
for(int i=0; i<d.size(); i++)
    cout<<d[i]<<" ";
return 0;
*/
    int t;
    cin>>t;
    while(t--) {
        int n;
        cin>>n;
/*
for(int i=4; i<10000;i++) {
cout<<"i="<<i<<" perms(i)="<<perms(i)<<endl;
*/
        solve(n);
    }
}

