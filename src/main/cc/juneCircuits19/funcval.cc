/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
        tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

#define DEBUG

ll P;

int mul(const int v1, const int v2) {
    return (int)((1LL * v1 * v2) % P);
}

int toPower(int a, ll p) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a);
        p >>= 1;
        a = mul(a, a);
    }
    return res;
}

int add(int a, int b) {
    int ret=a+b;
    while(ret<0)
        ret+=P;
    while(ret>P)
        ret-=P;
    return ret;
}

lll gaus(lll n) {
    return n*(n+1)/2;
}

ll f1(ll n) {
    if(n==1)
        return 1;
    return toPower(3, n);
}

ll f2(ll n) {
    if(n==1)
        return 1;


    ll ret= mul(2, f1(n-1)) - f2(n-1) + 2;
}

ll f(ll n) {
    if(n<=2)
        return 1;

    if(n&1)
        return f1(n/2);
    else
        return f2(n/2);
}


void solve() {
    ll L, R;
    cin>>L>>R;

// see http://homepages.gac.edu/~holte/courses/mcs256/documents/summation/top10sums.pdf
// geometric sum:
// sum=a* (1-r^^n) / (1-r)
// a=f(l)
// r=3
// n=(R-L)/2
// f(L) +3*f(L) +9*f(L) +27*...
//
    ll ans=0;
    for(ll i=L; i<=R; i++) {
        ans+=f(i);
        ans%=P;
    }

    cout<<ans<<endl;

}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    int t;
    cin>>t>>P;
#ifdef DEBUG
    for(int i=1; i<30; i++)
        cout<<"i="<<i<<" f(i)="<<f(i)<<endl;
#endif

    while(t--)
        solve();

}

