/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

const int MOD=1e9+7;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, h1, h2, a, b, c;
    cin>>n>>h1>>h2>>a>>b>>c;

/* - If one of abc is 0 only two values exist...
 * - We can swap all signs.
 * - In abc, there is one sign with only one value.
 */

/* Optimization of brute-force:
 * Can we make set of abc for several days, and so skip a lot of additions?
 * 2, 4, 8, 16,...
 */

/* How works the brute force solution?
 * -map<int, int> maps positions to number of ways that position can be reached.
 * -N days makes calculateable upper and lower limit of position which 
 *  can reach H2 until Nth day.
 * -loop over days, on each day calc all possible possitions for the next day.
 */
    map<ll, int> dp;   // dp[i]=number of ways position i is reached


    dp[h1]=1;
    vector<int> abc;
    abc.push_back(a);
    if(b!=a)
        abc.push_back(b);
    if(c!=b && c!=a)
        abc.push_back(c);

    vector<map<ll, ll>> dp2(1);    // dp2[i][j]=possible position changes for 2^i days, 
    for(int i=0; i<abc.size(); i++)
        dp2[0][abc[i]]=1;

    for(int i=1; 2<<i<n; i++) {
        dp2.push_back(map<ll, ll>());
        for(auto ent1 : dp2[i-1]) {
            for(auto ent2 : dp2[i-1]) {
                dp2[i][ent1.first+ent2.first]+=(ent1.second+ent2.second);
                dp2[i][ent1.first+ent2.first]%=MOD;
            }
        }
    }

/*
    ll abcMin=min(a, min(b, c));
    if(abcMin>0)
        abcMin=0;
    ll abcMax=max(a, max(b, c));
    if(abcMax<0)
        abcMax=0;
    
    ll hb=h2-n*abcMin;
    ll lb=h2-n*abcMax;
*/

    map<ll, int> dp3;
    dp3[h1]=1;

    for(int i=0; 1<<i <n; i++) {
        if(n&(1<<i))
            continue;

        map<ll, int> dp4;
        for(auto ent1 : dp3) {
            for(auto ent2 : dp2[i]) {
                dp4[ent1.first+ent2.first]+=ent1.second+ent2.second;
                dp4[ent1.first+ent2.first]%=MOD;
            }
        }
    }

    cout<<dp[h2]<<endl;
}

