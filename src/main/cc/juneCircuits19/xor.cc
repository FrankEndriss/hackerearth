/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

//#define DEBUG

const int MOD=1e9+7;

int hbit(int n) {
    int ret=0;
    while(n) {
        ret++;
        n>>=1;
    }
    return ret;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vector<vector<pair<int, int>>> tree(n);
    fori(n-1) {
        int x, y, z;
        cin>>x>>y>>z;
        x--;
        y--;
        tree[x].push_back({ y, z });
        tree[y].push_back({ x, z });
    }

    vector<int> paths(200001);
    int maxPath=-1;

    function<void(int, int, int)> dfs=[&](int v, int p, int x) {
        paths[x]++;
        maxPath=max(maxPath, x);
        for(auto c : tree[v])
            if(c.first!=p)
                dfs(c.first, v, x^c.second);
    };

    for(int i=0; i<n; i++)
        dfs(i, -1, 0);

#ifdef DEBUG
for(int i=0; i<=maxPath; i++) 
    cout<<"paths["<<i<<"]="<<paths[i]<<endl;
#endif
    
    ll ans=0;
    const int maxBit=hbit(maxPath);

    for(int i=0; i<=maxPath; i++) {
        ans+=paths[i]*paths[i];
        ans%=MOD;
        for(int b=0; b<maxBit; b++) {
            ans+=paths[i]*paths[i^(1<<b)];
            ans%=MOD;
        }
    }

    cout<<ans<<endl;

}

