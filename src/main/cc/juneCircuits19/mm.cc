/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
        tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

//#define DEBUG

struct dir {
    int n, m;
    dir(int pn, int pm): n(pn), m(pm) { }

    bool operator==(const dir& other) const {
        return n==other.n && m==other.m;
    }

    bool operator<(const dir& other) const {
        if(n==other.n)
            return m<other.m;
        return n<other.n;
    }
};

const dir S( 1,  0);
const dir W( 0, -1);
const dir N(-1,  0);
const dir E( 0,  1);

const dir right(const dir& d) {
    if(d==W)
        return N;
    else if(d==N)
        return E;
    else if(d==E)
        return S;
    else if(d==S)
        return W;

    assert(false);
}

const dir left(const dir& d) {
    if(d==W)
        return S;
    else if(d==S)
        return E;
    else if(d==E)
        return N;
    else if(d==N)
        return W;

    assert(false);
}

int n, m;
vector<vector<int>> A;
int mxN=0;
int mxM=0;
int mxA=-1;

struct plan {
    vector<vector<bool>> vis;
    vector<dir> steps;
    int curA;
    int curN;
    int curM;
    int cost;
    int c;

    plan() {
        reset();
    }

    reset() {
        vis.resize(n, vector<bool>(m, false));
        steps.clear();
        vis[0][0]=true;
        curA=A[0][0];
        curN=0;
        curM=0;
        cost=0;
        c=1;
    }

    bool canStep(const dir &d) const {
        return curN+d.n>=0 &&
               curN+d.n<n &&
               curM+d.m>=0 &&
               curM+d.m<m &&
               !vis[curN+d.n][curM+d.m];
    }

    void step(const dir &d) {
#ifdef DEBUG
        cout<<"pl.step, d.n="<<d.n<<" d.m="<<d.m<<" curN="<<curN<<" curM="<<curM<<endl;
#endif
        assert(canStep(d));
        c++;
        curN+=d.n;
        curM+=d.m;
        vis[curN][curM]=true;
        steps.push_back(d);

        if(A[curN][curM]>curA) {
            cost++;
            curA=A[curN][curM];
        }
    }

    void dump() {
        int ln=1;
        int lm=1;
        cout<<ln<<" "<<lm<<endl;;
        for(dir d : steps) {
            ln+=d.n;
            lm+=d.m;
            cout<<ln<<" "<<lm<<endl;;
        }
    }
};

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cin>>n>>m;
    A.resize(n, vector<int>(m));
    forn3(i, 0, n)
    forn3(j, 0, m) {
        cin>>A[i][j];
        if(A[i][j]>mxA) {
            mxN=(int)i;
            mxM=(int)j;
            mxA=A[i][j];
        }
    }
#ifdef DEBUG
    cout<<"mxN="<<mxN<<" mxM="<<mxM<<" mxA="<<mxA<<endl;
#endif

/* spiral clockwise */
    auto stTurnLeft=[](plan &pl, const dir& startDir) {
#ifdef DEBUG
        cout<<"running stTurnLeft\n";
#endif
        dir nextDir=startDir;
        while(pl.c<n*m) {
            if(pl.canStep(left(nextDir)))
                nextDir=left(nextDir);
            else if(pl.canStep(nextDir))
                ;
            else if(pl.canStep(left(left(nextDir))))
                nextDir=left(left(nextDir));
            else
                nextDir=right(nextDir);

            pl.step(nextDir);
        }
    };

/* spiral anti clockwise */
    auto stTurnRight=[](plan &pl, const dir& startDir) {
#ifdef DEBUG
        cout<<"running stTurnRight\n";
#endif
        dir nextDir=startDir;
        while(pl.c<n*m) {
            if(pl.canStep(right(nextDir)))
                nextDir=right(nextDir);
            else if(pl.canStep(nextDir))
                ;
            else if(pl.canStep(right(right(nextDir))))
                nextDir=right(right(nextDir));
            else
                nextDir=left(nextDir);

            pl.step(nextDir);
        }
    };

/* from top to bottom */
    auto stTopDown=[](plan &pl) {
#ifdef DEBUG
        cout<<"running stTopDown\n";
#endif
        while(pl.c<n*m) {
            while(pl.canStep(E))
                pl.step(E);
            while(pl.canStep(W))
                pl.step(W);

            if(pl.c==n*m)
                break;
            pl.step(S);
        }
    };

/* from bottom to top, using col 0 to initially step down. */
    auto stDownUp1=[](plan &pl) {
#ifdef DEBUG
        cout<<"running stDownUp\n";
#endif
        while(pl.canStep(S))
            pl.step(S);

        while(pl.c<n*m) {
            while(pl.canStep(E))
                pl.step(E);
            while(pl.canStep(W))
                pl.step(W);

            if(pl.c==n*m)
                break;
            pl.step(N);
        }
    };

/* from bottom to top, using col m-1 to initially step down. */
    auto stDownUp2=[](plan &pl) {
#ifdef DEBUG
        cout<<"running stDownUp\n";
#endif
        while(pl.canStep(E))
            pl.step(E);
        while(pl.canStep(S))
            pl.step(S);

        while(pl.c<n*m) {
            while(pl.canStep(E))
                pl.step(E);
            while(pl.canStep(W))
                pl.step(W);

            if(pl.c==n*m)
                break;
            pl.step(N);
        }
    };


/* from left to right */
    auto stLeftRight=[](plan &pl) {
#ifdef DEBUG
        cout<<"running stLeftRight\n";
#endif
        while(pl.c<n*m) {
            while(pl.canStep(S))
                pl.step(S);
            while(pl.canStep(N))
                pl.step(N);

            if(pl.c==n*m)
                break;
            pl.step(E);
        }
    };

    vector<plan> p;
    if(n>2 && m>2) {
        plan pl1;
        stTurnLeft(pl1, S);
        p.push_back(pl1);

        plan pl2;
        stTurnRight(pl2, E);
        p.push_back(pl2);
    }

    plan pl3;
    stTopDown(pl3);
    p.push_back(pl3);

    plan pl4;
    stLeftRight(pl4);
    p.push_back(pl4);

    plan pl5;
    stDownUp1(pl5);
    p.push_back(pl5);

    plan pl6;
    stDownUp2(pl6);
    p.push_back(pl6);

    sort(p.begin(), p.end(), [&p](const plan& p1, const plan& p2) {
        return p1.cost<p2.cost;
    });

#ifdef DEBUG
    for(plan& tp : p)
        cout<<"tp.cost="<<tp.cost<<endl;
#endif

    p[0].dump();
}

