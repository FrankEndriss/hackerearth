/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/** @return Number of ways to distribute i items to two buckets, with max upbound-1 item per bucket
 * and min 1 item per bucket.
 **/
int nCr2(int upbound, int i) {
    assert(i>=2);
    assert(upbound>(i+1)/2);

    int ret=0;
    int first=0;
    if(i<upbound) 
        ret=i-1;
    else {
        first=min(i-1, upbound-1);
        for(int j=first; j>=i-j; j--) {
            if(j==i-j)
                ret++;
            else
                ret+=2;
        }
    }
//cout<<"dbg nCr2 upbound="<<upbound<<" i="<<i<<" first="<<first<<" ret="<<ret<<endl;
    return ret;
}

void solve() {
    cini(n);
    if(n<4) {
        cout<<0<<endl;
        return;
    } else if(n==4) {
        cout<<3<<endl;
        return;
    }


    ll ans=0;
    for(int i=n-2; i>(n-i+1)/2; i--) {
        ans+=nCr2(i, n-i);
    }
    ans*=3;
    cout<<ans<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

