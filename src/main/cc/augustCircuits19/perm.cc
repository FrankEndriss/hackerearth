/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
//#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(n);
    vi a(n);
    vi bt(n);
    for(int i=0; i<n; i++) {
        cin>>a[i];
    }
    cini(q);

    int ans=0;
    for(int i=0; i<q; i++) {
        cini(t);
        if(t==1) {
            cini(x);
            cini(y);
            x^=ans;
            y^=ans;
            swap(a[x-1], a[y-1]);
        } else if(t==2) {
            cini(l);
            cini(r);
            cini(k);
            l^=ans;
            r^=ans;
            k^=ans;
            l--;
            r--;
            k--;

//cout<<"l="<<l<<" r="<<r<<" k="<<k<<endl;

            // brute force
            int bsize=l+max(0, n-1-r);
            if(bsize>k) {
                priority_queue<int> que;
                // vi b(bsize);
                //int idx=0;
                for(int j=0; j<l; j++) {
                    if(que.size()<=k)  
                        que.push(a[j]);
                    else if(que.top()>a[j]) {
                        que.pop();
                        que.push(a[j]);
                    }
                }
                for(int j=r+1; j<n; j++) {
                    if(que.size()<=k)  
                        que.push(a[j]);
                    else if(que.top()>a[j]) {
                        que.pop();
                        que.push(a[j]);
                    }
                }
                //nth_element(b.begin(), b.begin()+k, b.end());
                ans=que.top();
            } else {
                ans=n+k-bsize;
            }
            cout<<ans<<endl;
        } else
            assert(t==1 || t==2);
    }


}

