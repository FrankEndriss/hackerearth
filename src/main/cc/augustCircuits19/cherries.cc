/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

/*
#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")
*/

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

class DSU {
public:
    vector<int> p;
/* initializes each (0..size-1) to single set */
    DSU(int size) {
        p.resize(size);
        for(int i=0; i<size; i++)
            p[i]=i;
    }

/* finds set representative for member v */
    int find_set(int v) {
        if (v == p[v])
            return v;
        return p[v] = find_set(p[v]);
    }

/* makes v a single set, removes it from other set if contained there. */
    void make_set(int v) {
        p[v] = v;
    }

/* combines the sets of two members.
 *  * Use the bigger set as param a for optimized performance.  */
    void union_sets(int a, int b) {
        a = find_set(a);
        b = find_set(b);
        if (a != b)
            p[b] = a;
    }
};

void solve(int t) {
    cini(n);
    cini(m);
    // black = 1
    // red = 2
    DSU dsu(n+1);
    set<int> nodes;
    for(int i=0; i<m; i++) { // black, all other red
        cini(c);
        cini(d);
        dsu.union_sets(c, d);
        nodes.insert(c);
        nodes.insert(d);
    }

    map<int,int> comps;
    for(int i=1; i<=n; i++) {
        comps[dsu.find_set(i)]++;
    }
    int ans=0;
    for(auto ent : comps) {
        ans+=(ent.second-1);
    }

    ans+=(comps.size()-1)*2;
    cout<<"Case #"<<t<<": "<<ans<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    for(int i=1; i<=t; i++)
        solve(i);

}

