/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

struct people {
    int x;
    int y;
    int id;
    double d;
};

int main() {
    cini(n);
    cini(g);
    vector<people> p(n);
    for(int i=0; i<n; i++) {
        p[i].id=i+1;
        cin>>p[i].x>>p[i].y;
    }

/* Strategy:
 * We choose one pivot value, then put that one and the nearest g-1 
 * persons together in one house.
 * Repeat.
 *
 * For pivot we use an extreme value, say the one with min(x+y).
 */

    int h=0;
    vvi ans(n);

    while(p.size()) {   /* next house */
        size_t pivot=0;    /* find pivot as min(x+y) */
        for(size_t i=1; i<p.size(); i++)
            if(p[i].x+p[i].y < p[pivot].x+p[pivot].y)
                pivot=i;

        for(size_t i=0; i<p.size(); i++)  {
            ll dx=p[i].x-p[pivot].x;
            ll dy=p[i].y-p[pivot].y;
            p[i].d=sqrt(dx*dx+dy*dy);
        }

        function<bool(people, people)> distcmp=[&](people p1, people p2) {
            return p2.d<p1.d; // biggest dist first
        };
        sort(p.begin(), p.end(), distcmp);

        int currg=g;
        if(p.size()%g)
            currg++;

        if(p.size()<g*2)
            currg=p.size();
    
        for(int i=0; i<currg; i++) {
            ans[h].push_back(p.back().id);
            p.pop_back();
        }
        h++;
    }

    cout<<h<<endl;
    for(int i=0; i<h; i++) {
        cout<<ans[i].size()<<endl;
        for(int id : ans[i]) 
            cout<<id<<" ";
        cout<<endl;
    }

}

