/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

/*
#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")
*/

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int MOD=1e9+7;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a);
    }
    return res;
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    if (res < 0)
        res += mod;

    else if(res>=mod)
        res-=mod;

    return res;
}

int inv(const int x, const int mod=MOD) {
    return toPower(x, mod - 2);
}

int fraction(const int zaehler, const int nenner) {
    return mul(zaehler, inv(nenner));
}

struct person {
    string str;
    vvi sidx;    // index of positions of letters in str

    person() {
        sidx.resize('z'-'a'+1);    // index of positions of letters in str
    }
};


int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    vector<person> p(n);
    int ans2=1;
    for(int i=0; i<n; i++) {
        cin>>p[i].str;
        for(int j=0; j<(int)p[i].str.size(); j++)
            p[i].sidx[p[i].str[j]-'a'].push_back(j);

        ll g=p[i].str.size();
        g*=(p[i].str.size()+1);
        g/=2;
        g%=MOD;
        ans2=mul(ans2, (int)g);
    }
    
    sort(p.begin(), p.end(), [&](person& p1, person& p2) {
        return p1.str.size()<p2.str.size();
    });
    
    map<string, int> cache;

    /* Finds number of ways person[1]..person[n] can choose flag flag. */
    function<int(string&&)> check=[&](string&& flag) {
//cout<<"check(), flag="<<flag<<endl;
        auto it=cache.find(flag);
        if(it!=cache.end()) {
//cout<<"check from cache, ret="<<it->second<<endl;
            return it->second;
        }

        int lans=1;
        for(int i=1; i<n; i++) {    // persons
            int cnt=0;
            for(int idx : p[i].sidx[flag[0]-'a']) {    // idx of flag[0] in str 
                bool match=true;
                if(idx+flag.size()>p[i].str.size())
                    match=false;
                else {
                    for(int k=1; k<(int)flag.size() && idx+k<(int)p[i].str.size(); k++) {
                        if(p[i].str[idx+k]!=flag[k]) {
                            match=false;
                            break;
                        }
                    }
                }
                if(match)
                    cnt=pl(cnt, 1);
            }
//cout<<"matches for flag="<<flag<<" at person "<<i<<" =="<<cnt<<endl;
            lans=mul(lans, cnt);
            if(lans==0)
                break;
        }

        cache[flag]=lans;
//cout<<"check calced, ret="<<lans<<endl;
        return lans;
    };

    int ans=0;

    /* find all palindromes of person[0] and check() them. */
    const int len=(int)p[0].str.size();
    const string& s=p[0].str;
    for(int i=0; i<len; i++) {
        ans=pl(ans, check(s.substr(i, 1)));
        for(int j=1; i-j>=0 && i+j<len; j++) {
            if(s[i-j]==s[i+j])
                ans=pl(ans, check(s.substr(i-j, 1+2*j)));
        }
        if(i>0 && s[i]==s[i-1]) {
            ans=pl(ans, check(s.substr(i-1, 2)));
            for(int j=1; i-j-1>=0 && i+j<len; j++) {
                if(s[i-j-1]==s[i+j])
                    ans=pl(ans, check(s.substr(i-j-1, 2+2*j)));
            }
        }
    }

    //cout<<ans<<" "<<ans2<<endl;
    cout<<fraction(ans, ans2)<<endl;

}

