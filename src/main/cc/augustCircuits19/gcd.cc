/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;
typedef vector<vll> vvll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cini(m);
    vll a(n);
    vll f(n);
    vvll dp(n, vll(n, 1)); // dp[i][j] = gcd(a[i],...,a[i+j])
    for(int i=0; i<n; i++) {
        cin>>a[i];
        dp[i][0]=a[i];

        f[i]=a[i];
        if(i>0)
            f[i]+=f[i-1];
    }

    for(int i=0; i<n; i++) {
        for(int j=i+1; i+j<n; j++)  {
            dp[i][j]=__gcd(dp[i][j-1], a[i+j]);
            if(dp[i][j]==1)
                break;
        }
    }

    for(int k=0; k<m; k++) {
        cini(l);
        l--;
        cini(r);
        r--;

        // brute force
        ll ans=0;
        for(int i=l; i<=r; i++) {
            ans+=a[i];
            ll g=a[i];
            for(int j=i+1; j<=r; j++) {
                g=__gcd(g, a[j]);
                ans+=g;
                if(g==1) {
                    ans+=r-j;
                    break;
                }
            }
        }
        cout<<ans<<endl;
    }

}

