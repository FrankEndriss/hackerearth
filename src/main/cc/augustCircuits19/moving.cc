/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

template <typename T>
class fenwick {
public:
    vector<T> fenw;
    int n;

    fenwick(int _n) : n(_n) {
        fenw.resize(n);
    }

    void modify(int x, T v) {
        while (x < n) {
            fenw[x] += v;
            x |= (x + 1);
        }
    }

    T get(int x) {
        T v{};
        while (x >= 0) {
            v += fenw[x];
            x = (x & (x + 1)) - 1;
        }
        return v;
    }
};

template <typename T>
class fenwick2d {
public:
    vector<fenwick<T>> fenw2d;
    int n, m;

    fenwick2d(int x, int y) : n(x), m(y) {
        fenw2d.resize(n, fenwick<int>(m));
    }

    void modify(int x, int y, T v) {
        while (x < n) {
            fenw2d[x].modify(y, v);
            x |= (x + 1);
        }
    }

    T get(int x, int y) {
        x=min(x, n/2-1);
        y=min(y, m/2-1);
        T v{};
        while (x >= 0) {
            v += fenw2d[x].get(y);
            x = (x & (x + 1)) - 1;
        }
        return v;
    }
};

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(n);
    cini(m);
    cini(q);

    fenwick2d<int> f(2*n, 2*m);
    for(int i=0; i<n; i++) {
        cins(s);
        for(int j=0; j<m; j++) {
            if(s[j]=='1')
                f.modify(i, j, 1);
        }
    }

    int left=0;
    int leftX=0;
    int right=m-1;
    int rightX=0;
    int top=0;
    int topX=0;
    int bottom=n-1;
    int bottomX=0;
    for(int i=0; i<q; i++) {
        cini(t);
        if(t==2) {
            if(left>right || top>bottom)
                cout<<0<<endl;
            else {
                int q1=f.get(max(-1, top-1), max(-1, left-1));
                int q2=f.get(max(-1, top-1), min(m-1, right));
                int q3=f.get(min(n-1, bottom), max(-1, left-1));
                int q4=f.get(min(n-1, bottom), min(m-1, right));
                int ans=q4-q3-q2+q1;
/*cout<<"dbg q1="<<q1<<" q2="<<q2<<" q3="<<q3<<" q4="<<q4<<endl;
 */
                cout<<ans<<endl;
            }
        } else {

            cini(dx);
            cini(dy);
            if(dx>0) {
                int z=min(leftX, dx);
                leftX-=z;
                left+=dx-z;
                rightX+=dx;
            } else {
                int z=min(rightX, -dx);
                rightX-=z;
                right+=dx+z;
                leftX-=dx;
            }

            if(dy>0) {
                int z=min(topX, dy);
                topX-=z;
                top+=dy-z;
                bottomX+=dy;
            } else {
                int z=min(bottomX, -dy);
                bottomX-=z;
                bottom+=dy+z;
                topX-=dy;
            }
        }
/* cout<<"dbg left="<<left<<" right="<<right<<" top="<<top<<" bottom="<<bottom<<endl;
 */
    }
}
