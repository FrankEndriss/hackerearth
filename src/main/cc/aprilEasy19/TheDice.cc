/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;

int main() {
string s; cin>>s;
	if(s[s.length()-1]=='6') {
		cout<<"-1"<<endl;
		return 0;
	}
	int ans=0;
	for(uint i=0; i<s.length(); i++)
		if(s[i]!='6')
			ans++;

	cout<<ans<<endl;
}

