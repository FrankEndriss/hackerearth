
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;

int a[200001];

int main() {
int n, k, P, Q; cin>>n>>k>>P>>Q;
	for(int i=0; i<n; i++)
		cin>>a[i];

	int mul=P/Q;
	sort(a, a+n);
	// find longest range of a[i] where a[i]*mul<=a[i+x]
	int rstart=0;
	int maxlen=0;
	for(int i=1; i<n; i++) {
		if(a[rstart]*mul>=a[i]) {
			if(i-rstart+1>maxlen) {
				maxlen=i-rstart+1;
			}
		} else {
			while(a[rstart]*mul<a[i])
				rstart++;
		}
	}
	int ans=maxlen+min(n-maxlen, k);
	cout<<ans<<endl;
}

