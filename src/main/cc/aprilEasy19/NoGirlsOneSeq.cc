/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

ll gcd2(ll a, ll b) {
	while (b > 0) {
		ll temp = b;
		b = a % b;
		a = temp;
	}
	return a;
}

int a[1000001];

ll gcd(uint n, ll adj) {
	ll result = a[0]+adj;
	for(uint i=1; i<n; i++) {
		result = gcd2(result, a[i]+adj);
		if(result==1)
			return result;
	}
	return result;
}

int main() {
uint n; cin>>n;
ll l, r; cin>>l>>r;

	for(uint i=0; i<n; i++)
		cin>>a[i];

	ll ans=0;
	for(ll i=l; i<r; i++)
		ans=max(ans, gcd(n, i));

	cout<<ans<<endl;
}

