/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;

int main() {
string s; cin>>s;
uint n=s.length();
vector<int> consSta;
vector<int> consLen;

	int curLen=0;
	char last=' ';
	for(uint i=0; i<n; i++) {
		if(last==s[i]) {
			curLen++;
			if(curLen==2) {
//cout<<"log push i="<<i-1<<endl;
				consSta.push_back(i-1);
				consLen.push_back(2);
			} else
				consLen[consLen.size()-1]++;
		} else {
			curLen=1;
		}
		last=s[i];
	}

	if(consSta.size()==0) {
		cout<<n<<endl;
		return 0;
	}
//cout<<"log consSta, consLen: ";
//for(uint i=0; i<consSta.size(); i++)
//	cout<<"("<<consSta[i]<<","<<consLen[i]<<") ";
//cout<<endl;

	int ans=consSta[0]+1;
	for(uint i=0; i<consSta.size()-1; i++) {
//cout<<"log i="<<i<<" consSta[i]="<<consSta[i]<<endl;
//cout<<"log i="<<i<<" consSta[i+1]="<<consSta[i+1]<<endl;
		ans=max(ans, 1+consSta[i+1]-(consSta[i]+(consLen[i]-1)));
//cout<<"log ans="<<ans<<endl;
	}
	int x=n-(consSta[consSta.size()-1]+consLen[consLen.size()-1])+1;
//cout<<"log x="<<x<<" n="<<n<<endl;
	ans=max(ans, x);
//cout<<"log after loop ans="<<ans<<endl;
	
	cout<<ans<<endl;

}

