/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;

int r[500001];
int b[500001];
int ir[500001];
int ib[500001];

bool byr(int i1, int i2) {
	return r[i1]<r[i2];
}

bool byb(int i1, int i2) {
	return b[i1]<b[i2];
}

int main() {
int n; cin>>n;
	for(int i=0; i<n; i++) {
		ir[i]=i;
		ib[i]=i;
		cin>>r[i]>>b[i];
	}
	sort(ir, ir+n, byr);
	sort(ib, ib+n, byb);
		
	int cur=ir[0];
	int ans1=1;
	for(int i=1; i<n; i++)
		//if(max(r[id[cur]], b[id[cur]]) < min(r[id[i]], b[id[i]])) {
		if(r[cur]<b[id[i]] && b[cur]<r[id[i]]) {
			cur=id[i];
			ans1++;
		}

	cout<<ans<<endl;
}

