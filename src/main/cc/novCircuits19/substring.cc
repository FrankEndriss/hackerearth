/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cini(k);
    cini(m);
    cins(s);

    queue<int> q; // current substring of size k
    int cnt=0;  // count of ones on q

    int ans=0;
    vi ansv;
    for(int i=0; i<n; i++) {
        if(k==(int)q.size()) {
            int v=q.front();
            ansv.push_back(v);
            if(v==1)
                cnt--;
            q.pop();
        }
        int v;
        if(s[i]=='1')
            v=1;
        else
            v=0;
        if(cnt==m && v==1) {
            ans++;
            v=0;
        }
        if(v==1)
            cnt++;
        q.push(v);
    }

    cout<<ans<<endl;
    for(int i : ansv)
        cout<<i;

    while(q.size()) {
        int i=q.front();
        q.pop();
        cout<<i;
    }
    cout<<endl;

}

