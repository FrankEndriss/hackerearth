/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int MOD=1e9+7;

/* Obs:
 * For y=x*n ret== y/x  , id the bigger devided by the smaller
 * IF x,y are coprime then gcd==1, then ret==x*y
 * x,1, ret==x
 */
ll delta(ll x, ll y) {
    ll g=__gcd(x,y);
    return (x*y)/(g*g);
}

ll gaus(ll n) {
    return n*(n+1)/2;
}

ll solve(int n) {

    ll ans=n;    // pairs (1,1),(2,2),...(n,n)
    //ans*=2;             // pairs (1,2),(1,3),...(1,n)
    //ans--;

    priority_queue<pii, vector<pii>, greater<pii>> q;   // done pairs
    vvi d(n+1);
    vi didx(n+1);

    for(int i=1; i<n; i++) {
        d[i-1].resize(0);
        for(int j=i+1; j<=n; j++) {
            //pii p2={i,j};
            if(d[i].size()>didx[i] && d[i][didx[i]]==j) {
                didx[i]++;
                continue;
            }

#ifdef DEBUG
if(delta(i,j)!=i*j) {
cout<<"delta("<<i<<","<<j<<")="<<delta(i,j)<<endl;
}
if(__gcd(i,j)!=1) {
cout<<"gcd!=1, i="<<i<<" j="<<j<<endl;
assert(false);
}
#endif
            ll a=2LL*i*j;
            ans+=a;
            for(int i2=i+i, j2=j+j; j2<=n; i2+=i, j2+=j) {
                ans+=a;
#ifdef DEBUG
cout<<"push i2="<<i2<<" "<<j2<<endl;
#endif
                d[i2].push_back(j2);
            }
            ans%=MOD;
        }
    }
    return ans;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(n);

#ifdef DEBUG
        ll ans2=0;
        for(int i=1; i<=n; i++)
            for(int j=i; j<=n; j++) {
                ll d=delta(i,j);
                ans2+=d;
                if(i!=j)
                    ans2+=d;
                ans2%=MOD;
            }
        cout<<"ans2="<<ans2<<endl;
#endif

    ll ans=solve(n);
/*
    for(int i=1; i<=n; i++) {
//cout<<"i="<<i<<endl;
        ll g1=gaus(n/i);
//cout<<"g1="<<g1<<endl;
        ans+=g1*2-1;
        ans%=MOD;

        for(int j=i; j<n; j+=i) {
            for(int k=1; k<i && j+k<=n; k++)  {
                ans+=2*delta(i,j+k);
                ans%=MOD;
            }
        }
    }
*/

    cout<<ans<<endl;
}

