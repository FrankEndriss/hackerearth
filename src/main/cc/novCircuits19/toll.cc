/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

struct road {
    int v;  // from city
    int w;  // cost
};

void solve() {
    cini(n);
    cini(k);
    // tree[i][j]==index of jth road incoming to city i
    // r[tree[i][j]]=that incoming road
    vvi tree(n);
    vector<road> r((n-1)*2);

    for(int i=0; i<n-1; i++) {
        cin>>r[i*2].v>>r[i*2+1].v>>r[i*2].w>>r[i*2+1].w;
        r[i*2].v--;
        r[i*2+1].v--;
        tree[r[i*2].v].push_back(i*2+1);
        tree[r[i*2+1].v].push_back(i*2);
    }

   // ru[i]=road usage vectors for city i
   // ru[i][p]=road usage vector for city i with parent city p
   // ru[i][p][j]=usage of jth incoming road of city i with parent p; 
   // ru[i][p][j].first=road index of this jth road
   // r[ru[i][p][j].first].w=costs one usage
   // ru[i][p][j].second=times that road is used
    vector<map<int,vector<pii>>> ru(n);

/* fills ru[node][p] with values. */
    function<void(int,int)> dfsdp=[&](int node, int p) {
        if(ru[node][p].size()>0)
            return;

        
        int cnt=0;
        for(int chl : tree[node]) {
            if(p==r[chl].v)
                continue;

            dfsdp(r[chl].v, node);

            for(pii ent : ru[r[chl].v][node]) {
                cnt+=ent.second;
                ru[node][p].push_back(ent);
            }
            cnt++;
            ru[node][p].push_back({ r[chl].w, cnt });
        }
    }; 

    ll ans=1e18;
    for(int node=0; node<n; node++) {
        dfsdp(node, -1);

        vll t;
        for(pii ent : ru[node][-1]) {
           t.push_back(1LL * ent.first * ent.second);
        }

        assert(t.size()==n-1);
        sort(t.begin(), t.end());
        ll lans=0;
        for(int i=0; i<n-1-k; i++) 
            lans+=t[i];
        ans=min(ans, lans);
    }

// TODO WA and MLE
    cout<<ans<<endl;

}

/* Key obs:
 * Its a tree, so every road is used. Exactly the number times of
 * num left nodes or num right nodes.
 * Every road is finally used in one direction only.
 */
int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}


