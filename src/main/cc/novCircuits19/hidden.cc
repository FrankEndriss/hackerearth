/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* draws a grid to visualize the solution for input==2. */
void drawGrid(int n) {
    string str;
    while(n>(int)str.size())
        str+=".";

    vs m;
    for(int i=0; i<n; i++)
        m.push_back(str);

    for(int i=0; i<n; i++)
        for(int j=0; j<n; j++) {
            if(i==0 && j==0)
                continue;
            int x=i;
            int y=j;
            if(m[x][y]=='X')
                continue;

            while(true) {
                x+=i;
                y+=j;
                if(x<n && y<n)
                    m[x][y]='X';
                else
                    break;
            }
        }

    for(int i=0; i<n; i++) 
        cout<<m[i]<<endl;
}
    
void solve1() {
    cini(n);
//cout<<"searching for n="<<n<<endl;

/*
*/
/*
    int row=85000;
    while(true) {
cout<<"row="<<row<<endl;
        for(int col=2; col<=row; col++)  {
            // check square starting at [row][col] 
            bool gridOk=true;
            for(int j=0; j<n && gridOk; j++)  {
                    if(__gcd(row+j,col)==1)
                        gridOk=false;
                    if(__gcd(row+j,col+n-1)==1)
                        gridOk=false;

                    if(__gcd(row,col+j)==1)
                        gridOk=false;
                    if(__gcd(row+n-1,col+j)==1)
                        gridOk=false;
            }
            if(gridOk) {
                cout<<row<<" "<<col<<endl;
                return;
            }
        }
        row++;
    }

*/
    ll up=2;
    vi pr={ 2,3,5,7,11,13,17,19,23,29,31};
    int mul=1;
    while(true) {
cout<<"up="<<up<<endl;
        for(ll i=2; i<up; i++)  {
            // check grid starting at [i][up]
            int ok=true;
            for(int j=n-1; j>=0; j--)  {
                if(__gcd(up, i+j)==1) {
                    i+=j;
                    ok=false;
                    break;
                }
                for(int k=1; k<n; k++) {
                    if(__gcd(up+k, i+j)==1) {
                        ok=false;
                        break;
                    }
                }
                if(!ok)
                    break;
                
            }
            if(ok) {
                cout<<i<<" "<<up<<endl;
                return;
            }
        }
        up*=pr[mul];
        mul++;
    }
    
}

void solve2() {
    cini(n);
    if(n==1)
        cout<<"0, 2"<<endl;
    else if(n==2) 
        cout<<"14 20"<<endl;
    else if(n==3) 
        cout<<"1274 1308"<<endl;
    else if(n==4)
        cout<<"134043 184785885"<<endl;
    else if(n==5)
        cout<<"129963314 2546641254872348"<<endl;
    else
        cout<<"-123123123123123123123123"<<n<<" 123123123123123123123123"<<n<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);

    //drawGrid(60);
    
    //solve1();
    solve2();

}

