/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cini(k);
    vvi tree(n);
    for(int i=0; i<n-1; i++) {
        cini(u);
        cini(v);
        u--;
        v--;
        tree[u].push_back(v);
        tree[v].push_back(u);
    }

    vb terms(n);
    for(int i=0; i<k; i++) {
        cini(x);
        terms[x-1]=true;
    }

/* We need to find/count all cities on (loop free) pathes from term[i] to term[j] 
 * for any i!=j 
 *
 * We can do two dfs from all simple cities. From such city there must be two
 * independent paths to a terminal city.
 * Independent is no edge or city in boty paths, except the term city.
 * Since it is simple tree that should work.
 */

    function<bool(int,int)> dfs=[&](int node, int parent) {
        if(terms[node])
            return true;

        for(int ch : tree[node]) {
            if(ch!=parent)
                if(dfs(ch, node))
                    return true;
        }

        return false;
    };

    int ans=k;
    for(int i=0; i<n; i++) {
        if(terms[i])
            continue;
        if(tree[i].size()<2)
            continue;

        int cnt=0;
        for(int ch : tree[i]) {
            if(dfs(ch,i))
                cnt++;
            if(cnt==2)
                break;
        }
        if(cnt==2)
            ans++;
    }

    cout<<ans<<endl;

}

