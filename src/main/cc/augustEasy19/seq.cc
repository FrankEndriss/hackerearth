/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(i) ll i; cin>>i;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define endl "\n"

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cinll(a);
    cinll(b);
    cini(q);

    ll ma=1;
    vll x(2);
    x[0]=a;
    x[1]=b;
    for(int i=0; i<q; i++) {
        cinll(num);
        if(a!=0 || b!=0)  {
        while(num>x[x.size()-1] && num>x[x.size()-2]) {
            x.push_back(x[x.size()-1]+x[x.size()-2]);
        }
        }
        if(x[0]>x[1])
            swap(x[0], x[1]);
        auto it=lower_bound(x.begin(), x.end(), num);
        if(*it==num)
            cout<<"YES"<<endl;
        else
            cout<<"NO"<<endl;
    }
    
}

