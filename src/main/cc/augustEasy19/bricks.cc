/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define endl "\n"

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int MOD=1e9+7;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a);
        p >>= 1;
        a = mul(a, a);
    }
    return res;
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    if (res < 0)
        res += mod;

    else if(res>=mod)
        res-=mod;

    return res;
}


int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
/*
    const int N=1001;
    vll fak(N);
    fak[1]=1;
    for(int i=2; i<N; i++) 
        fak[i]=mul(fak[i-1], i);
*/

    cini(n);
    cini(k);
    vi x(k);
    for(int i=0; i<k; i++) 
        cin>>x[i];
    sort(x.begin(), x.end());

    int colors=0;
    for(int i=1; i<k; i++) {
        int gap=x[i]-x[i-1]-1;
        if(gap>1)
            colors+=2;
        else if(gap>0)
            colors++;
    }
    if(x[0]>1)
        colors++;

    if(x[x.size()-1]<n) 
        colors++;


    int ans=1;
    /* 1 to first */
    if(x[0]>1) {
        for(int i=1; i<x[0]; i++) {
            ans=mul(ans, colors);
        }
        colors--;
    }
    /* last to n */
    if(x[x.size()-1]<n)  {
        for(int i=x[x.size()-1]; i<=n; i++) {
            ans=mul(ans, colors);
        }
        colors--;
    }

    for(int i=1; i<k; i++) {
        int gap=x[i]-x[i-1]-1;
        if(gap==1) {
            ans=mul(ans, colors);
            colors--;
        } else {
            for(int j=gap; j>0; j--) {
                ans=mul(ans, colors);
                if(j<=2)
                    colors--;
            }
        }
    }

    cout<<ans<<endl;
}

