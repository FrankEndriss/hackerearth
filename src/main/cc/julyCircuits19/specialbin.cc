/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

const int MOD=1e9+7;
//#define DEBUG

int mul(ll a, ll b, int mod=MOD) {
    return (a*b)%MOD;
}

int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

int inv(const int x, const int mod=MOD) {
    return toPower(x, mod - 2, mod);
}

int pl(int a, int b) {
    return (a+b)%MOD;
}

const int N=1e6+2;
const int N2=N/2;

vi fac(N/2);

void init() {
    fac[1]=1;
    for(int i=2; i<N/2; i++) {
        fac[i]=mul(fac[i-1], i);
    }
}

/** see https://cp-algorithms.com/combinatorics/binomial-coefficients.html */
int nCr(int n, int k) {
    if(k==0 || k==n)
        return 1;
    if(k==1 || k==n-1)
        return n;
    if(k>n/2)
        return nCr(n, n-k);

    return mul(mul(fac[n], inv(fac[k])), inv(fac[n - k]));
}

ll rec(int n) {
    if(n<1)
        return 0;
    else if(n==1)
        return 1;
    else if(n==2 || n==3)
            return 2;
    else {
        ll even=n/2;
        ll odd=even;
        if(n&1)
            odd++;

        int lvl=2;
        while(lvl*2-1<n)
            lvl*=2;

        // lvl-1 nodes in fullfilled levels
        // lvl positions in last level
#ifdef DEBUG
        cout<<"n="<<n<<" lvl="<<lvl<<endl;
#endif

        /* So we got permutations, ie change one node with another.
         * This is even! * odd!
         * Then we got positions, which arise from the fact that we can place
         * the nodes of the last level on different positions.
         * This is nCr(2^level/2, numNodesOfThatOddnessOnLastLevel);
         */
        int ans=mul(fac[even], fac[odd]);    // permutations
#ifdef DEBUG
        cout<<"permutations="<<ans<<endl;
#endif

        /* case 1, odd root */
        int oddOnLastLevel=odd-lvl/2;
        int evenOnLstLevel=even-(lvl/2-1);
#ifdef DEBUG
        cout<<"odd root: oddOnLastLevel="<<oddOnLastLevel<<" evenOnLstLevel="<<evenOnLstLevel<<endl;
#endif
        /* We need to multiply the possibilities for the odd/even nodes to be placed in the last level.
         * Since that are lvl/2 positions, it is: ans*= fac[lvl/2] / fac[lvl/2-oddOnLastLevel]
         * Since there is no division availalbe, we do that by a loop.
         */
        int oddpositions=nCr(lvl/2, oddOnLastLevel);
        oddpositions=mul(oddpositions, nCr(lvl/2, evenOnLstLevel));

        /* case 2, even root */
        int evenpositions=0;
        if(n!=lvl*2-1) {    // case impossible
            oddOnLastLevel=odd- (lvl/2-1);
            evenOnLstLevel=even-lvl/2;
#ifdef DEBUG
            cout<<"even root: oddOnLastLevel="<<oddOnLastLevel<<" evenOnLstLevel="<<evenOnLstLevel<<endl;
#endif
            evenpositions=nCr(lvl/2, oddOnLastLevel);
            evenpositions=mul(evenpositions, nCr(lvl/2, evenOnLstLevel));
        }
        ans=mul(ans, pl(oddpositions, evenpositions));
        return ans;
    }
}

void solve() {
    int n;
    cin>>n;
    cout<<rec(n)<<endl;;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    int t;
    cin>>t;
    init();
    while(t--)
        solve();
}

