/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

void solve() {
    string s;
    cin>>s;
    vi dpl(s.size());
    vi dpr(s.size());

    if(s[0]=='(')
        dpl[0]=1;
    if(s[s.size()-1]==')')
        dpr[s.size()-1]=1;
    
    
    for(int i=1; i<s.size(); i++) {
        dpl[i]=dpl[i-1];
        if(s[i]=='(')
            dpl[i]++;

        dpr[s.size()-1-i]=dpr[s.size()-i];
        if(s[s.size()-1-i]==')')
            dpr[s.size()-1-i]++;
    }

    int ans=0;
    for(int i=0; i<s.size(); i++)
        ans=max(ans, min(dpl[i]*2, dpr[i]*2));

    cout<<ans<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int t;
    cin>>t;
    while(t--)
        solve();
}

