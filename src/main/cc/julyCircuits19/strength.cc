/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

const int MOD=1e9+7;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, m;
    cin>>n>>m;
    vi a(n);
    fori(n) {
        cin>>a[i];
    }
    sort(a.begin(), a.end(), greater<int>());

#define pl(x, y) x=((x+y)%MOD)

    vvi dp(n, vi(512)); // dp[i][j]= number of ways using first i+1 players numbers to reach j

    for(int i=0; i<=a[0]; i++) 
        dp[0][i]=1;

    for(int i=1; i<n; i++) {
        for(int j=0; j<512; j++) {
            for(int k=0; k<=a[i]; k++) {
                int x=j^k;
                pl(dp[i][x], dp[i-1][j]);
            }
        }
    }

    for(int i=0; i<=m; i++)
        cout<<dp[n-1][i]<<" ";
    cout<<endl;
}

