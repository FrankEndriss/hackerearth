/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

#define DEBUG

#define MOD 1000000007

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a);
        p >>= 1;
        a = mul(a, a);
    }
    return res;
}

int inv(const int x, const int mod=MOD) {
    return toPower(x, mod - 2);
}

int fraction(const int zaehler, const int nenner) {
    return mul(zaehler, inv(nenner));
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    if (res < 0)
        res += mod;

    else if(res>=mod)
        res-=mod;

    return res;
}

void solve() {
    ll n, k, s;
    cin>>n>>k>>s;
    int log=1;
    while((1<<log)<=n || (1<<log)<=s)
        log++;
#ifdef DEBUG
cout<<"max(n, s)="<<max(n, s)<<endl;
#endif

    vi dp(log);    // dp[i]=number of times bit i is set in the numbers in interval 0..n
    //vi dp2(log);    // dp2[i]=number of times bit i is set if above numbers are xored with s, this is k(1)
    for(int i=0; i<log; i++) {
/* brute force, optimize later */
        int mask=1<<i;
        for(int j=1; j<=n; j++) { 
            if(j&mask)
                dp[i]++;
        }
#ifdef DEBUG
cout<<"i="<<i<<" dp[i]="<<dp[i]<<endl;
#endif
    }

/**
    vi dp2(log);
    for(int i=0; i<log; i++)
        if(s&(1<<i))
            dp2[i]++;
*/
    
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int t;
    cin>>t;
    while(t--)
        solve();
}

