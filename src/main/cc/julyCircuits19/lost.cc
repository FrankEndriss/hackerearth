/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

void solve() {
    int n, m, s;
    cin>>n>>m>>s;
    s--;
    vvi gra(n);
    vi v(n);
    fori(n) {
        cin>>v[i];
    }
    fori(m) {
        int a, b;
        cin>>a>>b;
        a--; b--;
        gra[a].push_back(b);
    }

    /* find path with min v[i] from s to all vertices, output min v[i] in order. */
    vi dp(n, 1e9);
    queue<int> q;
    q.push(s);
    dp[s]=v[s];
    while(q.size()) {
        int node=q.front();
        q.pop();
        for(auto c : gra[node]) {
            int ma=max(dp[node], v[c]);
            if(dp[c]>ma) {
                dp[c]=ma;
                q.push(c);
            }
        }
    }
    for(int i=0; i<n; i++)
        cout<<dp[i]<<" ";
    cout<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int t;
    cin>>t;
    while(t--)
        solve();
}

