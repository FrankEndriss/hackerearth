/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<pii> vpii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

struct point {
    int x, y;
};

struct canopy {
    point p1, p2;
    int c;
};

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, d;
    cin>>n>>d;
    point start, end;
    cin>> start.x>> start.y>> end.x>> end.y;
    int c0;
    cin>>c0;    // tiredness factor

    vector<canopy> cano(n);
    fori(n) {
        int x1, x2, y1, y2;
        cin>>x1>>y1>>x2>>y2;
        cin>>cano[i].c;
        cano[i].p1.x=min(x1, x2); // p1 allways left/down
        cano[i].p1.y=min(y1, y2);
        cano[i].p2.x=max(x1, x2); // p2 allways right/up
        cano[i].p2.y=max(y1, y2);
    }

/* try all canos, use one, the best! ;)
 *
 * We need some utility functions.
 * double dist(p1, p2);
 * output(p1, p2, canoidx)
 * inside(point, canoidx)
 *
 * nearestTo(point, canoidx)
**/

/* Check if point is inside(true) of cano[idx]. */
    function<bool(point, int)> inside=[&cano](point p, int idx) {
        return cano[idx].p1.x<=p.x && cano[idx].p2.x>=p.x && cano[idx].p1.y<=p.y && cano[idx].p2.y>=p.y;
    };

/* Distance of two points */
    function<double(point, point)> dist=[](point p1, point p2) {
        ll dx=abs(p1.x-p2.x);
        ll dy=abs(p1.y-p2.y);
        return sqrt(dx*dx+dy*dy);
    };

/* Simple output formatting. */
    function<void(point, point, int)> pout=[](point p1, point p2, int canoidx) { 
        cout<<p1.x<<" "<<p1.y<<" "<<p2.x<<" "<<p2.y<<" "<<canoidx<<endl;
    };

    cout<<3<<endl;
    pout(start, cano[0].p1, 0);
    pout(cano[0].p1, cano[0].p2, 1);
    pout(cano[0].p2, end, 0);

}

