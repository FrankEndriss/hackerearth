/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

/*
#pragma GCC target ("avx2")
#pragma GCC optimization ("unroll-loops")
#pragma GCC optimization ("O3")
*/

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<pii> vpii;
typedef vector<vpii> vvpii;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;


class DSU {
public:
    vector<int> p;
/* initializes each (0..size-1) to single set */
    DSU(int size) {
        p.resize(size);
        for(int i=0; i<size; i++)
            p[i]=i;
    }

/* finds set representative for member v */
    int find_set(int v) {
        if (v == p[v])
            return v;
        return p[v] = find_set(p[v]);
    }

/* makes v a single set, removes it from other set if contained there. */
    void make_set(int v) {
        p[v] = v;
    }

/* Combines the sets of two members.
 * Use the bigger set as param a for optimized performance.  */
    void union_sets(int a, int b) {
        a = find_set(a);
        b = find_set(b);
        if (a != b)
            p[b] = a;
    }
};

#define lsb(i) ((i)&(-i))

class FenwickSparse {
    map<int, ll> tree;
    int sizeofTree;
    ll neutral;
    function<ll(ll, ll)> cumul;

public:
    void init(int psizeofTree, ll pneutral, function<ll(ll, ll)> pcumul) {
        this->sizeofTree=psizeofTree;
        this->neutral=pneutral;
        this->cumul=pcumul;
    }


    /** Updates the value at pIdx cumulative, i.e. "adds" value to the value stored at pIdx. */
    void updateCumul(int pIdx, ll value) {
        assert(pIdx>=1);
        assert(pIdx<=sizeofTree);
#ifdef DEBUG
cout<<"FS.updateCumul, idx="<<pIdx<<endl;
#endif

        while (pIdx < sizeofTree) {
#ifdef DEBUG
//cout<<"FS.updateCumul loop, x="<<pIdx<<" sizeofTree="<<sizeofTree<<endl;
if(pIdx==0)
    exit(1);
#endif
            auto it=tree.find(pIdx);
            if(it==tree.end())
                tree[pIdx] = value;
            else
                it->second=cumul(it->second, value);

            pIdx += lsb(pIdx);
        }
    }

    /** @return the cumul of range 0:pIdx inclusive. */
    ll readCumul(int pIdx) {
        ll sum = neutral;
        while (pIdx > 0) {
            auto it=tree.find(pIdx);
            ll val= it==tree.end()?neutral:it->second;
            sum = cumul(sum, val);
            pIdx -= lsb(pIdx);
        }
        return sum;
    }
};

class Fenwick2D {
    //unordered_map<int, FenwickSparse*> tree;
    vector<FenwickSparse> tree;
    vector<bool> vis;

    int sizeX;
    int sizeY;
    ll neutral;
    function<ll(ll, ll)> cumul;

    FenwickSparse& treeget(int x) {
        if(!vis[x]) {
            vis[x]=true;
            tree[x].init(sizeY, neutral, cumul);
        }
        return tree[x];
    }

public:
    void init(int psizeX, int psizeY, ll pneutral, function<ll(ll, ll)> pcumul) {
        this->sizeX=psizeX;
        this->sizeY=psizeY;
        this->neutral=pneutral;
        this->cumul=pcumul;
        this->tree=vector<FenwickSparse>(psizeX+2);
        this->vis=vector<bool>(psizeX+2, false);
    }

    /** Updates the value at pIx/pIy cumulative, i.e. "adds" value to the value stored at pIx/pIy. */
    void updateCumul(int pIx, int pIy, ll value) {
        pIx++; pIy++;
#ifdef DEBUG
cout<<"F2D.updateCumul, x="<<pIx<<" y="<<pIy<<endl;
#endif
        assert(pIx>=1);
        assert(pIx<=sizeX);
        while (pIx < sizeX) {
#ifdef DEBUG
//cout<<"F2D.updateCumul loop, x="<<pIx<<endl;
#endif
            treeget(pIx).updateCumul(pIy, value);
            pIx += lsb(pIx);
        }
    }

    /** @return the cumul of range (0,0):(pIx, pIy) inclusive. */
    ll readCumul(int x, int y) {
        x++; y++;
        ll sum = neutral;
        while (x > 0) {
            sum = cumul(sum, treeget(x).readCumul(y));
            x -= lsb(x);
        }
        return sum;
    }
};

//#define DEBUG

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    int n, q;
    cin>>n>>q;

    vector<tuple<int, int, int>> borders(n-1);
    vvi tree(n);

    fori(n-1) {
        int u, v, w;
        cin>>u>>v>>w;
        u--;
        v--;
        tree[u].push_back(v);
        tree[v].push_back(u);
        borders[i]=make_tuple( w, u, v );
    }
    sort(borders.begin(), borders.end());

    vll dpMinR(n);  // dpMinR[i]=list of min(R) of all countries
    ll sumR=0;
    fori(n) {
        cin>>dpMinR[i];
        sumR+=dpMinR[i];
    }

    vi parent(n);  // parent[i]=parent of city i
    function<void(int, int)> dfsParent=[&](int node, int p) {
        parent[node]=p;
        for(auto c : tree[node]) {
            if(c!=p)
                dfsParent(c, node);
        }
    };
    dfsParent(0, -1);

    const int MAXD=1e8+3;
    const int MAXR=1e5+3;
    Fenwick2D fenwick;
    fenwick.init(MAXR, MAXD, 0LL, [](ll i1, ll i2)  { return i1+i2; });

    DSU dsu(n);

    for(int i=0; i<n-1; i++) {
        const int bsValue=get<0>(borders[i]);
        int u=get<1>(borders[i]);
        int v=get<2>(borders[i]);
        u=dsu.find_set(u);
        v=dsu.find_set(v);
        if(parent[u]==v)
            swap(u, v);
        dsu.union_sets(u, v);

        if(dpMinR[v]>dpMinR[u]) {   // remove v
#ifdef DEBUG
cout<<"updCumul, bs="<<bsValue<<" R="<<dpMinR[v]<<endl;
#endif
            fenwick.updateCumul((int)dpMinR[v], (int)bsValue, dpMinR[v]);
        } else { // remove u
#ifdef DEBUG
cout<<"updCumul, bs="<<bsValue<<" R="<<dpMinR[u]<<endl;
#endif
            fenwick.updateCumul((int)dpMinR[u], (int)bsValue, dpMinR[u]);
        }
    }
    // remove the last one, should be the lowest rate
    const int u=dsu.find_set(0);
#ifdef DEBUG
cout<<"updCumul, bs="<<MAXD-2<<" R="<<dpMinR[u]<<endl;
#endif
    fenwick.updateCumul((int)dpMinR[u], MAXD-2, dpMinR[u]);

/* Process queries.
 **/

#ifdef DEBUG
cout<<" dumping fenw XxY"<<endl;
for(int i : { 0, 1, 2, 3, 4, 5, 6, MAXR-1 }) {
    for(int j : { 0, 1, 2, 3, 4, 5, 6, MAXD-1 }) {
        ll a=fenwick.readCumul(i, j);
        cout<<a<<" ";
    }
    cout<<endl;
}
#endif

    ll D, T, X;
    ll ans=0;
    fori(q) {
        cin>>D>>T>>X;
#ifdef DEBUG
cout<<"bev xor D="<<D<<" T="<<T<<" X="<<X<<endl;
#endif
        D^=ans;
        T^=ans;
        X^=ans;

        ll richRate=(X+T-1)/T;
#ifdef DEBUG
        cout<<"richRate="<<richRate<<" D="<<D<<endl;
#endif

        
        ll rem1=fenwick.readCumul((int)richRate-1, MAXD-1);
        ll rem2=fenwick.readCumul(MAXR, (int)D);
        ll rem3=fenwick.readCumul((int)richRate-1, (int)D);
        ans=sumR-rem1-rem2+rem3;
        ans*=T;
#ifdef DEBUG
        cout<<"rem1="<<rem1<<" x="<<MAXD<<" y="<<richRate-1<<endl;
        cout<<"rem2="<<rem2<<" x="<<D<<" y="<<MAXR<<endl;
        cout<<"rem3="<<rem3<<" x="<<D<<" y="<<richRate-1<<endl;
        cout<<"sumR="<<sumR<<endl;
        cout<<"ans="<<ans<<endl;
#else
        cout<<ans<<endl;
#endif
    }

}

