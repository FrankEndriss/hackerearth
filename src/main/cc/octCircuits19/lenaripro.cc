/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int INF=1e9;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cini(q);
    cinai(a,n);
    vector<pii> ari(n); // ari[i]={j,d} -> i is the j-th element of arithpro with diff d
    ari[0]={1,INF};
    ari[1]={2, a[1]-a[0]};

    for(int i=2; i<n; i++) {
        if(ari[i-1].second==INF)
            ari[i]={2, a[i]-a[i-1]};
        else if(a[i]-a[i-1]==ari[i-1].second)
            ari[i]={ari[i-1].first+1, ari[i-1].second};
        else
            ari[i]={2, a[i]-a[i-1]};
    }
#ifdef DEBUG
for(int i=0; i<ari.size(); i++) {
    cout<<"ari["<<i<<"]="<<ari[i].first<<","<<ari[i].second<<endl;
}
#endif
    
    map<int, vector<pii>> dmap; // dmap[d]={ {l1,r1}, {l2,r2},...} -> for diff d (backwards sorted)

    int prev=n+1;
    for(int i=n-1; i>0; i--) {
        if(ari[i].second!=prev && ari[i].second!=INF)
            dmap[ari[i].second].push_back({i-ari[i].first+1, i});
        prev=ari[i].second;
    }
#ifdef DEBUG
cout<<"dump dmap"<<endl;
for(auto ent : dmap) {
cout<<"ent.first="<<ent.first<<endl;
for(auto entp : ent.second)
cout<<"entp.first="<<entp.first<<" entp.second="<<entp.second<<endl;
}
#endif

    for(int i=0; i<q; i++) {
        cini(l);
        l--;
        cini(r);
        r--;
        cini(d);
/* We need to find the biggest ari[i].first where ari[i].second==d and i
 * mostly inside between l and r.
 * Ok, try brute force.
 */
        auto it=dmap.find(d);
        if(it==dmap.end())  {
            cout<<1<<endl;
            continue;
        }
        int ans=1;
        for(pii p : it->second) {
            int len=min(p.second, r)-max(p.first, l)+1;
            ans=max(ans, len);
        }
        cout<<ans<<endl;
    }

}

