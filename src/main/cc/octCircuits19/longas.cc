/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const vi two= { 1,2,4,8,16,32,64,128,256,512,1024,2048,4096 };
const int TWOSZ=two.size();
bool precheck(int n, int k) {
    if(n==1) {
        if(k==1) {
            cout<<"Yes"<<endl;
            cout<<1<<endl;
        } else {
            cout<<"No"<<endl;
        }
        return true;
    }

    if(k<2 || k>n) {
        cout<<"No"<<endl;
        return true;
    }

    if(k==n-1) {
        cout<<"Yes"<<endl;
        for(int i=n; i>2; i--)
            cout<<i<<" ";
        cout<<"1 2"<<endl;
        return true;
    }

    if(k>=n/2+1) {
        cout<<"Yes"<<endl;
        for(int i=0; i<k-1; i++)
            cout<<i+1<<" ";
        for(int i=n; i>=k; i--)
            cout<<i<<" ";
        cout<<endl;
        return true;
    }

    return false;
}

void solve() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(n);
    cini(k);

    if(precheck(n,k))
        return;


    int extra=0;
    if(k>2) {
        extra=k-2;
        n-=extra;
        k=2;
    }

    bool front=false;
    if(extra&1) {
        extra++;
        n--;
        front=true;
    }

    int maxNum=n;

    set<int> nums[2];
    for(int i=1; i<=n; i++)
        nums[!(i&1)].insert(i);

    vi ans;
    for(int start=0; start<=1; start++) {
        //cout<<start+1<<" ";
        ans.push_back(start+1);
        nums[start].erase(start+1);
        int prev=start+1;

        int dir=1;
        while(nums[start].size()) {
            if(nums[start].size()==1) {
                //cout<<*nums[start].begin()<<" ";
                ans.push_back(*nums[start].begin());
                nums[start].clear();
                break;
            }

            for(int i=TWOSZ-1; i>=0; i--) {
                int x=prev+(two[i]*dir);
                if(nums[start].count(x) && x!=maxNum) {
                    ans.push_back(x);
                    //cout<<x<<" ";
                    nums[start].erase(x);
                    prev=x;
                }
            }
            dir=-dir;
        }
    }

    if(front)
        for(int i=n+extra; i>n; i--)
            cout<<i<<" ";
    for(int i : ans)
        cout<<i<<" ";
    if(!front)
        for(int i=n+1; i<=n+extra; i++)
            cout<<i<<" ";

    cout<<endl;
}

void solve2a(vi &vals) {
    if(vals.size()==1) {
        cout<<vals[0]<<" ";
    } else {
        vi v[2];
        for(size_t i=0; i<vals.size(); i++)
            v[i&1].push_back(vals[i]);
        solve2a(v[0]);
        solve2a(v[1]);
    }
}

void solve2() {
    cini(n);
    cini(k);

    if(precheck(n,k))
        return;

    int extra=0;
    bool revextra=false;
    if(k>2) {
        extra=k-2;
        if((extra&1)^(n&1)) {   // parity extra != parity n
            revextra=true;
        }
        extra++;
    }

    cout<<"Yes"<<endl;
    if(revextra)
        for(int i=n; i>n-extra; i--)
            cout<<i<<" ";

    vi v;
    for(int i=1; i<=n-extra; i++)
        v.push_back(i);
    solve2a(v);

    if(!revextra)
        for(int i=n-extra+1; i<=n; i++)
            cout<<i<<" ";
}

int main() {
    solve2();
}
