/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<ll, ll> pllll;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

vector<vector<vector<vll>>> memo(33, vector<vector<vll>>(2, vector<vll>(33, vll(33, -1))));
int currLen=-1;

ll c2(ll cnt, bool freeUsed, int open, int close, vi &data, function<void()> callback) {
//cout<<"c2 freeUsed="<<freeUsed<<" open="<<open<<" close="<<close<<endl;

    if(open==0 && close==0 && freeUsed) {
        if(cnt==1)
            callback();
        return cnt-1;
    }

    ll memCnt=memo[currLen][freeUsed][open][close];
//cout<<"memCnt="<<memCnt<<" cnt="<<cnt<<endl;
    if(memCnt>0 && memCnt<cnt)
        return cnt-memCnt;

    ll lcnt=cnt;

    if(open>0) {
        data.push_back(0);
        lcnt=c2(lcnt, freeUsed, open-1, close, data, callback);
        data.pop_back();
    } else if(!freeUsed) {
        data.push_back(0);
        lcnt=c2(lcnt, true, open, close, data, callback);
        data.pop_back();
    }

    if(lcnt<=0) {
        //memo[currLen][freeUsed][open][close]=cnt-lcnt;
        return lcnt;
    }

    if(close>open) {
        data.push_back(1);
        lcnt=c2(lcnt, freeUsed, open, close-1, data, callback);
        data.pop_back();
    } else if(!freeUsed) {
        data.push_back(1);
        lcnt=c2(lcnt, true, open, close, data, callback);
        data.pop_back();
    }

    
    if(lcnt>0)
        memo[currLen][freeUsed][open][close]=cnt-lcnt;
    return lcnt;
}

void solve() {
    cini(n);
    cinll(k);

    assert(n&1);    // n is odd

    vi data;
    bool impossible=true;

    function<void()> callback=[&]() {
        impossible=false;
        char c[2]= { '(', ')' };
        for(int i : data)
            cout<<c[i];
        cout<<endl;
    };

    currLen=n/2;
    c2(k, false, n/2, n/2, data, callback);
    if(impossible)
        cout<<"Impossible"<<endl;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

