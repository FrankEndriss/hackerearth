/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

typedef long long ll;

const int primeAproxFactor=20;
const int MAXN=6000000;
vector<int> pr(MAXN);
vector<bool> cmp(MAXN*primeAproxFactor, false);

void init() {

    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!cmp[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                cmp[j] = true;
                j += i;
            }
        }
        i++;
    }
}

bool isPrime(int num) {
    return !cmp[num];
    //return *lower_bound(pr.begin(), pr.end(), num)==num;
}

void solve() {
    cini(n);
    int ans=0;
    for(int i=0; i<n; i++) {
        cini(a);
        if(isPrime(a)) {
            ans++;
        }
    }
    cout<<ans<<endl;

/* Function Z(i)==true if
 * fak(i-1) % i == i-1
 * We need to find all numbers up to 1e8
 * for which Z(i)==true.
 * Or at least find Z(i) for some 1e5 random numbers.
 * SQRT decomposition?
 *
 * However, calculating Z(i) for all numbers starting at
 * 1,2,3...
 * Just try a brute force...
 * Turns out all prime numbers result Z(i)==true.
 * So, we need to count the primenumbers in a fast.
 * 
 */
    
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    init();
/*
auto it=lower_bound(pr.begin(), pr.end(), 100000000);
auto idx=distance(pr.begin(), it);
cout<<"idx="<<idx<<endl;
cout<<pr[idx-1]<<endl;
*/
    cini(t);
    while(t--)
        solve();
}

