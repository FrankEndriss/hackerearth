/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

struct tline {
    int f;  // from city
    int e;  // to city
    int t;  // time travel (*A)
    int c;  // cost travel (*B)
    int h;  // hourly schedule
};

int A, B;

struct trainlistent {
    int train;
    int prev;
};

vector<trainlistent> trainlist;

struct path {
    ll c;  // sum c
    ll h;  // sum h + t, arrival time

/* current city. */
    int city;

/* Used trains, for trackback. */
    int trainlink;

    ll price() const {
        return c*B+h*A;
    }
};

const int INF=1e9;
vector<int> cityCnt; // counter for cities

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cini(m);
    cin>>A;
    cin>>B;

    cityCnt=vector<int>(n); // counter for cities

    vector<tline> trains(m);
    vvi graph(n);
    for(int i=0; i<m; i++) {
        cin>>trains[i].f>>trains[i].e>>trains[i].t>>trains[i].c>>trains[i].h;
        trains[i].f--;
        trains[i].e--;
        if(trains[i].f==trains[i].e || trains[i].e==0)
            continue;

        graph[trains[i].f].push_back(i);
    }

    struct pathByH {
        bool operator() (const path &p1, const path &p2) {
            if(A==0)
                return p1.c<p2.c;
            else
                return p1.h<p2.h;
        }
    };

    struct pathByPrice {
        bool operator() (const path &p1, const path &p2) {
            return p2.price()<p1.price();
        }
    };

    struct pathByCitycnt {
        bool operator() (const path &p1, const path &p2) {
            return cityCnt[p2.city]<cityCnt[p1.city];
        }
    };

    vector<path> ans; // pathes to city n-1

    priority_queue<path,vector<path>,pathByPrice> q;

    path start;
    start.c=0;
    start.h=0;
    //start.cities.insert(0);
    start.trainlink=-1;
    start.city=0;
    q.push(start);
    //const int MAXLOOP=1000000;
    //const int MAXCITY=1000;
    const int MAXLOOP=INF;
    const int MAXCITY=INF;
    cityCnt[0]+=MAXCITY;
    int cnt=0;
    ll ansPrice=INF;
    while(q.size()) {
        if(cnt++>MAXLOOP && ans.size()>0)
            break;
        path p=q.top();
        q.pop();
        for(int train : graph[p.city]) {
            if(cityCnt[trains[train].e]>=MAXCITY)
                continue;

            trainlist.push_back({train,p.trainlink});

            path next;
            next.trainlink=(int)trainlist.size()-1;
            next.c=p.c+trains[train].c;
            next.city=trains[train].e;
            ll offs=p.h%trains[train].h;
            if(offs>0)
                offs=trains[train].h-offs;
            next.h=p.h+offs+trains[train].t;
            
            if(next.city==n-1) {
                ans.push_back(next);
                ansPrice=min(ansPrice, next.price());
            } else {
                if(next.price()<ansPrice) {
                    cityCnt[next.city]++;
                    q.push(next);
                }
            }
        }
    }
    assert(ans.size()>0);

    auto it=min_element(ans.begin(), ans.end(), [&](path &p1, path &p2) {
        return p1.price()<p2.price();
    });

    stack<int> st;
    int tlink=it->trainlink;
    while(tlink>=0) {
        st.push(trainlist[tlink].train+1);
        tlink=trainlist[tlink].prev;
    }
    while(st.size()) {
        int t=st.top();
        st.pop();
        cout<<t<<" ";
    }
    cout<<endl;
}

