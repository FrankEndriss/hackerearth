/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<ll, ll> pllll;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int MOD=1e9+7;
const int MOD2=MOD+MOD;


vector<pii> p_space;
stack<int> p_space_free;

void p_set(int idx, int first, int second) {
    p_space[idx]={ first, second };
}

pii p_get(int idx) {
    return p_space[idx];
}

void p_free(int idx) {
    p_space_free.push(idx);
}

int p_alloc(int first, int second) {
    if(p_space_free.size()) {
        int ret=p_space_free.top();
        p_space_free.pop();
        p_set(ret, first, second);
        return ret;
    } else {
        p_space.push_back({first,second});
        return ((int)p_space.size())-1;
    }
}

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    if (res < 0)
        res += mod;

    else if(res>=mod)
        res-=mod;

    return res;
}

const int NMASK=(1<<6);
const int N=100001;
const int SQSIZE=256;
const int SQBLOCKS=(N/SQSIZE)+2;

/* raw data arrays, the numbers and calculated bitsets determined by the number.  */
vi a;
vi bits;

/* sqAns1[i][j] = product of a[i*SQSIZE] ... a[j*SQSIZE-1] 
 * ie the answer to a query if l==i*SQSIZE and r==j*SQSIZE */
vvi sqAns1(SQBLOCKS, vi(SQBLOCKS));

/* sqBlockprod[i][j] = product of numers with bits[i] with all numbers with fitting bits[k] */
vvi sqBlockprod(NMASK, vi(SQBLOCKS));

/* sqBlocksums[i][j] = sum of numbers with bits[i] in block j */
vvi sqBlocksums(NMASK, vi(SQBLOCKS));

/** assumes a and bits contains the data. Then builds sqBlocksums, sqBlockprod and sqAns1. */
void initSq() {
    for(int i=0; i<N; i++) {
        register i1=bits[i];
        register i2=i/SQSIZE;
        sqBlocksums[i1][i2]=pl(sqBlocksums[i1][i2], a[i], MOD2);
    }

    for(int i=0; i<SQBLOCKS; i++) {
        for(int j=0; j<NMASK; j++) {
            sqBlockprod[j][i]=sqBlocksums[j][i];
            for(int k=0; k<NMASK; k++) {
                if(!(k&j)) {
                    sqBlockprod[j][i]=mul(sqBlockprod[j][i], sqBlocksums[k][i], MOD2);
                }
            }
        }
    }

    // TODO sqAns1
}

vb npre(NMASK);
vector<set<pair<pii, int>>> nums(NMASK);
// nums[x] = set of {index,a}{prefixsum,prefixsumPref} with bitmask of a = x

const ll INFL=1e18;
const int INF=1e9;

void unindex(int idx) {
    auto it=nums[bits[idx]].lower_bound({{idx,a[idx]},0});
    p_free(it->second);
    nums[bits[idx]].erase(it);
    npre[bits[idx]]=false;
}

void index(int idx) {
    int val=a[idx];
    stack<int> st;
    while(val) {
        st.push(val%10!=0);
        val/=10;
    }
    bits[idx]=0;
    while(st.size()) {
        bits[idx]<<=1;
        bits[idx]|=st.top();
        st.pop();
    }

//cout<<"bits[idx]="<<bits[idx]<<" idx="<<idx<<" a[idx]="<<a[idx]<<endl;
    nums[bits[idx]].insert({{idx,a[idx]},p_alloc(0,0)});
    npre[bits[idx]]=false;
}

void index_numsPre(int b) {
    if(!npre[b]) {
        int prev=0;
        for(auto p : nums[b]) {
            int sum=prev+p.first.second;
            p_set(p.second, sum, prev);
            prev=sum;
        }
        npre[b]=true;
    }
}

int solve(int l, int r) {
    int ans=0;
    for(int i=l; i<=r-1; i++) {
        for(int j=0; j<(NMASK); j++) {
            if(!(bits[i]&j)) {
                index_numsPre(j);
                auto itL=nums[j].lower_bound({{i,-1},0});
                auto itR=nums[j].lower_bound({{r+1,-1},0});

#ifdef DEBUG
int tmpL=p_get(itL->second).second;
int tmpR=p_get(itR->second).first;
cout<<"(itL->second).second="<<tmpL<<" (itR->second).first="<<tmpR<<endl;
#endif
                int sum=p_get(itR->second).first - p_get(itL->second).second;
                ans=pl(ans, mul(a[i], sum));
            }
        }
    }
    return ans;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);

    /* init nums sets */
    for(int i=0; i<(NMASK); i++) {
        nums[i].insert({{INF,0},p_alloc(0,0)});
    }

    cini(n);
    a.resize(n);
    bits.resize(n);

    for(int i=0; i<n; i++) {
        cin>>a[i];
        index(i);
    }

    cini(q);

    for(int i=0; i<q; i++) {
        cini(t);
        if(t==0) {
            cini(idx);
            idx--;
            unindex(idx);
            cin>>a[idx];
            index(idx);
        } else {
            cini(l);
            cini(r);
            l--;
            r--;
            int ans=solve(l,r);
            cout<<ans<<endl;
        }
    }
}


