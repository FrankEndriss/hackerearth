/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

#define SOL_1
//#define DEBUG

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    int n, m;
    cin>>n>>m;

    typedef struct _edge {
        ll weight;
        int edgenum;
    } edge;

    vector<unordered_map<int, edge>> tree(n+1);

    for(int i=0; i<m; i++) {
        int u, v;
        ll w;
        cin>>u>>v>>w;
        /* add only unset or cheaper edges */
        auto ent=tree[u].find(v);
        if(ent==tree[u].end() || ent->second.weight>w) {
            tree[u][v]={ w, i+1 };
            tree[v][u]={ w, i+1 };
        }
    }

    /** dfs all paths from 1 to n is much to slow...
     * Dijkstra cheapest paths from 1 to n.
     * Stop this at some point of time, and output optimal solution so far.
     */

    typedef struct _spath {
        ll weight;          /* costs so far */
        int curNode;        /* last vertex in path */
        vector<int> edges;
        set<int> snodes;

        void push_back(int node, edge e) {
            weight+=e.weight;
            edges.push_back(e.edgenum);
            snodes.insert(node);
            curNode=node;
        }

        bool contains(int node) {
            return snodes.count(node);
        }

        int back() {
            return curNode;
        }

        bool operator<(const struct _spath &other) const {
            if(edges.size()!=other.edges.size())
                return edges.size()<other.edges.size();

            if(weight!=other.weight)
                return weight<other.weight;

            return edges<other.edges;
        }
            
    } spath;

    /* paths found so far. */
    vector<spath> paths;

    priority_queue<spath> q; 
    spath start;
    start.snodes.insert(1);
    start.curNode=1;
    q.push(start);

    bool fini=false;
    while(q.size()>0 && !fini) {
        auto next=q.top();
        q.pop();
#ifdef DEBUG
cout<<"next, curNode="<<next.curNode<<" snodes.size()="<<next.snodes.size()<<endl;
#endif

        for(auto c : tree[next.back()]) {   // pair<node, edge>
            if(!next.contains(c.first)) {
                if(c.first==n) { /* found end of path */
                    spath res=next;
                    res.push_back(c.first, c.second);
                    paths.push_back(res);
#ifdef SOL_1
                    fini=true;
                    break;  // DEBUG for SOL_1
#endif
                } else { /* dig deeper */
                    spath newNext=next;
                    newNext.push_back(c.first, c.second);
                    q.push(newNext);
                }
            }
        }
    }

#ifdef DEBUG
for(auto p : paths) {
    cout<<"p.cost="<<p.weight;
    for(int e : p.edges) 
        cout<<" "<<e;
    cout<<endl;
}
#endif

#ifdef SOL_1
/* First, simple aproach. Simply output the single path
 * with minimum M.
 */
    cout<<1<<endl;
    cout<<paths[0].edges.size();
    for(uint i=0; i<paths[0].edges.size(); i++) 
        cout<<" "<<paths[0].edges[i];
    cout<<endl;
#endif
}

