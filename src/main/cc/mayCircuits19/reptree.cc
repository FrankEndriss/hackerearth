/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

//#define DEBUG

vector<pair<pair<int, int>, int>> edges;
vector<vector<int>> G(1000001);
void storeG(int x, int y, int w) {
    if(x==y) // no need to store self loops
        return;

    edges.push_back({ { x, y }, w });
    for(int i=1; i*i<=w; i++) {
        if(w%i==0) {
            G[i].push_back((int)edges.size()-1);
            int ii=w/i;
            if(ii!=i)
                G[ii].push_back((int)edges.size()-1);
        }
    }
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
int n, q;
    cin>>n>>q;
    vector<vector<int>> tree(n+1);
    vector<int> parent(n+1);
    fori(n-1) {
        int p;
        cin>>p;
        parent[i+2]=p;
        tree[p].push_back(i+2);
        tree[i+2].push_back(p);
    }

    const int ROOT=1;

    vector<int> depth(n+1);
    function<void(int, int, int)> dfs=[&](int v, int par, int dep) {
        depth[v]=dep;
        for(auto c : tree[v])
            if(c!=par)
                dfs(c, v, dep+1);
    };
    dfs(ROOT, -1, 0);

/* checks if the path from from to to contains the edge
 * between v and parent[v]
 */
    function<bool(int, int, int)> pathContains=[&](int from, int to, int v) {
#ifdef DEBUG
cout<<"pathContains, from="<<from<<" dep[from]="<<depth[from]<<" to="<<to<<" dep[to]="<<depth[to]<<" v="<<v<<endl;
#endif
#define chk(x) if(x==v) return true;
        while(depth[from]>depth[to]) {
            chk(from);
            from=parent[from];
            assert(from>0);
        }
        while(depth[to]>depth[from]) {
            chk(to);
            to=parent[to];
            assert(to>0);
        }
        while(from!=to) {
            chk(from);
            chk(to);
            from=parent[from];
            to=parent[to];
            assert(from>0);
            assert(to>0);
        }
        return false;
#undef chk
    };

    ll lastAns=0;
    fori(q) {
        int t;
        cin>>t;
        if(t==1) {
            int x, y, w;
            cin>>x>>y>>w;
            x=(x+lastAns-1)%n+1;
            y=(y+lastAns-1)%n+1;
#ifdef DEBUG
cout<<"q1, x="<<x<<" y="<<y<<" w="<<w<<endl;
#endif
            storeG(x, y, w);
        } else if(t==2) {
            int v, prime;
            cin>>v>>prime;
            v=(v+lastAns-1)%(n-1)+2;
#ifdef DEBUG
cout<<"q2, v="<<v<<" p="<<prime<<endl;
#endif

            ll ans=0;
            for(auto eidx : G[prime]) {
#ifdef DEBUG
cout<<"q2, from="<<edges[eidx].first.first<<" to="<<edges[eidx].first.second<<endl;
#endif
                if(pathContains(edges[eidx].first.first, 
                                edges[eidx].first.second, 
                                v))
                    ans+=edges[eidx].second;
            }
            cout<<ans<<endl;
            lastAns=ans;
#ifdef DEBUG
cout<<"finished query"<<endl;
#endif
        } else
            assert(false);
    }

}

