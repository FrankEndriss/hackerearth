/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vector<int> s(n), sid(n), e(n);
    fori(n) {
        cin>>s[i];
        sid[i]=i;
    }
    fori(n) {
        cin>>e[i];
    }
    sort(sid.begin(), sid.end(), [&](int i1, int i2) {
        return s[i1]<s[i2];
    });

    vector<int> p(n, -1);
    for(int i=1; i<n; i++) {
        if(e[sid[i]]<=e[sid[i-1]]) { // its a child
            p[sid[i]]=sid[i-1];
        } else { // its a sibling of one of the parents
            int sibnode=p[sid[i-1]];
            while(e[sibnode]<e[sid[i]])
                sibnode=p[sibnode];
            p[sid[i]]=sibnode;
        }
    }
    fori(n)
        cout<<p[i]+1<<" ";
    cout<<endl;
}

