/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    ll n, k;
    cin>>n>>k;

    /* if k is even we can apply -2, 0, 2,... up to +-k
     * if k is odd we cann apply -1, 1, 3,... up to +-k
     */
    vector<ll> a(n);
    vector<int> id(n);
    ll sum=0;
    fori(n) {
        cin>>a[i];
        sum+=a[i];
        id[i]=(int)i;
    }

    sort(id.begin(), id.end(), [&](int i1, int i2) {
        return a[i1]<a[i2];
    });

    vector<ll> d(n);   // d[i]=sum of changes needed if capital is i to reach 0
    d[0]=sum- a[id[0]]*n;
//cout<<"d[0]="<<d[0]<<endl;
    for(int i=1; i<n; i++) {
        ll ld=a[id[i]]-a[id[i-1]];
        d[i]= d[i-1] + ld*(i+1) - ld*(n-i+1);
//cout<<"d["<<i<<"]="<<d[i]<<endl;
    }

    ll mindiff=1000000000LL*1000000001;
    vector<ll> minidx;
    fori(n) {
        ll ld;
        if(d[i]>=k)
            ld=d[i]-k;
        else  {
            ld=abs(d[i]-k)%2;
        }
        if(ld<mindiff) {
            minidx.clear();
            mindiff=ld;
            minidx.push_back(i);
        } else if(ld==mindiff) {
            minidx.push_back(i);
        }
//cout<<"ld="<<ld<<" at idx="<<i<<endl;
    }

    for(uint i=0; i<minidx.size(); i++)
        minidx[i]=id[minidx[i]];
    auto ans=*min_element(minidx.begin(), minidx.end());
    cout<<ans+1<<" "<<mindiff<<endl;
}

