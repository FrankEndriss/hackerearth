/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

/* see https://cp-algorithms.com/geometry/segment-to-line.html */
struct pt {
    double x, y;
};

struct line {
    double a, b, c;
};

const double EPS = 1e-9;

line calcLine(pt p1, pt p2) {
    double a=p1.y-p2.y;
    double b=p2.x-p1.x;
    return { a, b, -a*p1.x-b*p1.y };
}


/* see https://cp-algorithms.com/geometry/circle-line-intersection.html */
int lineCircleIntersecions(double r, line n, vector<pt> &ret) {
    double a=n.a;
    double b=n.b;
    double c=n.c;
    double x0 = -a*c/(a*a+b*b), y0 = -b*c/(a*a+b*b);

    if (c*c > r*r*(a*a+b*b)+EPS)
        return 0;
    else if (abs (c*c - r*r*(a*a+b*b)) < EPS) {
        ret.push_back({ x0, y0 });
        return 1;
    }
    else {
        double d = r*r - c*c/(a*a+b*b);
        double mult = sqrt (d / (a*a+b*b));
        double ax, ay, bx, by;
        ax = x0 + b * mult;
        bx = x0 - b * mult;
        ay = y0 - a * mult;
        by = y0 + a * mult;
        ret.push_back({ax, ay});
        ret.push_back({bx, by});
        return 2;
    }
}

double dist(pt p1, pt p2) {
    double dx=p1.x-p2.x;
    double dy=p1.y-p2.y;
    return sqrt(dx*dx+dy*dy);
}

double x1, y1, x2, y2, r;

double solve0() {
    pt cent={ (x1+x2)/2, (y1+y2)/2 };
    double len=sqrt(cent.x*cent.x + cent.y*cent.y);
    pt sect={ cent.x*len/r, cent.y*len/r };
    return dist({ x1, y1}, sect) + dist({ x2, y2}, sect});
}

/* simple case, direct seg from p1 to p2 */
double solve1() {
    return dist({ x1, y1}, { x2, y2});
}

/* len is p1->sect1 + arc sect1->sect2 + sect2->p2 */
double solve2(pt sect1, pt sect2) {
/* find tangent(s) from sect1 to circle and sect2 to circle
 * find shorter arc on circle from t1 to t2.
 * Add the two tangents and the arc up.
 */
    
}

/* see https://www.mathopenref.com/consttangents.html */
int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cin>>x1>>y1>>x2>>y2>>r;

    /* 1. First check if the segment from p1/p2 crosses the circle. Do this by
     * constructing the vertical to the segment throug the circles center.
     * 2. Case1: If they cross construct the tangents from the points to the circle.
     * 3. Add up all parts of the len.
     * 2. Case2: If they do not cross construct the vertical to the segement
     * throug the circle center, it crosses the circle at searched point on the circle.
     * 3. Add up both segments.
     */
    line seg=calcLine({ x1, y1}, { x2, y2});
    vector<int> ret;
    int c=lineCircleIntersections(r, seg, ret);
    double ans;
    if(c==0)
        ans=solve0();
    else if(c==1)
        ans=solve1();
    else if(c==2)
        ans=solve2(ret[0], ret[1]);

    cout<<ans<<endl;
}

