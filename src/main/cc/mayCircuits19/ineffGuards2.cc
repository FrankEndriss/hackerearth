/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

//#define DEBUG

/* return true if g can see j */
bool isVisible(int gx, int gy, int jx, int jy) {
    return abs(gy-jy)<=jx-gx;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    int n, m;
    cin>>n>>m;
    vector<pair<int, int>> G(n);

    for(int i=0; i<n; i++) {
        cin>>G[i].first>>G[i].second;
    }
    sort(G.begin(), G.end());

    int ans=n;
    vector<bool> counted(n, false);
    for(int i=0; i<m; i++) {
        int jx, jy;
        cin>>jx>>jy;
        /* if a J is seen by exactly one G, that G is not ineffective. All others are. */
        int lcount=0;
        int guard;
        for(int j=0; j<n; j++) {
            if(G[j].first>jx || (G[j].first==jx && G[j].second>jy))
                break;
            else if(isVisible(G[j].first, G[j].second, jx, jy)) {
                guard=j;
                lcount++;
            }
            if(lcount==2)
                break;
        }
        if(lcount==1 && !counted[guard]) {
            counted[guard]=true;
            ans--;
        }
    }

    cout<<ans<<endl;
}

