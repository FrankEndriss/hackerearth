/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

#define SOL_1
//#define DEBUG

const int N=100003;
vector<pair<int, int>> tree[N];   // pair<node, edge>

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    int n, m;
    cin>>n>>m;

    typedef struct _sedge {
        ll w;
        int u;
        int v;
    } edge;
    vector<edge> edges(m);

    for(int i=0; i<m; i++) {
        int u, v;
        ll w;
        cin>>u>>v>>w;
        edges[i]= { w, u, v };
        tree[u].push_back({ v, i});
        tree[v].push_back({ u, i});
    }

    for(int i=1; i<=n; i++) {
        sort(tree[i].begin(), tree[i].end(), [&](pair<int, int> p1, pair<int, int> p2) {
            return edges[p1.second].w<edges[p2.second].w;
        });
    }

    deque<pair<int, ll>> next;    // BFS, <node, cnt>
    vector<ll> cheapest(n+1, -1);
    vector<int> p(n+1);   // edge[p[i]]= edge which was used to travel to node i

    cheapest[1]=0;
    next.push_back({ 1, 1LL} );

    ll cnt=1;   // simple counter
    vector<ll> ignlist(n+1);   // ignore next.front() if cnt <ignlist[node]
    while(next.size()>0) {
        cnt++;

        pair<int, ll> nodeent=next.front();
        next.pop_front();
        int node=nodeent.first;
        if(nodeent.second<=ignlist[node])
            continue;

        if(node==n) { /* found a path */
            // spath res=npath.second;
            // res.push_back(c.first, c.second);
            // paths.push_back(npath);
#ifdef SOL_1
            break;  // DEBUG for SOL_1
#endif
        }

#ifdef DEBUG
        cout<<"next, node="<<node<<endl;
#endif
        for(auto c : tree[node]) {  // pair<node, edgenum>
            ll newW=cheapest[node]+edges[c.second].w;
            if(cheapest[c.first]<0 || cheapest[c.first]>newW) {
            //if(cheapest[c.first]<0) {
                while(next.size()>0 && next.back().first==c.first)
                    next.pop_back();
                if(cheapest[c.first]>newW) {
                    ignlist[c.first]=cnt-1;
                    next.push_front({ c.first, cnt } );
                } else
                    next.push_back({ c.first, cnt } );
                cheapest[c.first]=newW;
                p[c.first]=c.second;
            }
        }
    }

#ifdef SOL_1
    vector<int> path; // edge
    int node=n;
    while(node!=1) {
        int ledge=p[node];
        path.push_back(ledge);
        if(edges[ledge].u==node)
            node=edges[ledge].v;
        else
            node=edges[ledge].u;
    }

    cout<<1<<endl;
    cout<<path.size();
    for(int i=path.size()-1; i>=0; i--)
        cout<<" "<<path[i]+1;
    cout<<endl;
#endif
}

