/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);

    /* https://en.wikipedia.org/wiki/Minimum_spanning_tree
     *
     * All edges:
     * The minW of an edge between any two vertices is the max of
     * weights on the path in MST between those vertices.
     *
     * Since these edges can be absent in the counted graphs the num
     * of possi for such edge equals 1+MAXW-minW, and 1 if minW>MAXW.
     * for(p : possi)
     *  ans*=p;
     *
     * Problem:
     * The number of edges is huge. (n*n-1)
     * We cannot sum up all edges one by one.
     *
     * What about...
     * Sort edges of MST by weight desc. Any edge of the MST devides the MST into two components.
     * All edges from vertices in comp1 to vertices in comp2 have minW at least...
     *
     * DFS traverse the tree from some choosen root. Any subtree returns a multiset of minW.
     * The multiset of leafs is empty.
     * Any node builds its multiset by adding up the multisets of its children.
     * Where any entry smaller than the edge to that node is summed up in the entry of that edge-weight.
     *
     * dfs1: Select the max weights.
     * dfs2: calc the numbers.
     */

    const int MOD=987654319;

    int n, MAXW;
    cin>>n>>MAXW;
    vector<vector<pair<int, int>>> tree(n+1);

    fori(n-1) {
        int u, v, w;
        cin>>u>>v>>w;
        tree[u].push_back({v, w});
        tree[v].push_back({u, w});
    }

    /* simple brute force.
     * For all nodes traverse the tree, keeping track of the max weight on the path,
     * collecting ans for all vertex pairs where first<second.
     */
    ll ans=1;
    int root=-1;
    function<void(int, int, int)> dfs=[&](int v, int p, int maxw) {
//cout<<"dfs, root="<<root<<" v="<<v<<" p="<<p<<" maxw="<<maxw<<endl;
        for(auto chl : tree[v]) {
            if(chl.first!=p) {
                int lmaxw=max(chl.second, maxw);
                if(chl.first>root) {
                    int mul=max(0, MAXW-lmaxw+1)+1;
//cout<<"root="<<root<<" v="<<v<<" chl.first="<<chl.first<<" mul="<<mul<<endl;
                    ans*=mul;
                    ans%=MOD;
                }
                dfs(chl.first, v, lmaxw);
            }
        }
    };

    for(root=1; root<n; root++) {
        for(auto c : tree[root])
            dfs(c.first, root, c.second);
    }

    cout<<ans<<endl;
}


