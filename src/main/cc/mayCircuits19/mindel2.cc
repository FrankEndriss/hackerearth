/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

#define SOL_1
//#define DEBUG

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    int n, m;
    cin>>n>>m;

    vector<vector<pair<int, pair<int, int>>> tree(n+1);

    for(int i=0; i<m; i++) {
        int u, v;
        ll w;
        cin>>u>>v>>w;
        tree[u].push_back({ v, { w, i+1 }});
        tree[v].push_back({ u, { w, i+1 }});
    }

    fori(n)
        sort(tree[i].begin(), tree[i].end());

}

