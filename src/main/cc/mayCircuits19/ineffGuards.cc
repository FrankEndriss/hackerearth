/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

//#define DEBUG

const int MOV=100000000;
const int NMAX=MOV*2+7;
/* rotate by 45 degree against clockwise and then move to positive range.
 * Note that we rotate and divive by sqrt(2) in one step, to get rid of 
 * the sqrt numbers.*/
void rotmov(pair<int, int> &p) {
    int x=p.first-p.second;
    int y=p.first+p.second;
    p.first=x+MOV;
    p.second=y+MOV;
}

/* mirror p at point (MOV, MOV) */
pair<int, int> mirror(pair<int, int> &p) {
    return { 2*MOV-p.first, 2*MOV-p.second } ;
}

/* return true if g can see j */
bool isVisible(pair<int, int> g, pair<int, int> j) {
    return abs(g.second-j.second)<=j.first-g.first;
}

/* 2D Fenwick trees */
//typedef map<int, map<int, int>> Ftree;
typedef unordered_map<ll, int> Ftree;
Ftree bitG;
Ftree bitJ;

void updatey(Ftree &ftree, ll x , int y , int val){
    //auto &ymap=ftree[x];
    x<<=32;
    while (y <= NMAX){
        ftree[x | y] += val;
        y += (y & -y); 
    }
}

int ready(Ftree &ftree, ll x, int y){
    int sum = 0;
    x<<=32;
    while (y > 0){
        auto it=ftree.find(x|y);
        if(it!=ftree.end())
            sum += it->second;
        y -= (y & -y);
    }
    return sum;
}

int read(Ftree &ftree, int x, int y){
    int sum = 0;
    while (x > 0){
        sum += ready(ftree, x, y);
        x -= (x & -x);
    }
    return sum;
}

void update(Ftree &ftree, int x , int y , int val){
    while (x <= NMAX){
        updatey(ftree, x , y , val); 
        x += (x & -x); 
    }
}
 

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    int n, m;
    cin>>n>>m;
    vector<pair<int, int>> G(n), J(m);

    //function<int(int, int)> add=[](int i1, int i2) { return i1+i2; };
    //Ftree bitG;
    //Ftree bitJ;

#ifdef DEBUG
cout<<"init Fenwicks"<<endl;
#endif

    fori(n) {
        cin>>G[i].first>>G[i].second;
        rotmov(G[i]);
#ifdef DEBUG
cout<<"G["<<i<<"]= "<<G[i].first<<" "<<G[i].second<<endl;
#endif
        //if(read(bitG, G[i].first, G[i].second)==0)
            update(bitG, G[i].first, G[i].second, 1);
    }

#ifdef DEBUG
cout<<42<<endl;
return 0;
#endif

    fori(m) {
        cin>>J[i].first>>J[i].second;
        rotmov(J[i]);
#ifdef DEBUG
cout<<"J["<<i<<"]= "<<J[i].first<<" "<<J[i].second<<endl;
#endif
    }

/*
* To efficently search if a G or J is seen by a guard we need to use
* a 2D BIT, see
* https://www.geeksforgeeks.org/two-dimensional-binary-indexed-tree-or-fenwick-tree/
*
* To make use of such tree we need to roatate the whole picture by 45 degrees
* (here against the clock). Then, the guards look right/up, p2 is seen by p1 if
* p1.x<=p2.x && p1.y<=p2.y
* We move all points to the positive range (by adding 10^8), then use
* BIT to find the number of points in range [(0, 0),(x, y)]
*
* Any G sees a (possibly empty) set of J.
* Note:
* We _do not_ search for the minimum set of G whichs union seeing all J. 
* This would be https://en.wikipedia.org/wiki/Set_cover_problem
* Instead:
*
* "Go through all guards and check if it is ineffective. In this sense, no guards
* are removed and we check for each guard in a fresh manner independently.
* This will give the number of ineffective guards possible."
*
* So, 
* a guard is ineffective if he sees no jewelery where he is the only one seeing it.
* a guard is not ineffective if he is such an only one.
*
* 1. Find all J with more than one G, remove that J.
* 2. Find number of G with no remaining J.
*
* Since this simple approach run out of memory we need to optimize:
* A Guard seen by another guard is allways inefficient.
*/

    vector<bool> remJ(m, false);

    fori(m) {
        int guardsOfJ=read(bitG, J[i].first, J[i].second);
#ifdef DEBUG
cout<<"guardsOfJ["<<i<<"]="<<guardsOfJ<<endl;
#endif
        if(guardsOfJ!=1)
            remJ[i]=true;
    }
    //bitG.clear();

    fori(m) {
        if(!remJ[i]) {
            auto pmirr=mirror(J[i]);
            update(bitJ, pmirr.first, pmirr.second, 1);
        }
    }
    //remJ.clear();

    int ans=0;
    fori(n) {
        auto pmirr=mirror(G[i]);
        if(read(bitJ, pmirr.first, pmirr.second)==0)
            ans++;
    }

    cout<<ans<<endl;
}

